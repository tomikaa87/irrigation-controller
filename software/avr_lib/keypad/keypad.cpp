/*
 * keypad.cpp
 *
 *  Created on: 2013.04.07.
 *      Author: ToMikaa
 */

#include "keypad.h"

#include <util/delay.h>

/*** Convenience macros *******************************************************/

/*
 * Check keypad column configuration
 */
#if (KEYPAD_COLS > 0)
    #ifndef KEY_COL_DDR
        #error KEY_COL_DDR is undefined
    #endif
    #ifndef KEY_COL_PORT_IN
        #error KEY_COL_PORT_IN is undefined
    #endif
    #ifndef KEY_COL_PORT
        #error KEY_COL_PORT is undefined
    #endif
    #ifndef KEY_C1
        #error KEY_C1 is undefined
    #endif
    #if (KEYPAD_COLS > 1)
        #ifndef KEY_C2
            #error KEY_C2 is undefined
        #endif
        #if (KEYPAD_COLS > 2)
            #ifndef KEY_C3
                #error KEY_C3 is undefined
            #endif
            #if (KEYPAD_COLS > 3)
                #ifndef KEY_C4
                    #error KEY_C4 is undefined
                #endif
                #if (KEYPAD_COLS > 4)
                    #error Maximum 4 key columns supported
                #endif
            #endif
        #endif
    #endif
#else
    #error Invalid number of KEYPAD COLS defined
#endif

/*
 * Check keypad row configuration
 */
#if (KEYPAD_ROWS > 0)
    #ifndef KEY_ROW_DDR
        #error KEY_ROW_DDR is undefined
    #endif
    #ifndef KEY_ROW_PORT
        #error KEY_ROW_PORT is undefined
    #endif
    #ifndef KEY_R1
        #error KEY_R1 is undefined
    #endif
    #if (KEYPAD_ROWS > 1)
        #ifndef KEY_R2
            #error KEY_R2 is undefined
        #endif
        #if (KEYPAD_ROWS > 2)
            #ifndef KEY_R3
                #error KEY_R3 is undefined
            #endif
            #if (KEYPAD_ROWS > 3)
                #ifndef KEY_R4
                    #error KEY_R4 is undefined
                #endif
                #if (KEYPAD_ROWS > 4)
                    #error Maximum 4 key rows supported
                #endif
            #endif
        #endif
    #endif
#else
    #error Invalid number of KEYPAD ROWS defined
#endif

#if (KEYPAD_ROWS == 1)
    #define KEY_ROW_BITS (_BV(KEY_R1))
    #define KEY_COL_BITS (_BV(KEY_C1))
#elif (KEYPAD_ROWS == 2)
    #define KEY_ROW_BITS (_BV(KEY_R1) | _BV(KEY_R2))
    #define KEY_COL_BITS (_BV(KEY_C1) | _BV(KEY_C2))
#elif (KEYPAD_ROWS == 3)
    #define KEY_ROW_BITS (_BV(KEY_R1) | _BV(KEY_R2) | _BV(KEY_R3))
    #define KEY_COL_BITS (_BV(KEY_C1) | _BV(KEY_C2) | _BV(KEY_C3))
#elif (KEYPAD_ROWS == 4)
    #define KEY_ROW_BITS (_BV(KEY_R1) | _BV(KEY_R2) | _BV(KEY_R3) | _BV(KEY_R4))
    #define KEY_COL_BITS (_BV(KEY_C1) | _BV(KEY_C2) | _BV(KEY_C3) | _BV(KEY_C4))
#endif

#define KEY_ROW_ACTIVATE(ROW) \
    KEY_ROW_DDR |= _BV(ROW); \
    KEY_ROW_PORT &= ~_BV(ROW); \
    _delay_us(1);

#define KEY_ROW_RELEASE(ROW) \
    KEY_ROW_DDR &= ~_BV(ROW); \
    KEY_ROW_PORT |= _BV(ROW); \
    _delay_us(1);

#define KEY_CHECK_COL(COL) (!(KEY_COL_PORT_IN & _BV(COL)))

/*** Constructor **************************************************************/

Keypad::Keypad():
    m_pressCnt(PressCnt), m_longPressPass(0), m_lastKey(0),
    m_longPressCnt(0), m_longPressLim(LongPressLimMax)
{
    // Setup key row pins
    KEY_ROW_DDR |= KEY_ROW_BITS;
    KEY_ROW_PORT &= ~KEY_ROW_BITS;

    // Setup key column pins
    KEY_COL_DDR &= ~KEY_COL_BITS;
    KEY_COL_PORT |= KEY_COL_BITS;
}

uint8_t Keypad::task()
{
    // Long press handler
    if (m_pressCnt < PressCnt) {
        // Set row pins to LOW
        KEY_ROW_DDR |= KEY_ROW_BITS;
        KEY_ROW_PORT &= ~KEY_ROW_BITS;
        _delay_us(1);

        if ((KEY_COL_PORT_IN & KEY_COL_BITS) != KEY_COL_BITS) {
            // A key is still pressed
            m_pressCnt = 0;
            m_longPressCnt++;
        } else {
            m_pressCnt++;
        }

        // Set row pins to input with pull-ups
        KEY_ROW_DDR &= ~KEY_ROW_BITS;
        KEY_ROW_PORT |= KEY_ROW_BITS;
        _delay_us(1);

        if (m_longPressCnt == m_longPressLim) {
            m_longPressCnt = 0;
            if (m_longPressPass == 3) {
                if (m_longPressLim > LongPressLimMin) {
                    m_longPressLim -= LongPressLimDelta;
                }
                return m_lastKey;
            } else {
                m_longPressPass++;
            }
        }

        return 0;
    }

    m_longPressCnt = 0;
    m_longPressPass = 0;
    m_longPressLim = LongPressLimMax;

    // Set row pins to low
    KEY_ROW_DDR &= ~KEY_ROW_BITS;
    KEY_ROW_PORT |= KEY_ROW_BITS;

    // Read ROW 1
#if (KEYPAD_ROWS > 0)
    KEY_ROW_ACTIVATE(KEY_R1);
#if (KEYPAD_COLS > 0)
    if (KEY_CHECK_COL(KEY_C1)) {
        m_pressCnt = 0;
        m_lastKey = 0x11;
        return 0x11;
    }
#endif
#if (KEYPAD_COLS > 1)
    if (KEY_CHECK_COL(KEY_C2)) {
        m_pressCnt = 0;
        m_lastKey = 0x12;
        return 0x12;
    }
#endif
#if (KEYPAD_COLS > 2)
    if (KEY_CHECK_COL(KEY_C3)) {
        m_pressCnt = 0;
        m_lastKey = 0x13;
        return 0x13;
    }
#endif
#if (KEYPAD_COLS > 3)
    if (KEY_CHECK_COL(KEY_C4)) {
        m_pressCnt = 0;
        m_lastKey = 0x14;
        return 0x14;
    }
#endif
    KEY_ROW_RELEASE(KEY_R1);
#endif

    // Read ROW 2
#if (KEYPAD_ROWS > 1)
    KEY_ROW_ACTIVATE(KEY_R2);
#if (KEYPAD_COLS > 0)
    if (KEY_CHECK_COL(KEY_C1)) {
        m_pressCnt = 0;
        m_lastKey = 0x21;
        return 0x21;
    }
#endif
#if (KEYPAD_COLS > 1)
    if (KEY_CHECK_COL(KEY_C2)) {
        m_pressCnt = 0;
        m_lastKey = 0x22;
        return 0x22;
    }
#endif
#if (KEYPAD_COLS > 2)
    if (KEY_CHECK_COL(KEY_C3)) {
        m_pressCnt = 0;
        m_lastKey = 0x23;
        return 0x23;
    }
#endif
#if (KEYPAD_COLS > 3)
    if (KEY_CHECK_COL(KEY_C4)) {
        m_pressCnt = 0;
        m_lastKey = 0x24;
        return 0x24;
    }
#endif
    KEY_ROW_RELEASE(KEY_R2);
#endif

    // Read ROW 3
#if (KEYPAD_ROWS > 2)
    KEY_ROW_ACTIVATE(KEY_R3);
#if (KEYPAD_COLS > 0)
    if (KEY_CHECK_COL(KEY_C1)) {
        m_pressCnt = 0;
        m_lastKey = 0x31;
        return 0x31;
    }
#endif
#if (KEYPAD_COLS > 1)
    if (KEY_CHECK_COL(KEY_C2)) {
        m_pressCnt = 0;
        m_lastKey = 0x32;
        return 0x32;
    }
#endif
#if (KEYPAD_COLS > 2)
    if (KEY_CHECK_COL(KEY_C3)) {
        m_pressCnt = 0;
        m_lastKey = 0x33;
        return 0x33;
    }
#endif
#if (KEYPAD_COLS > 3)
    if (KEY_CHECK_COL(KEY_C4)) {
        m_pressCnt = 0;
        m_lastKey = 0x34;
        return 0x34;
    }
#endif
    KEY_ROW_RELEASE(KEY_R3);
#endif

    // Read ROW 4
#if (KEYPAD_ROWS > 3)
    KEY_ROW_ACTIVATE(KEY_R4);
#if (KEYPAD_COLS > 0)
    if (KEY_CHECK_COL(KEY_C1)) {
        m_pressCnt = 0;
        m_lastKey = 0x41;
        return 0x41;
    }
#endif
#if (KEYPAD_COLS > 1)
    if (KEY_CHECK_COL(KEY_C2)) {
        m_pressCnt = 0;
        m_lastKey = 0x42;
        return 0x42;
    }
#endif
#if (KEYPAD_COLS > 2)
    if (KEY_CHECK_COL(KEY_C3)) {
        m_pressCnt = 0;
        m_lastKey = 0x43;
        return 0x43;
    }
#endif
#if (KEYPAD_COLS > 3)
    if (KEY_CHECK_COL(KEY_C4)) {
        m_pressCnt = 0;
        m_lastKey = 0x44;
        return 0x44;
    }
#endif
    KEY_ROW_RELEASE(KEY_R4);
#endif

    return 0;
}
