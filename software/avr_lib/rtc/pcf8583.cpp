/*
 * PCF8583 RTC IC driver module
 *
 *  Created on: 2013.04.06.
 *      Author: Tamas Karpati <tomikaa87@gmail.com>
 */

#include "pcf8583.h"

#include <avr/io.h>
#include <util/twi.h>

#include "hd44780.h"
HD44780 lcd;

/*** Constructor **************************************************************/

PCF8583::PCF8583()
{
}

/*** API methods **************************************************************/

void PCF8583::setDate(uint16_t y, uint8_t m, uint8_t d, uint8_t weekday) const
{
    uint8_t data[2];

    // Year and day register
    data[0] = (y % 4) << 6 | toBcd(d);
    // Weekday and month register
    data[1] = (weekday & 0x07) << 5 | toBcd(m);

    writeRegArray(YearDateAddr, data, 2);

    // Write full year to user ram
    data[0] = y & 0xff;
    data[1] = y >> 8;
    writeRegArray(YearLowAddr, data, 2);
}

void PCF8583::setDate(Date d) const
{
    setDate(d.y, d.m, d.d, d.weekday);
}

PCF8583::Date PCF8583::date() const
{
    // Read year/date and month register
    uint8_t yd[2];
    readRegArray(YearDateAddr, yd, 2);
    // Read full year registers
    uint8_t fy[2];
    readRegArray(YearLowAddr, fy, 2);

    // Calculate values
    Date d;
    d.y = fy[0] | (fy[1] << 8);
    uint8_t yearCnt = yd[0] >> 6;
    d.d = toDec(yd[0] & 0x3f);
    d.m = toDec(yd[1] & 0x1f);

    // Adjust full year value
    while (d.y % 4 != yearCnt) {
        d.y++;
    }

    // Read weekday
    d.weekday = yd[1] >> 5;

    return d;
}

void PCF8583::setTime(uint16_t h, uint8_t m, uint8_t s) const
{
    uint8_t data[3];

    data[0] = toBcd(s);
    data[1] = toBcd(m);
    data[2] = toBcd(h);

    writeRegArray(SecsAddr, data, sizeof (data));
}

void PCF8583::setTime(Time t) const
{
    uint8_t data[3];

    data[0] = toBcd(t.s);
    data[1] = toBcd(t.m);
    data[2] = toBcd(t.h);

    writeRegArray(SecsAddr, data, 3);
}

PCF8583::Time PCF8583::time() const
{
    uint8_t data[3];
    Time t;

    // Read time data from RTC IC
    if (!readRegArray(SecsAddr, data, 3)) {
        return t;
    }

    t.s = toDec(data[0]);
    t.m = toDec(data[1]);
    t.h = toDec(data[2]);

    return t;
}

bool PCF8583::writeReg(SramAddress addr, uint8_t data) const
{
    // Send START condition with slave address
    if (!m_i2c.start(PCF8583_ADDR | TW_WRITE)) {
        return false;
    }

    // Write register address
    if (!m_i2c.write(addr)) {
        return false;
    }

    // Write data byte
    if (!m_i2c.write(data)) {
        return false;
    }

    // Send STOP condition
    m_i2c.stop();

    return true;
}

 bool PCF8583::writeRegArray(SramAddress addr, uint8_t* buf, uint8_t size) const
{
     // Send START condition with slave address
     if (!m_i2c.start(PCF8583_ADDR | TW_WRITE)) {
         return 0xff;
     }

     // Write register address
     if (!m_i2c.write(addr)) {
         return 0xff;
     }

     // Write 'size' number of data bytes
     while (size--) {
         if (!m_i2c.write(*buf++)) {
             return false;
         }
     }

     // Send STOP condition
     m_i2c.stop();

     return true;
}

uint8_t PCF8583::readReg(SramAddress addr) const
{
    // Send START condition with slave address
    if (!m_i2c.start(PCF8583_ADDR | TW_WRITE)) {
        return 0xff;
    }

    // Write register address
    if (!m_i2c.write(addr)) {
        return 0xff;
    }

    // Send RESTART condition with slave address
    if (!m_i2c.start(PCF8583_ADDR | TW_READ)) {
        return 0xff;
    }

    // Read data
    uint8_t data = m_i2c.readNak();

    // Send STOP condition
    m_i2c.stop();

    return data;
}

bool PCF8583::readRegArray(SramAddress addr, uint8_t* buf, uint8_t size) const
{
    // Send START condition with slave address
    if (!m_i2c.start(PCF8583_ADDR | TW_WRITE)) {
        return false;
    }

    // Write register address
    if (!m_i2c.write(addr)) {
        return false;
    }

    // Send RESTART condition with slave address
    if (!m_i2c.start(PCF8583_ADDR | TW_READ)) {
        return false;
    }

    // Read 'size' number of data bytes
    while (size--) {
        if (!size) {
            // Read last byte, send NACK
            *buf = m_i2c.readNak();
        } else {
            // Read byte, send ACK, increment buf address
            *buf++ = m_i2c.readAck();
        }
    }

    // Send STOP condition
    m_i2c.stop();

    return true;
}

uint8_t PCF8583::toDec(uint8_t bcd)
{
    return ((bcd >> 4) * 10) + (bcd & 0x0f);
}

uint8_t PCF8583::toBcd(uint8_t dec)
{
    return ((dec / 10) << 4) + (dec % 10);
}

uint16_t PCF8583::dayOfYear(Date d)
{
    uint16_t doy = 0;
    for (uint8_t i = 0; i < d.m - 1; i++) {
        doy = pgm_read_byte(DaysOfMonth + i);
        // Leap year correction
        if (i == 1 && d.y % 4 == 0) {
            doy++;
        }
    }
    doy += d.d;

    return doy;
}
