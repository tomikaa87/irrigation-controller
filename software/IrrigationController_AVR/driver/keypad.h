/*
 * keypad.h
 *
 *  Created on: 2013.04.07.
 *      Author: ToMikaa
 */

#ifndef KEYPAD_H_
#define KEYPAD_H_

#include <avr/io.h>

/*** Configuration ************************************************************/

#define KEYPAD_ROWS                     3
#define KEYPAD_COLS                     3

#define KEY_R1                          PINC2
#define KEY_R2                          PINC3
#define KEY_R3                          PINC4
//#define KEY_R4                          PINC5
#define KEY_ROW_DDR                     DDRC
#define KEY_ROW_PORT                    PORTC

#define KEY_C1                          PINC5
#define KEY_C2                          PINC6
#define KEY_C3                          PINC7
//#define KEY_C4                          PINC7
#define KEY_COL_DDR                     DDRC
#define KEY_COL_PORT                    PORTC
#define KEY_COL_PORT_IN                 PINC

/******************************************************************************/

class Keypad
{
public:
    Keypad();

    uint8_t task();

private:
    enum {
        RepeatWait = 30,
        RepeatDelayMin = 4000,
        RepeatDelayDelta = 2000,
        RepeatDelayMax = 20000
    };

    uint8_t m_repeatWaitCnt;
    uint8_t m_speedUpDelay;
    uint8_t m_lastKey;
    uint16_t m_repeatCnt;
    uint16_t m_repeatDelay;
};

#endif /* KEYPAD_H_ */
