/*
 * TWI (I2C) interface driver
 *
 *  Created on: 2013.04.07.
 *      Author: Tamas Karpati <tomikaa87@gmail.com>
 */

#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>

/*** Configuration ************************************************************/

#ifndef I2C_SCL_CLOCK
    #define I2C_SCL_CLOCK               100000ul
#endif

#ifndef F_CPU
    #define F_CPU                       8000000ul
#endif

/******************************************************************************/

class I2C
{
public:
    I2C();

    bool start(uint8_t addr) const;
    void startWait(uint8_t addr) const;
    void stop() const;
    bool write(uint8_t data) const;
    uint8_t readAck() const;
    uint8_t readNak() const;
};

#endif /* I2C_H_ */
