/*
 * HD44780 (or compatible) based alphanumeric LCD driver module
 * with adjustable backlight and contrast support and 4-bit data transfer.
 *
 *  Created on: 2013.04.04.
 *      Author: Tamas Karpati <tomikaa87@gmail.com>
 */

#include "hd44780.h"

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/*** Internal macros **********************************************************/

#define _WRITE_MODE() \
    LCD_PORT_DDR |= _BV(LCD_PIN_D4) | _BV(LCD_PIN_D5) | \
    _BV(LCD_PIN_D6) | _BV(LCD_PIN_D7)

#define _READ_MODE() \
    LCD_PORT_DDR &= ~(_BV(LCD_PIN_D4) | _BV(LCD_PIN_D5) | \
    _BV(LCD_PIN_D6) | _BV(LCD_PIN_D7))

#define _DLY_SETUP()    _delay_ms(5)
#define _DLY_CLEAR()    _delay_ms(2)
#define _DLY_INSTR()    _delay_us(1)

#define _PIN_HIGH(PIN)   LCD_PORT |= _BV(PIN)
#define _PIN_LOW(PIN)    LCD_PORT &= ~(_BV(PIN))
#define _PIN_READ(PIN)   (LCD_PORT_IN & _BV(PIN) ? 1 : 0)
#define _PIN_SET(PIN,HIGH) \
    {if (HIGH) _PIN_HIGH(PIN); else _PIN_LOW(PIN);}

const uint8_t PROGMEM LCD_INIT_SEQ[4] = {0x03, 0x03, 0x03, 0x02};

/*** Constructor **************************************************************/

HD44780::HD44780(uint8_t backlightPwm, uint8_t contrastPwm)
{
    ioInit();
    init();
    LCD_SET_LED_PWM_DUTY(backlightPwm);
    LCD_SET_CONT_PWM_DUTY(contrastPwm);

    memset(m_lines, 0, sizeof (m_lines));
    m_lastLine = 0;
    m_firstLine = 0;
    m_lineCount = 0;
}

/*** API methods **************************************************************/

void HD44780::ioInit() const
{
    // Setup PWM
    LCD_SETUP_LED_PWM();
    LCD_SETUP_CONT_PWM();

    // Setup LCD control pins
    LCD_PORT_DDR |= _BV(LCD_PIN_EN) | _BV(LCD_PIN_RS) | _BV(LCD_PIN_RW);
}

void HD44780::init() const
{
    _WRITE_MODE();

    // Send initialization sequence
    for (uint8_t i = 0; i < sizeof (LCD_INIT_SEQ); i++) {
        _DLY_SETUP();
        writeNibble(pgm_read_byte(LCD_INIT_SEQ + i));
    }

    // Write setup instructions
    writeByte(FuncSet | Func2Lines, false);
    writeByte(SetDispCur | DispOn, false);
    writeByte(Clear, false);
}

void HD44780::clear() const
{
    writeByte(Clear, false);
}

void HD44780::putch(char c) const
{
    writeByte(c);
}

void HD44780::puts(const char *s) const
{
    while (*s) {
        writeByte(*s++);
    }
}

void HD44780::puts_P(const char *s) const
{
    char c = pgm_read_byte(s++);
    while (c) {
        writeByte(c);
        c = pgm_read_byte(s++);
    }
}

void HD44780::print(const char *fmt, ...) const
{
    char buf[21];
    va_list args;
    va_start(args, fmt);

    vsnprintf(buf, sizeof (buf), fmt, args);
    puts(buf);
}

void HD44780::print_P(const char *fmt, ...) const
{
    char buf[21];
    va_list args;
    va_start(args, fmt);

    vsnprintf_P(buf, sizeof (buf), fmt, args);
    puts(buf);
}

char HD44780::getch() const
{
    // Busy check
    while (readByte(false) & 0x80)
        continue;

    return static_cast<char>(readByte());
}

void HD44780::gotoxy(uint8_t x, uint8_t y) const
{
    uint8_t addr;

    switch (y) {
    case 0:
        addr = Line1;
        break;
    case 1:
        addr = Line2;
        break;
    case 2:
        addr = Line3;
        break;
    case 3:
        addr = Line4;
        break;
    default:
        addr = Line1;
        break;
    }

    addr += x;

    // Set DDRAM address
    writeByte(SetDdramAddr | addr, false);
}

void HD44780::defineChar(uint8_t place, uint8_t *data) const
{
    // Check place number
    if (place > 7) {
        return;
    }

    // Set CGRAM address
    uint8_t cgaddr = SetCgramAddr | (place << 3);
    writeByte(cgaddr, false);

    // Write custom character data
    for (uint8_t i = 0; i < 8; i++) {
        writeByte(*data++);
    }
}

void HD44780::shift(bool display, bool right) const
{
    uint8_t instr = DispCurShift;

    if (display) {
        instr |= DispShift;
    }
    if (right) {
        instr |= ShiftRight;
    }

    writeByte(instr, false);
}

void HD44780::setDispMode(bool dispOn, bool curUnderline, bool curBlink) const
{
    uint8_t instr = SetDispCur;

    if (dispOn) {
        instr |= DispOn;
    }
    if (curUnderline) {
        instr |= CurUnderline;
    }
    if (curBlink) {
        instr |= CurBlink;
    }

    writeByte(instr, false);
}

void HD44780::setFunc(bool data8Bit, bool disp2Lines, bool chars10Dots) const
{
    uint8_t instr = FuncSet;

        if (data8Bit) {
            instr |= Func8bit;
        }
        if (disp2Lines) {
            instr |= Func2Lines;
        }
        if (chars10Dots) {
            instr |= Func10Dots;
        }

        writeByte(instr, false);
}

void HD44780::setBackightLevel(uint8_t pwm) const
{
    LCD_SET_LED_PWM_DUTY(pwm);
}

void HD44780::setContrast(uint8_t pwm) const
{
    LCD_SET_CONT_PWM_DUTY(pwm);
}

void HD44780::log(const char *s)
{
    char *line = m_lines + m_lastLine * 21;
    strncpy(line, s, 21);
    m_lastLine++;
    if (m_lastLine % 4 == 0) {
        m_lastLine = 0;
    }
    if (m_lineCount < 4) {
        m_lineCount++;
    } else {
        m_firstLine++;
        if (m_firstLine % 4 == 0) {
            m_firstLine = 0;
        }
    }

    clear();
    for (uint8_t i = 0, l = m_firstLine; i < m_lineCount; i++) {
        gotoxy(0, i);
        line = m_lines + l * 21;
        puts(line);
        if (++l == 4) {
            l = 0;
        }
    }

    /*
     * ll 0 1 2 3 0 1 2 3
     * fl 0 0 0 0 1 2 3 0
     * lc 0 1 2 3 4 4 4 4
     */
}

void HD44780::log_P(const char *s)
{
    char buf[21];
    strncpy_P(buf, s, sizeof (buf));
    log(buf);
}

void HD44780::printlog(const char *fmt, ...)
{
    char buf[21];
    va_list args;
    va_start(args, fmt);

    vsnprintf(buf, sizeof (buf), fmt, args);
    log(buf);
}

void HD44780::printlog_P(const char *fmt, ...)
{
    char buf[21];
    va_list args;
    va_start(args, fmt);

    vsnprintf_P(buf, sizeof (buf), fmt, args);
    log(buf);
}

/*** Private stuff ************************************************************/

uint8_t HD44780::readByte(bool dataMode) const
{
    _READ_MODE();

    // Data/Command mode
    _PIN_SET(LCD_PIN_RS, dataMode);
    _DLY_INSTR();

    // Read high nibble
    _PIN_HIGH(LCD_PIN_RW);
    _DLY_INSTR();
    _PIN_HIGH(LCD_PIN_EN);
    _DLY_INSTR();

    uint8_t data = (LCD_PORT_IN & 0x0f) << 4;

    // Strobe EN for the next nibble
    _PIN_LOW(LCD_PIN_EN);
    _DLY_INSTR();
    _PIN_HIGH(LCD_PIN_EN);
    _DLY_INSTR();

    // Read low nibble
    data |= (LCD_PORT_IN & 0x0f);

    _PIN_LOW(LCD_PIN_EN);

    _WRITE_MODE();

    return data;
}

void HD44780::writeNibble(uint8_t nibble) const
{
    // Output bits
    _PIN_SET(LCD_PIN_D4, nibble & 1);
    _PIN_SET(LCD_PIN_D5, nibble & 2);
    _PIN_SET(LCD_PIN_D6, nibble & 4);
    _PIN_SET(LCD_PIN_D7, nibble & 8);

    // Strobe EN
    _DLY_INSTR();
    _PIN_HIGH(LCD_PIN_EN);
    _DLY_INSTR();
    _PIN_LOW(LCD_PIN_EN);
}

void HD44780::writeByte(uint8_t byte, bool dataMode) const
{
    // Busy checking
    _PIN_LOW(LCD_PIN_RS);
    while (readByte(false) & 0x80)
        continue;

    // Data/Command mode
    _DLY_INSTR();
    _PIN_SET(LCD_PIN_RS, dataMode);

    // Write mode
    _DLY_INSTR();
    _PIN_LOW(LCD_PIN_RW);
    _DLY_INSTR();
    _PIN_LOW(LCD_PIN_EN);

    // Write data, high nibble first
    writeNibble(byte >> 4);
    writeNibble(byte & 0x0f);
}
