/*
 * PCF8583 RTC IC driver module
 *
 *  Created on: 2013.04.06.
 *      Author: Tamas Karpati <tomikaa87@gmail.com>
 */

#ifndef PCF8583_H_
#define PCF8583_H_

/*** Configuration ************************************************************/

#define PCF8583_A0                      0
#define PCF8583_ADDR                    0b10100000 | (PCF8583_A0 << 1)

/******************************************************************************/

#include "i2c.h"

#include <stdint.h>
#include <avr/pgmspace.h>

const uint8_t PROGMEM DaysOfMonth[12] = {
//       J   F   M   A   M   J   J   A   S   O   N   D
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

const char PROGMEM WeekdayName[7][4] = {
    "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"
};

class PCF8583
{
public:
    /*
     * RTC IC SRAM addresses
     */
    enum SramAddress {
        // Register addresses
        ControlAddr         = 0x00,
        HundSecsAddr,
        SecsAddr,
        MinsAddr,
        HoursAddr,
        YearDateAddr,
        WeekdayMonthAddr,
        TimerAddr,
        AlarmCtrlAddr,
        AlarmHundSecsAddr,
        AlarmSecsAddr,
        AlarmMinsAddr,
        AlarmHoursAddr,
        AlarmDateAddr,
        AlarmMonthAddr,
        AlarmTimerAddr,

        // User RAM addresses
        YearLowAddr,
        YearHighAddr,

        UserRamBaseAddr,

        // Configuration storage
        ConfigChecksum      = UserRamBaseAddr,
        ConfigData,

        UserRamBaseEnd      = 0xff
    };

    struct Date {
        uint16_t y;
        uint8_t m;
        uint8_t d;
        uint8_t weekday;
    };

    struct Time {
        uint8_t h;
        uint8_t m;
        uint8_t s;

        Time() {
            h = 0xff;
            m = 0xff;
            s = 0xff;
        }
        Time(uint8_t _h, uint8_t _m, uint8_t _s):
            h(_h), m(_m), s(_s) {}
    };

    PCF8583();

    /*
     * Date management
     */
    void setDate(uint16_t y, uint8_t m, uint8_t d, uint8_t weekday) const;
    void setDate(Date d) const;
    Date date() const;

    /*
     * Time management
     */
    void setTime(uint16_t h, uint8_t m, uint8_t s) const;
    void setTime(Time t) const;
    Time time() const;

    /*
     * Low level access methods
     */
    bool writeReg(SramAddress addr, uint8_t data) const;
    bool writeRegArray(SramAddress addr, uint8_t *buf, uint8_t size) const;
    uint8_t readReg(SramAddress addr) const;
    bool readRegArray(SramAddress addr, uint8_t *buf, uint8_t size) const;

    /*
     * Utility methods
     */
    static uint8_t toDec(uint8_t bcd);
    static uint8_t toBcd(uint8_t dec);
    static uint16_t dayOfYear(Date d);

private:
    I2C m_i2c;
};

#endif /* PCF8583_H_ */
