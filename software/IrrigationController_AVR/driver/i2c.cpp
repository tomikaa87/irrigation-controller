/*
 * TWI (I2C) interface driver
 *
 *  Created on: 2013.04.07.
 *      Author: Tamas Karpati <tomikaa87@gmail.com>
 */

#include "i2c.h"

#include <avr/io.h>
#include <util/twi.h>
#include <util/delay.h>

/*** Convenient macros ********************************************************/

#define I2C_WAIT() \
    { uint8_t CNT = 0; while (!(TWCR & _BV(TWINT)) && CNT++ < 20) _delay_us(10); }
#define I2C_RESULT()    (TW_STATUS)

/*** Constructor **************************************************************/

I2C::I2C()
{
    // Initialize I2C
    TWSR = 0;
    TWBR = ((F_CPU/I2C_SCL_CLOCK) - 16) / 2;
}

/*** API methods **************************************************************/

bool I2C::start(uint8_t addr) const
{
    // Send START condition
    TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
    I2C_WAIT();

    // Check result
    uint8_t r = I2C_RESULT();
    if (r != TW_START && r != TW_REP_START) {
        return false;
    }

    // Send slave address
    TWDR = addr;
    TWCR = _BV(TWINT) | _BV(TWEN);
    I2C_WAIT();

    // Check result
    r = I2C_RESULT();
    if (r != TW_MT_SLA_ACK && r != TW_MR_SLA_ACK) {
        return false;
    }

    return true;
}

void I2C::startWait(uint8_t addr) const
{
    for (;;) {
        // Send START condition
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
        I2C_WAIT();

        // Check result
        uint8_t r = I2C_RESULT();
        if (r != TW_START && r != TW_REP_START) {
            continue;
        }

        // Send slave address
        TWDR = addr;
        TWCR = _BV(TWINT) | _BV(TWEN);
        I2C_WAIT();

        // Check result
        r = I2C_RESULT();
        if (r == TW_MT_SLA_NACK || r == TW_MR_DATA_NACK) {
            // Device is busy, send STOP condition
            TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
            I2C_WAIT();
            continue;
        }
        break;
    }
}

void I2C::stop() const
{
    // Send STOP condition
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
    uint8_t c = 0;
    while ((TWCR & _BV(TWSTO)) && c++ < 20)
        _delay_us(10);
}

bool I2C::write(uint8_t data) const
{
    // Send data to slave device
    TWDR = data;
    TWCR = _BV(TWINT) | _BV(TWEN);
    I2C_WAIT();

    // Check result
    uint8_t r = I2C_RESULT();
    if (r != TW_MT_DATA_ACK) {
        return false;
    }

    return true;
}

uint8_t I2C::readAck() const
{
    TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN);
    I2C_WAIT();

    return TWDR;
}

uint8_t I2C::readNak() const
{
    TWCR = _BV(TWINT) | _BV(TWEN);
    I2C_WAIT();

    return TWDR;
}
