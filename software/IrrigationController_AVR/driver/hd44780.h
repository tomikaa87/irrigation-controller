/*
 * HD44780 (or compatible) based alphanumeric LCD driver module
 * with adjustable backlight and contrast support and 4-bit data transfer.
 *
 *  Created on: 2013.04.04.
 *      Author: Tamas Karpati <tomikaa87@gmail.com>
 */

#ifndef HD44780_H_
#define HD44780_H_

#include <stdint.h>
#include <avr/pgmspace.h>

/*** Hardware configuration ***************************************************/

#define LCD_HAS_PWM_BACKLIGHT
#define LCD_HAS_PWM_CONTRAST

/*
 * Pin configuration
 */
#define LCD_PORT                        PORTA
#define LCD_PORT_IN                     PINA
#define LCD_PORT_DDR                    DDRA
#define LCD_PIN_D4                      PINA0
#define LCD_PIN_D5                      PINA1
#define LCD_PIN_D6                      PINA2
#define LCD_PIN_D7                      PINA3
#define LCD_PIN_RS                      PINA4
#define LCD_PIN_RW                      PINA5
#define LCD_PIN_EN                      PINA6

/*
 * Backlight and contrast control configuration
 */
#ifdef LCD_HAS_PWM_BACKLIGHT
#ifdef LCD_HAS_PWM_CONTRAST

/*
 * PWM backlight configuration
 * No prescaler, 31.5 kHz, non-inverting output
 */
#define LCD_SETUP_LED_PWM() \
        TCCR0 = _BV(WGM00) | _BV(WGM01) | _BV(COM01) | _BV(CS00); \
        DDRB |= _BV(PINB3);
#define LCD_SET_LED_PWM_DUTY(DUTY) \
        { if (DUTY > 0) { DDRB |= _BV(PORTB3); OCR0 = DUTY; }\
        else DDRB &= ~_BV(PINB3); }
#else
    /*
     * Default PWM macros
     */
    #define LCD_SETUP_LED_PWM() (void);
    #define LCD_SET_LED_PWM_DUTY(DUTY) (void)DUTY;
#endif

/*
 * PWM contrast configuration
 * No prescaler, 31.5 kHz, inverting output
 */
#define LCD_SETUP_CONT_PWM() \
        TCCR2 = _BV(WGM20) | _BV(WGM21) | _BV(COM21) | _BV(COM20) | _BV(CS20); \
        DDRD |= _BV(PIND7);
#define LCD_SET_CONT_PWM_DUTY(DUTY) \
        OCR2 = DUTY;
#else
    /*
     * Default PWM macros
     */
    #define LCD_SETUP_CONT_PWM() (void);
    #define LCD_SET_CONT_PWM_DUTY(DUTY) (void)DUTY;
#endif

/*
 * Supported display types
 */
#define LCD_TYPE_2_LINE     1
#define LCD_TYPE_4x16       2
#define LCD_TYPE_4x20       3

/*
 * Selected display type
 */
#define LCD_DISP_TYPE       LCD_TYPE_4x20

/******************************************************************************/

class HD44780
{
public:
    HD44780(uint8_t backlightPwm = 0x50, uint8_t contrastPwm = 0x50);

    enum Command {
        Clear           = 0b00000001,
        DispCurHome     = 0b00000010,
        SetCgramAddr    = 0b01000000,
        SetDdramAddr    = 0b10000000,
        SetEntryMode    = 0b00000100,   // Use with EntryMode
        SetDispCur      = 0b00001000,   // Use with CursorMode
        DispCurShift    = 0b00010000,   // Use with ShiftMode
        FuncSet         = 0b00100000    // Use with Function
    };

    enum EntryMode {
        EntryInc        = 0b00000010,
        DispShiftOn     = 0b00000001
    };

    enum CursorMode {
        DispOn          = 0b00000100,
        CurUnderline    = 0b00000010,
        CurBlink        = 0b00000001
    };

    enum ShiftMode {
        DispShift       = 0b00001000,
        ShiftRight      = 0b00000100
    };

    enum Function {
        Func8bit        = 0b00010000,
        Func2Lines      = 0b00001000,
        Func10Dots      = 0b00000100
    };

    enum LineAddress {
        Line1           = 0x00,
        Line2           = 0x40,
#if (LCD_DISP_TYPE == 1)
        Line3           = 0x00,         // No 3rd and 4th line
        Line4           = 0x40
#elif (LCD_DISP_TYPE == 2)
        Line3           = 0x10,
        Line4           = 0x50
#elif (LCD_DISP_TYPE == 3)
        Line3           = 0x14,
        Line4           = 0x54
#endif
    };

    void ioInit() const;

    void init() const;
    void clear() const;
    void putch(char c) const;
    void puts(const char *s) const;
    void puts_P(const char *s) const;
    void print(const char *fmt, ...) const;
    void print_P(const char *fmt, ...) const;
    char getch() const;
    void gotoxy(uint8_t x, uint8_t y) const;
    void defineChar(uint8_t place, uint8_t *data) const;
    void shift(bool display = false, bool right = false) const;
    void setDispMode(bool dispOn = true, bool curUnderline = false,
            bool curBlink = false) const;
    void setFunc(bool data8Bit = false, bool disp2Lines = false,
            bool chars10Dots = false) const;

    void setBackightLevel(uint8_t pwm) const;
    void setContrast(uint8_t pwm) const;

    void log(const char *s);
    void log_P(const char *s);
    void printlog(const char *fmt, ...);
    void printlog_P(const char *fmt, ...);

private:
    char m_lines[84];
    uint8_t m_firstLine;
    uint8_t m_lastLine;
    uint8_t m_lineCount;

    uint8_t readByte(bool dataMode = true) const;
    void writeNibble(uint8_t nibble) const;
    void writeByte(uint8_t byte, bool dataMode = true) const;
};

#endif /* HD44780_H_ */
