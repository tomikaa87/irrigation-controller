/**
 * Irrigation Controller Software for AVR
 * ModuleName
 *
 * @author Tamas Karpati <tomikaa87@gmail.com>
 * @date 2013.04.08.
 * @file memory.h
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdint.h>

uint16_t StackCount(void);

#endif /* MEMORY_H_ */
