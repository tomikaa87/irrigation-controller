/**
 * Irrigation Controller Software for AVR
 * Main source file
 *
 * @author Tamas Karpati <tomikaa87@gmail.com>
 * @date 2013.04.08.
 * @file main.cpp
 */

#include "driver/hd44780.h"
#include "ui/ui.h"

#include <avr/wdt.h>

/*** Globals ******************************************************************/

/******************************************************************************/

int main()
{
    // Enable watchdog timer, 2 seconds overflow
    //wdt_enable(WDTO_2S);

    // Initialize user interface
    ui::UI ui;

    for (;;) {
        // Run UI task
        switch (ui.task()) {
        case ui::UI::NoAction:
            break;
        }

        // Clear watchdog timer
        wdt_reset();
    }

    return 0;
}

