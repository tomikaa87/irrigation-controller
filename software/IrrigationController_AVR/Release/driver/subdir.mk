################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../driver/hd44780.cpp \
../driver/i2c.cpp \
../driver/keypad.cpp \
../driver/pcf8583.cpp 

OBJS += \
./driver/hd44780.o \
./driver/i2c.o \
./driver/keypad.o \
./driver/pcf8583.o 

CPP_DEPS += \
./driver/hd44780.d \
./driver/i2c.d \
./driver/keypad.d \
./driver/pcf8583.d 


# Each subdirectory must supply rules for building sources it contributes
driver/%.o: ../driver/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega32 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


