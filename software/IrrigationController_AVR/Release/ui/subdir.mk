################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../ui/mainscreen.cpp \
../ui/ui.cpp 

OBJS += \
./ui/mainscreen.o \
./ui/ui.o 

CPP_DEPS += \
./ui/mainscreen.d \
./ui/ui.d 


# Each subdirectory must supply rules for building sources it contributes
ui/%.o: ../ui/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega32 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


