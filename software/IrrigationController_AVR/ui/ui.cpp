/**
 * Irrigation Controller Software for AVR
 * User Interface main module
 *
 * @author Tamas Karpati <tomikaa87@gmail.com>
 * @date 2013.04.08.
 * @file ui.cpp
 */

#include "ui.h"
#include "../memory.h"

#include <util/delay.h>

namespace ui
{

/*** Constructor **************************************************************/

UI::UI()
{
    // Load configuration from RTC SRAM or load defaults on error
    loadConfigFromRtc();

    m_lcd.printlog_P(PSTR("Stack free: %5d"), StackCount());
}

/*** API methods **************************************************************/

UI::Result UI::task()
{
    // Handle key press
    uint8_t key = m_keypad.task();
    static uint32_t presses = 0;

    switch (key) {
    case 0:
        break;

    case 0x11:
        m_config.lcdBacklight++;
        m_lcd.printlog_P(PSTR("lcd bl: %d"), m_config.lcdBacklight);
        m_lcd.setBackightLevel(m_config.lcdBacklight);
        break;

    case 0x21:
        m_config.lcdBacklight--;
        m_lcd.printlog_P(PSTR("lcd bl: %d"), m_config.lcdBacklight);
        m_lcd.setBackightLevel(m_config.lcdBacklight);
        break;

    case 0x12:
        m_config.lcdContrast++;
        m_lcd.printlog_P(PSTR("lcd cont: %d"), m_config.lcdContrast);
        m_lcd.setContrast(m_config.lcdContrast);
        break;

    case 0x22:
        m_config.lcdContrast--;
        m_lcd.printlog_P(PSTR("lcd cont: %d"), m_config.lcdContrast);
        m_lcd.setContrast(m_config.lcdContrast);
        break;

    case 0x13:
        loadConfigFromRtc();
        break;

    case 0x23:
        saveConfigToRtc();
        break;

    default:
        m_lcd.printlog_P(PSTR("(%lu) Key press: %02x"), ++presses, key);
    }

    return NoAction;
}

/*** Private stuff ************************************************************/

void UI::loadDefaultConfig()
{
    m_config.lcdBacklight = 90;
    m_config.lcdContrast = 225;
}

void UI::loadConfigFromRtc()
{
    union {
        Config cfg;
        uint8_t data[sizeof (Config)];
    } u;

    // Read checksum
    uint8_t checksum = m_rtc.readReg(PCF8583::ConfigChecksum);

    // Read configuration data
    if (!m_rtc.readRegArray(PCF8583::ConfigData, u.data, sizeof (Config)) ||
            checksum != calcConfigChecksum(u.data, sizeof (Config))) {
        // Load default configuration and save it in the RTC SRAM
        loadDefaultConfig();
        saveConfigToRtc();
    } else {
        m_config = u.cfg;
    }

    // Apply loaded configuration
    applyConfig();
}

void UI::saveConfigToRtc()
{
    // Read config data
    union {
        Config cfg;
        uint8_t data[sizeof (Config)];
    } u;
    u.cfg = m_config;

    // Calculate checksum
    uint8_t checksum = calcConfigChecksum(u.data, sizeof (Config));

    // Save configuraton in the RTC SRAM
    if (!m_rtc.writeRegArray(PCF8583::ConfigData, u.data, sizeof (Config))) {
        m_lcd.log_P(PSTR("Config save fail"));
    }

    // Save configuration data checksum
    if (!m_rtc.writeReg(PCF8583::ConfigChecksum, checksum)) {
        m_lcd.log_P(PSTR("Cksum save fail"));
    }
}

void UI::applyConfig()
{
    // Setup LCD
    m_lcd.setBackightLevel(m_config.lcdBacklight);
    m_lcd.setContrast(m_config.lcdContrast);
}

uint8_t UI::calcConfigChecksum(uint8_t* data, uint8_t len)
{
    uint8_t sum = 0xAB;
    while (len--) {
        sum ^= *data++;
    }
    return sum;
}

} /* namespace ui */
