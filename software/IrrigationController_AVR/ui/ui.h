/**
 * Irrigation Controller Software for AVR
 * User Interface main module
 *
 * @author Tamas Karpati <tomikaa87@gmail.com>
 * @date 2013.04.08.
 * @file ui.h
 */

#ifndef UI_H_
#define UI_H_

#include "../driver/hd44780.h"
#include "../driver/keypad.h"
#include "../driver/pcf8583.h"

namespace ui
{

class UI
{
public:
    UI();

    enum Result {
        NoAction
    };

    Result task();

private:
    HD44780 m_lcd;
    Keypad m_keypad;
    PCF8583 m_rtc;

    struct Config {
        // LCD settings
        uint8_t lcdBacklight;
        uint8_t lcdContrast;
    };
    Config m_config;

    void loadDefaultConfig();
    void loadConfigFromRtc();
    void saveConfigToRtc();
    void applyConfig();
    uint8_t calcConfigChecksum(uint8_t *data, uint8_t len);
};

} /* namespace ui */
#endif /* UI_H_ */
