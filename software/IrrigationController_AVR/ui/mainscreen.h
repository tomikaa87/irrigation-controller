/**
 * Irrigation Controller Software for AVR
 * ModuleName
 *
 * @author Tamas Karpati <tomikaa87@gmail.com>
 * @date 2013.04.08.
 * @file mainscreen.h
 */

#ifndef MAINSCREEN_H_
#define MAINSCREEN_H_

#include "screeniface.h"

namespace ui
{

class MainScreen: public ui::ScreenIface
{
public:
    MainScreen();

    void draw();
    void update();
    uint8_t keyPress(uint8_t key);
};

} /* namespace ui */
#endif /* MAINSCREEN_H_ */
