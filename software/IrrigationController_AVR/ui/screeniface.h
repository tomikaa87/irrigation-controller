/**
 * Irrigation Controller Software for AVR
 * Interface for UI Screens
 *
 * @author Tamas Karpati <tomikaa87@gmail.com>
 * @date 2013.04.08.
 * @file screeniface.h
 */

#ifndef SCREENIFACE_H_
#define SCREENIFACE_H_

namespace ui
{

#include <stdint.h>

class ScreenIface
{
public:
    ScreenIface() {}
    virtual ~ScreenIface();

    virtual void draw() = 0;
    virtual void update() = 0;
    virtual uint8_t keyPress(uint8_t key) = 0;
};

} /* namespace ui */
#endif /* SCREENIFACE_H_ */
