#include <TimerOne.h>

int InputPin = 3;
TimerOne g_timer;
unsigned long pulseCount = 0;
unsigned long currentPps = 0;
bool updateOutput = false;

void timerIsr()
{
  static unsigned long lastPulseCount = 0;
  currentPps = pulseCount - lastPulseCount;
  lastPulseCount = pulseCount;
  
  updateOutput = true;
}

void setup() 
{ 
  g_timer.initialize();
  g_timer.attachInterrupt(timerIsr);
  
  Serial.begin(115200);
  while (!Serial) continue;
  
  pinMode(InputPin, INPUT_PULLUP);
  
  Serial.println("Initialization finished");
}

void loop() 
{
   byte input = digitalRead(InputPin);
   static bool lastInputState = false;
   
   if (input && !lastInputState)
     ++pulseCount;
   lastInputState = input;
   
   if (updateOutput)
   {
     updateOutput = false;
     Serial.print(currentPps);
     Serial.print(" pps (total: ");
     Serial.print(pulseCount);
     Serial.println(")");
   }
}
