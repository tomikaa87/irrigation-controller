/**
 * Irrigation System - Sensor Control module
 * Water Detection Module
 *
 * @author Tamas Karpati
 * @date 2013-05-20
 */

#ifndef WATERDETECT_H
#define	WATERDETECT_H

#include <stdint.h>

void wd_start();
uint8_t wd_measure();
void wd_stop();

#endif	/* WATERDETECT_H */

