/**
 * Irrigation System - Sensor Control module
 * Water Detection Module
 *
 * @author Tamas Karpati
 * @date 2013-05-20
 */

#include "waterdetect.h"
#include "config.h"

/*** API functions ************************************************************/

void wd_start()
{
    // Setup probe pin as digital input
    WDM_PROBE_TRIS = 1;

    // Setup CCP module for 4 kHz, 50% DC measurement signal
    CCP_PIN_TRIS = 0;
    CCP_TMR_CON = 0b00000101;
    CCP_TMR_PR = 0b11111001;
    CCP_CCPCON = 0b00111100;
    CCP_CCPRL = 0b01111100;
}

uint8_t wd_measure()
{
    static const uint8_t MeasureCount = 50;
    uint8_t i, no_water = 0;

    for (i = 0; i < MeasureCount; i++) {
        no_water |= WDM_PROBE_PIN;
        if (no_water) {
            break;
        }
        __delay_ms(1);
    }

    return no_water;
}

void wd_stop()
{
    // Shutdown CCP module
    CCP_CCPCON = 0;
    CCP_CCPRL = 0;
    CCP_TMR_CON = 0;
    CCP_TMR_PR = 0;
    CCP_PIN_TRIS = 1;
}