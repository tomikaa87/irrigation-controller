/**
 * Irrigation System - Sensor Control module
 * Configuration header
 *
 * @author Tamas Karpati
 * @date 2013-05-20
 */

#ifndef CONFIG_H
#define	CONFIG_H

#include <htc.h>

/*** PIC configuration ********************************************************/

#ifndef _XTAL_FREQ
#define _XTAL_FREQ                      16000000ul
#endif

/*** Water Detection Module configuration *************************************/

#define WDM_PROBE_TRIS                  TRISCbits.TRISC3
#define WDM_PROBE_PIN                   PORTCbits.RC3
#define CCP_PIN_TRIS                    TRISAbits.TRISA2
#define CCP_TMR_PR                      PR2
#define CCP_TMR_CON                     T2CON
#define CCP_CCPCON                      CCP3CON
#define CCP_CCPRL                       CCPR3L

/*** Dallas 1-Wire configuration **********************************************/

#define OW_DQ_IN                        PORTCbits.RC2
#define OW_DQ_OUT                       LATCbits.LATC2
#define OW_DQ_TRIS                      TRISCbits.TRISC2

/*** I2C configuration ********************************************************/

/*** UART configuration *******************************************************/

#endif	/* CONFIG_H */

