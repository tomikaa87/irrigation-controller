/**
 * Irrigation System - Sensor Control module
 * Main source
 *
 * @author Tamas Karpati
 * @date 2013-05-20
 */

#include <htc.h>

/*** PIC configuration ********************************************************/

__CONFIG(FOSC_INTOSC & WDTE_OFF & PWRTE_ON & MCLRE_OFF & CP_ON & CPD_ON &
        BOREN_ON & CLKOUTEN_OFF & IESO_OFF & FCMEN_ON);
__CONFIG(WRT_ALL & PLLEN_OFF & STVREN_ON & BORV_LO & LVP_OFF);

/******************************************************************************/

#include "config.h"
#include "waterdetect.h"

void setup()
{
    // Setup internal oscillator, 16 MHz
    OSCCON = 0b01111010;

    ANSELA = 0;
    ANSELC = 0;
}

void main()
{
    setup();

    LATC1 = 0;
    TRISC1 = 0;

//    LATC1 = 1;
//    uint8_t i;
//    for (i = 0; i < 100; i++) {
//        __delay_ms(10);
//    }
//    LATC1 = 0;

    wd_start();
    for (;;) {
        
        LATC1 = wd_measure() ? 1 : 0;
        __delay_ms(10);
    }
}