/*******************************************************************************
  I2C Generated Driver File

  Company:
    Microchip Technology Inc.

  File Name:
    i2c.c

  Summary:
    This is the generated driver implementation file for the I2C driver using MPLAB� Code Configurator

  Description:
    This source file provides implementations for driver APIs for I2C.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v1.1
        Device            :  PIC12F1840
        Version           :  1.1
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.30
        MPLAB             :  MPLAB X 2.0
*******************************************************************************/

/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/


#include <xc.h>
#include "i2c.h"

/************* THIS IS THE USER GENERATED I2C SOURCE FILE ************************************/

#define I2C_READ_HEAD_SIZE (8)
#define I2C_WRITE_HEAD_SIZE (8)
#define I2C_READ_HEAD_MASK (I2C_READ_HEAD_SIZE-1)
#define I2C_WRITE_HEAD_MASK (I2C_WRITE_HEAD_SIZE-1)

uint8_t i2cReadBuffer[I2C_READ_HEAD_SIZE];
uint8_t i2cWriteBuffer[I2C_WRITE_HEAD_SIZE];

uint8_t i2cCpuWriteHeadIndex = 0;
uint8_t i2cCpuReadHeadIndex = 0;
uint8_t i2cModuleReadHeadIndex = 0;
uint8_t i2cModuleWriteHeadIndex = 0;

union
{
   uint16_t address10Bit;
   struct
    {
        unsigned lowAddressByte: 8;
        unsigned highAddressByte: 8;
    };
}i2cSlaveAddress;

uint8_t i2cFlag = 0;

/**
  Prototype:        void I2C_Init(void)
  Input:            none
  Output:           none
  Description:      custom init function for Init
  Comment:          
  Usage:            I2C_Init();
*/
void I2C_Init(void)
{
    // set the I2C module to the options selected in the User Interface

    // BF RCinprocess_TXcomplete; UA dontupdate; SMP midsample_enable; P stopbit_notdetected; S startbit_notdetected; R_nW write_noTX; CKE tx_on_idle_to_active; D_nA lastbyte_address; 
    SSP1STAT = 0x00;

    // SSPEN enabled; WCOL no_collision; SSPOV no_overflow; CKP lo_hold; SSPM I2CSlave_7bitint; 
    SSP1CON1 = 0x2E;

    // ACKSTAT received; RCEN disabled; RSEN disabled; ACKEN disabled; ACKDT acknowledge; SEN disabled; GCEN disabled; PEN disabled; 
    SSP1CON2 = 0x00;

    // BOEN disabled; AHEN disabled; SBCDE disabled; SDAHT 100nshold; ACKTIM ackseq; DHEN disabled; PCIE disabled; SCIE disabled; 
    SSP1CON3 = 0x00;

    // SSPBUF 0x0; 
    SSP1BUF = 0x00;

    // SSPMSK 0x0; 
    SSP1MSK = 0x00;

    // SSPADD 224; 
    SSP1ADD = 0xE0;


    PIE1bits.SSP1IE = 1;
}

/**
  Prototype:        void I2C_ReadFrame(void)
  Input:            none
  Output:           none
  Description:      This function is used to read from the I2C bus and store into I2C read buffer. This is a non blocking function.
  Usage:            I2C_ReadFrame();
*/
void I2C_ReadFrame(void)
{
    i2cReadBuffer[i2cModuleWriteHeadIndex++] = SSPBUF;
    SSP1CON1bits.CKP = 1;
    i2cModuleWriteHeadIndex &= I2C_READ_HEAD_MASK;
}

/**
  Prototype:        void I2C_WriteFrame(void)
  Input:            none
  Output:           none
  Description:      This function will write the content of I2C write buffer into the I2C bus. This is a non blocking function.
  Usage:            I2C_WriteFrame();
*/
void I2C_WriteFrame(void)
{
    if (!SSP1CON2bits.ACKSTAT)
    {
        SSPBUF = i2cWriteBuffer[i2cModuleReadHeadIndex++];
        SSP1CON1bits.CKP = 1;
        i2cModuleReadHeadIndex &= I2C_WRITE_HEAD_MASK;
    }
}

/**
  Prototype:        void I2C_WriteTo_i2cWriteBuffer(uint8_t i2cData)
  Input:            i2cData : byte to be written in the buffer
  Output:           none
  Description:      This function will write a byte into the i2c write buffer.
  Usage:            I2C_WriteTo_i2cWriteBuffer(i2cData);
*/
void I2C_WriteTo_i2cWriteBuffer(uint8_t i2cData)
{
    i2cCpuWriteHeadIndex &= I2C_WRITE_HEAD_MASK;
    i2cWriteBuffer[i2cCpuWriteHeadIndex++] = i2cData;
}

/**
  Prototype:        uint8_t I2C_ReadFrom_i2cReadBuffer(void)
  Input:            none
  Output:           Data byte from I2C read buffer
  Description:      This function will read a byte from the I2C read buffer.
  Usage:            I2C_ReadFrom_i2cReadBuffer();
*/
uint8_t I2C_ReadFrom_i2cReadBuffer(void)
{
    i2cCpuReadHeadIndex &= I2C_READ_HEAD_MASK;
    return (i2cReadBuffer[i2cCpuReadHeadIndex++]);
}

/**
  Prototype:        bool I2C_IsWriteBufferFull(void)
  Input:            none
  Output:           'TRUE' : I2C write buffer is full
                    'FALSE' : I2C write buffer is not full
  Description:      This function will check whether the I2C write buffer is full or not.
  Usage:            I2C_IsWriteBufferFull();
*/
bool I2C_IsWriteBufferFull(void)
{
    if (i2cCpuWriteHeadIndex > I2C_WRITE_HEAD_MASK)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
  Prototype:        bool I2C_IsDataAvailableInReadBuffer(void)
  Input:            none
  Output:           'TRUE' : Data is available in the I2C read buffer
                    'FALSE' : Data is not available in the I2C read buffer
  Description:      This function will check whether data is available in the I2C read buffer.
  Usage:            I2C_IsDataAvailableInReadBuffer();
*/
bool I2C_IsDataAvailableInReadBuffer(void)
{
    if (i2cModuleWriteHeadIndex == i2cCpuReadHeadIndex)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
  Prototype:        void I2C_ISR(void)
  Input:            none
  Output            none
  Description:      I2C Interrupt Service Routine. Called by the Interrupt Manager.
                    Place your I2C Interrupt code here.
  Usage:            I2C_ISR;
*/
void I2C_ISR(void)
{
    uint8_t receivedByte;
    PIR1bits.SSP1IF = 0;			// Clear interrupt flag
    if (SSP1STATbits.BF)				// check whether Buffer is full
    {
        if (SSP1STATbits.D_nA)		// check whether last byte received was a data byte
        {
            I2C_ReadFrame();
            i2cFlag = 0;
        }
        else
        {
            receivedByte = SSPBUF;
            if (SSP1STATbits.UA)		// In 10 bit addressing mode check for UA(update address) bit
            {
                if (!i2cFlag)
                {
                    SSPADD = i2cSlaveAddress.lowAddressByte;		// load lower byte address
                    i2cFlag = 1;
                }
                else
                {
                    SSPADD = i2cSlaveAddress.highAddressByte;		// load higher byte address
                }
            }
            else
            {
                if (SSP1STATbits.R_nW)
                {
                    I2C_WriteFrame();
                    i2cFlag = 0;
                }
                else
                {
                    I2C_ReadFrame();
                }
            }
        }
    }
    else
    {
        if (!SSP1CON2bits.ACKSTAT && SSP1STATbits.R_nW)
        {
            I2C_WriteFrame();
        }
    }
}

