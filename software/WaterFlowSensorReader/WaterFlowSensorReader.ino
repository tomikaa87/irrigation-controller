#include <TimerOne.h>
#include <stdint.h>

// --- Configuration

int SensorInputPin = 3;
int LedPin = 13;

// --- Globals

bool g_updateNeeded = false;

typedef union
{
     struct {
         uint16_t pps;
         uint32_t total;
     };
     
     byte data[sizeof (unsigned long) * 2];
} Data;
Data g_data = {0};

// --- Interrput handling

void timerIsr()
{
    static unsigned long lastPulseCount = 0;
    g_data.pps = g_data.total - lastPulseCount;
    lastPulseCount = g_data.total;
  
    g_updateNeeded = true;
}

// --- Setup

void setup() 
{
    static TimerOne timer; 
    timer.initialize();
    timer.attachInterrupt(timerIsr);
    
    Serial.begin(115200);
    while (!Serial) continue;
    
    pinMode(SensorInputPin, INPUT_PULLUP);
    pinMode(LedPin, OUTPUT);
}

// --- Serial communications

enum {
    PKT_INVALID = 0,
    PKT_WFSDATA         // Water flow sensor data
} SerialPacketTypes;

typedef union {
    struct _fields {
        uint8_t Type;
        uint8_t Length;
        uint8_t Channel;
        uint8_t Checksum;
    } Fields;

    uint8_t Data[sizeof (struct _fields)];
} SerialPacketHeader;

typedef union {
    struct _fields {
        uint16_t PPS;
        uint32_t Total;
    } Fields;
    
    uint8_t Data[sizeof (_fields)];
} WaterFlowSensorData;

void serialWritePacket()
{
    SerialPacketHeader header = {0};
    header.Fields.Type = PKT_WFSDATA;
    header.Fields.Length = sizeof (uint16_t) + sizeof (uint32_t);
    header.Fields.Channel = 0;
    header.Fields.Checksum = 0x55;
    
    WaterFlowSensorData data = {0};
    data.Fields.PPS = g_data.pps;
    data.Fields.Total = g_data.total;
    
    for (uint8_t i = 0; i < sizeof (WaterFlowSensorData); ++i)
        header.Fields.Checksum ^= data.Data[i];
        
    Serial.write(0xF0);
    Serial.write(0x0D);
    
    Serial.write(header.Data, sizeof (SerialPacketHeader));
    Serial.write(data.Data, sizeof (WaterFlowSensorData));
}

// --- Main loop

void loop() 
{
    
   byte inputState = digitalRead(SensorInputPin);
   static bool lastInputState = false;
   if (inputState && !lastInputState)
       ++g_data.total;
   lastInputState = inputState;
   
   if (g_updateNeeded)
   {
       digitalWrite(LedPin, g_data.pps > 0 ? 1 : 0);
     
       g_updateNeeded = false;
       serialWritePacket();
   }
}
