/**
 * Irrigation System - Control module
 * Alarm system
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#include "alarm.h"
#include "drivers/lcd_hd44780.h"
#include "global.h"
#include "drivers/buzzer.h"
#include "displaymanager.h"
#include "drivers/led.h"
#include "ui/ui.h"

#include <stdint.h>

//------------------------------------------------------------------------------
// Forward declarations
//------------------------------------------------------------------------------

static void prepareAlarm();
static void beepIfNecessary();
static void printHeader();

#define isAlarmUnseen(ALARM) \
    (g_isAlarmActive || (g_lastAlarm == ALARM && !g_isLastAlarmSeen))

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

bit g_isAlarmActive = 0;
bit g_isLastAlarmSeen = 0;
static tick_t g_showTimeoutTicks = 0;
static uint8_t g_lastLCDModeFlags = 0;

typedef enum
{
    ALARM_NONE,
    ALARM_EMPTY_TANK,
    ALARM_PUMP_FAILURE,
    ALARM_WATER_TOO_COLD
} EAlarm;
static EAlarm g_lastAlarm = ALARM_NONE;

//------------------------------------------------------------------------------
// API
//------------------------------------------------------------------------------

void AlarmTask()
{
    if (g_isAlarmActive &&
        TicksElapsed(g_showTimeoutTicks) > GlobalData.Settings.Alarm.Timeout * 100)
    {
        AlarmDismiss();
        LEDFlash(LED1);
        g_isLastAlarmSeen = 0;
    }
    else if (!g_isAlarmActive && !g_isLastAlarmSeen &&
             TicksElapsed(g_showTimeoutTicks) > GlobalData.Settings.Alarm.Interval * 100)
    {
        g_isLastAlarmSeen = 1;

        switch (g_lastAlarm)
        {
            case ALARM_EMPTY_TANK:
                AlarmEmptyTank();
                break;

            default:
                break;
        }

        g_isLastAlarmSeen = 0;
    }
}

//--------------------------------------------------------------------
void AlarmDismiss()
{
    DisplayStopBlinking();
    g_isAlarmActive = 0;
    g_showTimeoutTicks = TicksGet();
    g_isLastAlarmSeen = 1;

    LCDSetDisplayMode(g_lastLCDModeFlags);
    UIRedrawCurrentScreen();

    LEDDisable(LED1);
}

//--------------------------------------------------------------------
void AlarmEmptyTank()
{
    if (isAlarmUnseen(ALARM_EMPTY_TANK))
        return;

    prepareAlarm();
    g_lastAlarm = ALARM_EMPTY_TANK;

    LCDPuts("Water tank is empty.");
    LCDMoveCursor(0, 2);
    LCDPuts("Irrigation is paused");
    LCDMoveCursor(0, 3);
    LCDPuts("until next refill.");

    beepIfNecessary();
}

//--------------------------------------------------------------------
void AlarmPumpFailure(uint8_t pump)
{
    if (isAlarmUnseen(ALARM_PUMP_FAILURE))
        return;

    prepareAlarm();
    g_lastAlarm = ALARM_PUMP_FAILURE;

    LCDPrintf("Pump %u is failing.", pump + 1);
    LCDMoveCursor(0, 2);
    LCDPuts("Check connections,");
    LCDMoveCursor(0, 3);
    LCDPuts("pipes, water level.");

    beepIfNecessary();
}

//--------------------------------------------------------------------
void AlarmWaterTooCold()
{
    if (isAlarmUnseen(ALARM_WATER_TOO_COLD))
        return;

    prepareAlarm();
    g_lastAlarm = ALARM_WATER_TOO_COLD;

    LCDPuts("Water is too cold.");
    LCDMoveCursor(0, 2);
    LCDPuts("Don't let water");
    LCDMoveCursor(0, 3);
    LCDPuts("freeze in the system");

    beepIfNecessary();
}

//------------------------------------------------------------------------------
// Private stuff
//------------------------------------------------------------------------------

static void prepareAlarm()
{
    g_isAlarmActive = 1;
    
    g_lastLCDModeFlags = LCDGetDisplayMode();
    LCDSetDisplayMode(LCD_DM_ENABLED);
    
    printHeader();

    if (GlobalData.Settings.Alarm.BlinkingEnabled)
    {
        DisplayBlink(8);
    }

    g_showTimeoutTicks = TicksGet();
}

//--------------------------------------------------------------------
static void beepIfNecessary()
{
    if (GlobalData.Settings.Alarm.BeepLevel != BV_SILENT)
    {
        for (uint8_t i = 0; i < 8; ++i)
        {
            BuzzerBeep((EBuzzerVolume)GlobalData.Settings.Alarm.BeepLevel);
            if (i < 7)
                for (uint8_t j = 0; j < 5; ++j, __delay_ms(10));
        }
    }
}

//--------------------------------------------------------------------
static void printHeader()
{
    LCDClear();
    LCDPuts("====== ALARM! ======");
    LCDMoveCursor(0, 1);
}