/**
 * Irrigation System - Control module
 * Alarm system
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#ifndef ALARM_H
#define	ALARM_H

#include <stdint.h>

void AlarmTask();
void AlarmDismiss();

extern bit g_isAlarmActive;
#define AlarmIsActive()             g_isAlarmActive

extern bit g_isLastAlarmSeen;
#define AlarmIsLastSeen()           g_isLastAlarmSeen

void AlarmEmptyTank();
void AlarmPumpFailure(uint8_t pump);
void AlarmWaterTooCold();

#endif	/* ALARM_H */

