/**
 * Irrigation System - Control module
 * I2C peripheral module
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */
 
#ifndef I2C_H
#define I2C_H

#include <stdint.h>

void I2CInit();
void I2CStart();
void I2CStop();
void I2CRestart();
void I2CAck();
void I2CNak();
void I2CWait();
void I2CWrite(uint8_t data);
uint8_t I2CRead();

#endif // I2C_H