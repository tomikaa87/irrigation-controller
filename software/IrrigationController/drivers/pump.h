/**
 * Irrigation System - Control module
 * Pump control module
 *
 * @author Tamas Karpati
 * @date 2013-04-21
 */

#ifndef PUMP_H
#define	PUMP_H

#include <stdint.h>
#include "../global.h"

/*
 * General functions
 */
void PumpInit();
uint16_t PumpCalcRunTime(uint8_t pump, uint16_t amountDl);

/*
 * High level control functions. Use these functions in order to track water usage.
 */
uint8_t PumpStart(uint8_t pump);
void PumpStop(uint8_t pump);
inline tick_t PumpGetRunTime(uint8_t pump);
inline uint8_t PumpIsRunning(uint8_t pump);

/*
 * Low level control functions. WARNING: usage of these functions won't affect the water gauge!
 */
void PumpSetPWM(uint8_t pump, uint8_t dutyCycle);
uint8_t PumpGetPWM(uint8_t pump);

/*
 * Calibration control functions
 */
uint8_t PumpStartCalibration(uint8_t pump);
uint8_t PumpStopCalibration();

#endif	/* PUMP_H */

