/**
 * Irrigation System - Control module
 * PCF8583P based RTC driver
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */
 
#ifndef RTC_PCF8583P_H
#define RTC_PCF8583P_H

#include <time.h>

/**
 * Register addresses (clock mode)
 */
#define RTC_ADDR_CONTROL                0x00
#define RTC_ADDR_HUNDSECONDS            0x01
#define RTC_ADDR_SECONDS                0x02
#define RTC_ADDR_MINUTES                0x03
#define RTC_ADDR_HOURS                  0x04
#define RTC_ADDR_YEAR_DATE              0x05
#define RTC_ADDR_WEEKDAY_MONTH          0x06
#define RTC_ADDR_TIMER                  0x07
#define RTC_ADDR_ALARM_CTRL             0x08
#define RTC_ADDR_ALARM_HUNDSECONDS      0x09
#define RTC_ADDR_ALARM_SECONDS          0x0A
#define RTC_ADDR_ALARM_MINUTES          0x0B
#define RTC_ADDR_ALARM_HOURS            0x0C
#define RTC_ADDR_ALARM_DATE             0x0D
#define RTC_ADDR_ALARM_MONTH            0x0E
#define RTC_ADDR_ALARM_TIMER            0x0F
// Custom addresses
#define RTC_ADDR_YEAR_LO                0x10
#define RTC_ADDR_YEAR_HI                0x11
#define RTC_ADDR_USER_RAM_BASE          0x12

void RTCInit();
void RTCTask();

// Low level functions
void RTCWriteReg(unsigned char address, unsigned char data);
void RTCWriteRegArray(unsigned char address, unsigned char *buffer, unsigned char length);
unsigned char RTCReadReg(unsigned char address);
void RTCReadRegArray(unsigned char address, unsigned char *buffer, unsigned char length);

// High level functions
void RTCSetDate(unsigned int year, unsigned char month, unsigned char day, unsigned char weekday);
void RTCGetDate(unsigned int *oYear, unsigned char *oMonth, unsigned char *oDay);
unsigned char RTCCalcWeekOfYear(unsigned int dayOfYear, unsigned char jan1DayOfYear);
void RTCGetTime(unsigned char *oHour, unsigned char *oMinute, unsigned char *oSecond);
void RTCSetTime(unsigned char hour, unsigned char minute, unsigned char second);
void RTCSetAlarmState(unsigned char iEnabled);

// Converter functions
unsigned char RTCToDEC(unsigned char bcdValue);
unsigned char RTCToBCD(unsigned char decimalValue);

time_t RTCReadTime_t(time_t *t);

#endif // RTC_PCF8583P_H