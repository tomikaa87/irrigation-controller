#include "serial.h"
#include "../config.h"
#include "../serialparser.h"

#include <xc.h>
#include <stdint.h>

#define SERIAL_BAUD_RATE                115200ul
#define SERIAL_HIGH_SPEED               1
#define SERIAL_RX_BUF_SIZE              32

#ifndef _XTAL_FREQ
#warning _XTAL_FREQ must be defined
#endif

#if (SERIAL_HIGH_SPEED == 1)
    #define SERIAL_BRG_DIV              16ul
#else
    #define SERIAL_BRG_DIV              64ul
#endif
#define SERIAL_SPBRG_VAL \
    (unsigned char)(_XTAL_FREQ / SERIAL_BAUD_RATE / SERIAL_BRG_DIV - 1)
#define SERIAL_REAL_BAUD \
    (unsigned long)(_XTAL_FREQ / (SERIAL_BRG_DIV * (SERIAL_SPBRG_VAL + 1)))
#define SERIAL_BAUD_ERROR \
    (signed char)((((float)SERIAL_REAL_BAUD - (float)SERIAL_BAUD_RATE) / (float)SERIAL_BAUD_RATE) * 100.0)

// Baud rate error indicator
char Warning_SerialBaudErrorTooHigh[SERIAL_BAUD_ERROR <= 3];

volatile char _rxBuf[SERIAL_RX_BUF_SIZE] = {0, };
volatile uint8_t _rxBufNextIn = 0;
volatile uint8_t _rxBufNextOut = 0;
#define _rxBufHasChar \
        (_rxBufNextIn != _rxBufNextOut)

void SerialInit()
{
    SERIAL_RX_TRIS = 1;
    SERIAL_TX_TRIS = 0;
    SERIAL_RCIE = 1;
    
    // BRG16 = 1
    SERIAL_BAUDCON = 0b0000100;
    // TXEN = 1, BRGH = UART_TX_HIGH_SPEED
    SERIAL_TXSTA = 0b00100000 | (SERIAL_HIGH_SPEED ? 0b100 : 0);
    // SPEN = 1, CREN = 1
    SERIAL_RCSTA = 0b10010000;
    // Set SPBRG
    SERIAL_SPBRG = SERIAL_SPBRG_VAL & 0xFF;
    // Set SPBRGH
    SERIAL_SPBRGH = (SERIAL_SPBRG_VAL >> 8) & 0xFF;
}

void putch(char c)
{
    while (!SERIAL_TRMT)
        continue;
    SERIAL_TXREG = c;
    while (!SERIAL_TRMT)
        continue;
}

char getch()
{
    while (!SERIAL_RCIF)
        continue;
    return SERIAL_RCREG;
}

char getbufch()
{
    char c;

    // Wait untit a byte is ready to read from the USART buffer
    while (!_rxBufHasChar)
        continue;
    c = _rxBuf[_rxBufNextOut];
    // Step to next index
    _rxBufNextOut = (_rxBufNextOut + 1) % SERIAL_RX_BUF_SIZE;
    return c;
}

void putbufch(char c)
{
    uint8_t next_in;

    _rxBuf[_rxBufNextIn] = c;
    next_in = _rxBufNextIn;
    _rxBufNextIn = (_rxBufNextIn + 1) % SERIAL_RX_BUF_SIZE;
    if (_rxBufNextIn == _rxBufNextOut)
        _rxBufNextIn = next_in;
}


void SerialTask()
{
    if (!_rxBufHasChar)
        return;

    ProcessSerialData(getbufch());
}