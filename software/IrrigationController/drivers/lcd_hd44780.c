/**
 * Irrigation System - Control module
 * HD44780 based LCD driver
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */

#include "lcd_hd44780.h"
#include "../config.h"

#include <stdarg.h>
#include <stdio.h>

#ifndef LCD_HD44780_EXT_CFG
    // Pin configuration ...
#endif

static uint8_t g_lastDisplayModeFlags = 0;

/*** Custom characters ********************************************************/

const uint8_t LCDCC_LevelIndicator0[8] = {
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00011111,
};

const uint8_t LCDCC_LevelIndicator1[8] = {
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00011111,
    0b00011111,
};

const uint8_t LCDCC_LevelIndicator2[8] = {
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00011111,
    0b00011111,
    0b00011111,
    0b00011111,
};

const uint8_t LCDCC_LevelIndicator3[8] = {
    0b00000000,
    0b00000000,
    0b00011111,
    0b00011111,
    0b00011111,
    0b00011111,
    0b00011111,
    0b00011111,
};

/*** Internal constants *******************************************************/

/**
 * Commands
 */
#define CMD_CLEAR                       0b00000001
#define CMD_DISP_CUR_HOME               0b00000010
#define CMD_SET_CGRAM_ADDR              0b01000000
#define CMD_SET_DDRAM_ADDR              0b10000000

// The commands below should be used in conjunction with constants defined
// directly after the command

/**
 * Entry modes, display shift
 */
#define CMD_ENTRY_MODE                  0b00000100
#define ENTRY_MODE_INC                  0b00000010
#define DISP_SHIFT_ON                   0b00000001

/**
 * Display switch, cursor mode
 */
#define CMD_DISP_CUR_ONOFF              0b00001000
#define DISPLAY_ON                      0b00000100
#define CUR_MODE_UNDERLINE              0b00000010
#define CUR_MODE_BLINK                  0b00000001

/**
 * Display and cursor shift
 */
#define CMD_DISP_CUR_SHIFT              0b00010000
#define DISPLAY_SHIFT                   0b00001000
#define SHIFT_RIGHT                     0b00000100

/**
 * Function set
 */
#define CMD_FUNC_SET                    0b00100000
#define FUNC_8_BIT                      0b00010000
#define FUNC_2_LINES                    0b00001000
#define FUNC_10_DOT_FORMAT              0b00000100

/**
 * Display addresses
 */
#define LINE_1_ADDR                     0x00
#define LINE_2_ADDR                     0x40
#ifdef LCD_HD44780_4x16
    #define LINE_3_ADDR                 0x10
    #define LINE_4_ADDR                 0x50
#elif defined LCD_HD44780_4x20
    #define LINE_3_ADDR                 0x14
    #define LINE_4_ADDR                 0x54
#endif

/*** Low-level functions ******************************************************/

#define _DELAY_INIT                     __delay_ms(5)
#define _DELAY_CLEAR                    __delay_ms(2)
#define _DELAY_INSTR                    _delay(1)

/**
 * Switches the IO direction to input.
 */
void _read_mode()
{
    LCD_TRIS_D4 = 1;
    LCD_TRIS_D5 = 1;
    LCD_TRIS_D6 = 1;
    LCD_TRIS_D7 = 1;
}

/**
 * Switches the IO direction to output.
 */
void _write_mode()
{
    LCD_TRIS_D4 = 0;
    LCD_TRIS_D5 = 0;
    LCD_TRIS_D6 = 0;
    LCD_TRIS_D7 = 0;
}

/**
 * Reads a byte from the LCD controller.
 */
uint8_t _read_byte(uint8_t rs)
{
    uint8_t low, high;

    _read_mode();

    LCD_PIN_RS = rs;
    _DELAY_INSTR;
    LCD_PIN_RW = 1;
    _DELAY_INSTR;
    LCD_PIN_EN = 1;
    _DELAY_INSTR;
    high = LCD_PIN_D4_I | (LCD_PIN_D5_I << 1) | (LCD_PIN_D6_I << 2) |
            (LCD_PIN_D7_I << 3);
    LCD_PIN_EN = 0;
    _DELAY_INSTR;
    LCD_PIN_EN = 1;
    _DELAY_INSTR;
    low = LCD_PIN_D4_I | (LCD_PIN_D5_I << 1) | (LCD_PIN_D6_I << 2) |
            (LCD_PIN_D7_I << 3);
    LCD_PIN_EN = 0;
    
    _write_mode();

    return ((high << 4) | low);
}

/**
 * Writes a half byte (nibble) to the LCD controller.
 */
void _write_nibble(uint8_t nibble)
{
    LCD_PIN_D4_O = (nibble & 1 ? 1 : 0);
    LCD_PIN_D5_O = (nibble & 2 ? 1 : 0);
    LCD_PIN_D6_O = (nibble & 4 ? 1 : 0);
    LCD_PIN_D7_O = (nibble & 8 ? 1 : 0);
    _DELAY_INSTR;
    LCD_PIN_EN = 1;
    _DELAY_INSTR;
    LCD_PIN_EN = 0;
}

/**
 * Writes a byte to the LCD controller.
 * @param rs pass 0 to command mode, 1 to data mode
 * @param b the command or data byte
 */
void _write_byte(uint8_t rs, uint8_t b)
{
    LCD_PIN_RS = 0;

    // Check if the LCD controller is busy
    while (_read_byte(0) & 0x80)
        continue;
    
    _DELAY_INSTR;
    LCD_PIN_RS = rs;
    _DELAY_INSTR;
    LCD_PIN_RW = 0;
    _DELAY_INSTR;
    LCD_PIN_EN = 0;
    
    _write_nibble(b >> 4);
    _write_nibble(b & 0x0F);
}

/*** API function implementations *********************************************/

/**
 * Initializes the LCD controller.
 */
void LCDInit()
{
    LCD_PIN_RS = 0;
    LCD_PIN_RW = 0;
    LCD_PIN_EN = 0;
    LCD_TRIS_RS = 0;
    LCD_TRIS_RW = 0;
    LCD_TRIS_EN = 0;

    _write_mode();

    _DELAY_INIT;
    _write_nibble(0x03);
    _DELAY_INIT;
    _write_nibble(0x03);
    _DELAY_INIT;
    _write_nibble(0x03);
    _DELAY_INIT;
    _write_nibble(0x02);
    _DELAY_INIT;

    _write_byte(0, CMD_FUNC_SET | FUNC_2_LINES);
    _write_byte(0, CMD_DISP_CUR_ONOFF | DISPLAY_ON);
    g_lastDisplayModeFlags = LCD_DM_ENABLED;
    _write_byte(0, CMD_CLEAR);
    _DELAY_CLEAR;

    LCD_TRIS_LED = 0;
    CCP4CON = 0b00001100;
    LCDSetBacklight(0x80);
}

/**
 * Clear the screen.
 */
void LCDClear()
{
    _write_byte(0, CMD_CLEAR);
    _DELAY_CLEAR;
}

/**
 * Puts a character on the screen.
 * @param c the character
 */
void LCDPutch(char c)
{
    if (c == '\b') {
        // Decrement address by 1
        LCDShift(LCD_SH_CURSOR | LCD_SH_LEFT);
        return;
    }

    _write_byte(1, c);
}

/**
 * Puts a string on the screen.
 * @param s the string
 */
void LCDPuts(const char *s)
{
    // Send the string char-by-char
    while (*s)
        LCDPutch(*s++);
}

/**
 * Reads a byte from the DDRAM at the current position.
 */
char LCDGetch()
{
    // Check if the LCD controller is busy
    while (_read_byte(0) & 0x80)
        continue;

    return _read_byte(1);
}

/**
 * Positions the cursor to the given coordinates
 * @param x character (row) number, starting from 0
 * @param y line number, starting from 0
 */
void LCDMoveCursor(uint8_t x, uint8_t y)
{
    uint8_t addr;

    // Calculate DDRAM address
    switch (y) {
        case 0:
            addr = LINE_1_ADDR;
            break;
        case 1:
            addr = LINE_2_ADDR;
            break;
#if (defined LCD_HD44780_4x16 || defined LCD_HD44780_4x20)
        case 2:
            addr = LINE_3_ADDR;
            break;
        case 3:
            addr = LINE_4_ADDR;
            break;
#endif
        default:
            return;
    }
    addr += x;

    // Set DDRAM address
    _write_byte(0, addr | 0x80);
}

inline void LCDMoveCursorToPos(EPoint pos)
{
    LCDMoveCursor(pos.x, pos.y);
}

/**
 * Puts a custom character into the CGRAM
 * @param place CGRAM address
 * @param data custom character data
 */
void LCDDefineChar(uint8_t place, const uint8_t *data)
{
    uint8_t addr, i;

    // Check place number
    if (place > 7)
        return;

    // Set CGRAM address
    addr = 0x40 | (place << 3);
    _write_byte(0, addr);

    // Write custom character data
    for (i = 0; i < 8; i++)
        _write_byte(1, data[i]);
}

/**
 * Shifts the cursor or the whole screen.
 * @param display if 1 it will shift the screen, otherwise the cursor
 * @param right if 1 it will shift right, otherwise left
 */
void LCDShift(uint8_t shiftFlags)
{
    //uint8_t instr = CMD_DISP_CUR_SHIFT;

    //if (iFlags & LCD_SH_DISPLAY)
    //    instr |= DISPLAY_SHIFT;
    //if (iFlags & LCD_SH_RIGHT)
    //    instr |= SHIFT_RIGHT;

    _write_byte(0, CMD_DISP_CUR_SHIFT | shiftFlags);
}

/**
 * Turns the display on or off and sets the cursor mode.
 * @param disp_on if 1 it will turn on the display, otherwise it will turn off
 * @param cur_underline if 1 the cursor will be an underline
 * @param cur_blink if 1 the cursor will blink
 */
void LCDSetDisplayMode(uint8_t modeFlags)
{
    //uint8_t instr = CMD_DISP_CUR_ONOFF;

    //if (iFlags)
    //    instr |= DISPLAY_ON;
    //if (iUnderlineCur)
    //    instr |= CUR_MODE_UNDERLINE;
    //if (iBlinkingCur)
    //    instr |= CUR_MODE_BLINK;

    g_lastDisplayModeFlags = modeFlags;
    _write_byte(0, CMD_DISP_CUR_ONOFF | modeFlags);
}

uint8_t LCDGetDisplayMode()
{
    return g_lastDisplayModeFlags;
}

/**
 * Sets several options of the LCD controller.
 * @param data_8bit if 1, 8 bit data transfer will be used
 * @param disp_2lines if 1, 2 lines mode will be enabled
 * @param chars_10dots if 1, 5x10 dot character mode will be enabled
 */
void LCDSetFunc(uint8_t funcFlags)
{
    //uint8_t instr = CMD_FUNC_SET;

    //if (iFlags)
    //    instr |= FUNC_8_BIT;
    //if (i2lines)
    //    instr |= FUNC_2_LINES;
    //if (i10dots)
    //    instr |= FUNC_10_DOT_FORMAT;

    _write_byte(0, CMD_FUNC_SET | funcFlags);
}

/**
 * Draws a fancy progress bar on the screen.
 * @param x the x coordinate of the bar
 * @param y the y coordinate of the bar
 * @param width width of the bar
 * @param pos current position (in percent)
 * @param first_cc CGRAM address of the first custom character used to
 *                 draw the bar
 */
void LCDDrawProgressBar(uint8_t x, uint8_t y, uint8_t width, uint8_t pos, uint8_t firstCustomCharPlace)
{
    // TODO implement
}

/**
 * printf-like print function that prints text directly to the display.
 * @param fmt Format string.
 * @param ... Parameters
 */
void LCDPrintf(const char *format, ...)
{
    char buf[21];

    va_list args;
    va_start(args, format);

#ifndef __XC8
    vsprintf(buf, format, args);
    va_end(args);
#else
    // TODO remove this hack after full vsprintf support has been implemented

    // Calling sprintf() forces the compiler to compile _doprnt() into this object
    sprintf(buf, "a");
    buf[0] = 0;

    struct __prbuf pb;
    pb.ptr = buf;
    pb.func = (void (*)(char))NULL;

    _doprnt(&pb, format, args);

    *pb.ptr = 0;
#endif

    LCDPuts(buf);
}

/**
 * Changes the backlight brightness level of the display.
 * @param level
 */
inline void LCDSetBacklight(uint8_t level)
{
    CCPR4L = level;
}

inline uint8_t LCDGetBacklight()
{
    return CCPR4L;
}
