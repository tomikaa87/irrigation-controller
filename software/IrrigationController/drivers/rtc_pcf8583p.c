/**
 * Irrigation System - Control module
 * PCF8583P based RTC driver
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */

#include "rtc_pcf8583p.h"
#include "../drivers/i2c.h"
#include "../global.h"

/*** Internal constants ***************************************************************************/

#define WRITE_ADDR                      0b10100000
#define READ_ADDR                       0b10100001

/*** Forward declarations *************************************************************************/

/*** API function implementations *****************************************************************/

void RTCInit()
{
    RTCTask();
}

void RTCTask()
{
    RTCReadTime_t(&GlobalData.RTCTime);
}

/*** Low level API functions **********************************************************************/

void RTCWriteReg(unsigned char address, unsigned char data)
{
    // Start
    I2CStart();
    // Send slave address
    I2CWrite(WRITE_ADDR);
    // Send register address
    I2CWrite(address);
    // Write data
    I2CWrite(data);
    // Stop communication
    I2CStop();
}

void RTCWriteRegArray(unsigned char address, unsigned char *buffer, unsigned char length)
{
    unsigned char i;

    // Start
    I2CStart();
    // Send slave address
    I2CWrite(WRITE_ADDR);
    // Send register address
    I2CWrite(address);
    // Write n bytes
    for (i = 0; i < length; i++) {
        I2CWrite(buffer[i]);
    }
    // Stop communication
    I2CStop();
}

unsigned char RTCReadReg(unsigned char address)
{
    unsigned char data;

    // Start
    I2CStart();
    // Send slave address
    I2CWrite(WRITE_ADDR);
    // Send register address
    I2CWrite(address);
    // Send restart condition
    I2CRestart();
    // Send slave address (read mode)
    I2CWrite(READ_ADDR);
    // Read data
    data = I2CRead();
    // Stop communication
    I2CStop();

    return data;
}

void RTCReadRegArray(unsigned char address, unsigned char *buffer, unsigned char length)
{
    unsigned char i;

    // Start
    I2CStart();
    // Send slave address
    I2CWrite(WRITE_ADDR);
    // Send register address
    I2CWrite(address);
    // Send restart condition
    I2CRestart();
    // Send slave address (read mode)
    I2CWrite(READ_ADDR);
    // Read n bytes
    for (i = 0; i < length; i++) {
        buffer[i] = I2CRead();
        if (i == length - 1)
            I2CNak();          // end of transmission
        else
            I2CAck();          // continue transmission with the next byte
    }
    // Stop communication
    I2CStop();
}

/*** High level API functions *********************************************************************/

void RTCSetDate(unsigned int year, unsigned char month, unsigned char day, unsigned char weekday)
{
    unsigned char data[2];
    
    // Calculate year counter value
    data[0] = (year % 4) << 6;
    // Calculate day value
    data[0] |= RTCToBCD(day);

    // Calculate weekday counter value
    data[1] = (weekday & 0x07) << 5;
    // Calculate month counter value
    data[1] |= RTCToBCD(month);

    // Write registers
    RTCWriteRegArray(RTC_ADDR_YEAR_DATE, data, 2);

    // Store full year to a user location
    data[0] = year & 0xFF;
    data[1] = year >> 8;
    RTCWriteRegArray(RTC_ADDR_YEAR_LO, data, 2);
}

void RTCGetDate(unsigned int *oYear, unsigned char *oMonth, unsigned char *oDay)
{
    unsigned char year_date[2], full_year[2], year_cnt;

    // Read year/date and month register
    RTCReadRegArray(RTC_ADDR_YEAR_DATE, year_date, 2);
    // Read full year register
    RTCReadRegArray(RTC_ADDR_YEAR_LO, full_year, 2);

    // Calculate values
    *oYear = full_year[0] | (full_year[1] << 8);
    year_cnt = year_date[0] >> 6;
    *oDay = RTCToDEC(year_date[0] & 0x3F);
    *oMonth = RTCToDEC(year_date[1] & 0x1F);

    // Adjust full year value
    while ((*oYear % 4) != year_cnt)
        (*oYear)++;

//    // Read weekday if necessary
//    if (weekday != 0)
//        *weekday = year_date[1] >> 5;
//
//    // Calculate day of year if necessary
//    if (day_of_year != 0) {
//        // Calculate day of year
//        *day_of_year = 0;
//        for (i = 0; i < (*month) - 1; i++) {
//            (*day_of_year) += DAYS_OF_MONTH[i];
//            // Leap year compesation
//            if (i == 1 && year_cnt == 0)
//                (*day_of_year)++;
//        }
//        (*day_of_year) += (*day);
//    }
}

/**
 * !! EXPERIMENTAL !!
 * Calculates the week of the year.
 * @param day_of_year day of the year
 * @param weekday_of_jan1 weekday of january 1st this year (0-6)
 * @return week of the year
 */
unsigned char RTCCalcWeekOfYear(unsigned int dayOfYear, unsigned char jan1DayOfYear)
{
    unsigned char day_cnt = 0, week_of_year = 0;

    // Calculate week of year
    while (dayOfYear-- >= jan1DayOfYear) {
        day_cnt++;
        if (day_cnt == 7) {
            day_cnt = 0;
            week_of_year++;
        }
    }
    if (day_cnt > 0)
        week_of_year++;

    return week_of_year;
}

void RTCGetTime(unsigned char *oHour, unsigned char *oMinute, unsigned char *oSecond)
{
    unsigned char data[3];

    // Read time from the IC
    RTCReadRegArray(RTC_ADDR_SECONDS, data, 3);

    // Get hours
    *oHour = RTCToDEC(data[2]);
    // Get minutes
    *oMinute = RTCToDEC(data[1]);
    // Get seconds
    if (oSecond != 0)
        *oSecond = RTCToDEC(data[0]);
}

void RTCSetTime(unsigned char hour, unsigned char minute, unsigned char second)
{
    unsigned char data[4];

    // Convert values to BCD
    data[0] = 0; // Hundredth seconds
    data[1] = RTCToBCD(second);
    data[2] = RTCToBCD(minute);
    data[3] = RTCToBCD(hour);

    // Write data
    RTCWriteRegArray(RTC_ADDR_HUNDSECONDS, data, 4);
}

void RTCSetAlarmState(unsigned char iEnabled)
{
    unsigned char data;

    // Read alarm control register
    data = RTCReadReg(RTC_ADDR_ALARM_CTRL);

    if (iEnabled)
        data |= 0x04;
    else
        data &= ~0x04;

    // Write alarm control register
    RTCWriteReg(RTC_ADDR_ALARM_CTRL, data);
}

//void rtc_get_datetime_tm(struct tm *t)
//{
//    g_flags.rtc_enabled = 0;
//
//    unsigned char month, day, wday, hour, min, sec;
//    unsigned int year;
//
//    rtc_get_date(&year, &month, &day, &wday, 0);
//    rtc_get_time(&hour, &min, &sec);
//
//    t->tm_hour = hour;
//    t->tm_min = min;
//    t->tm_sec = sec;
//    t->tm_mday = day;
//    t->tm_mon = month - 1;
//    t->tm_year = year - 1900;
//    t->tm_wday = wday + 1;
//    if (t->tm_wday > 6) {
//        t->tm_wday = 0;
//    }
//
//    g_flags.rtc_enabled = 1;
//}
//
//time_t rtc_get_datetime_time_t()
//{
//    struct tm t;
//    rtc_get_datetime_tm(&t);
//    return mktime(&t);
//}

/*** Standard C time handling functions ***********************************************************/

time_t time(time_t *t)
{
    if (t) {
        *t = GlobalData.RTCTime;
    }
    return GlobalData.RTCTime;
}

/**
 * Standard C time function that reads current time from the RTC chip.
 * @param t Pointer to a time_t variable. Current time will be loaded into it if != NULL.
 * @return Current time.
 */
time_t RTCReadTime_t(time_t *t)
{
    struct tm curr_tm;

    // Read date from the RTC
    unsigned int year;
    unsigned char mon, mday;
    RTCGetDate(&year, &mon, &mday);
    if (mon < 1 || mon > 12) {
        mon = 1;
    }
    if (mday < 1 || mday > 31) {
        mday = 1;
    }
    curr_tm.tm_mon = mon - 1;           // RTC indexes month from 1 to 12, not 0 to 11
    curr_tm.tm_mday = mday;
    if (year < 2000 || year > 2099) {
        year = 2000;
    }
    curr_tm.tm_year = year - 1900;

    // Read time from the RTC
    unsigned char hour, min, sec;
    RTCGetTime(&hour, &min, &sec);
    if (hour > 23) {
        hour = 0;
    }
    if (min > 59) {
        min = 0;
    }
    if (sec > 59) {
        sec = 0;
    }
    curr_tm.tm_hour = hour;
    curr_tm.tm_min = min;
    curr_tm.tm_sec = sec;

    // Convert curr_tm into time_t
    time_t curr = mktime(&curr_tm);

    // Set value of pointed variable
    if (t) {
        *t = curr;
    }

    return curr;
}

/**
 * Standard C time setter function that updates the time directly in the RTC chip.
 * @param t Pointer to a time_t variable that holds the new time.
 * @return 0 on success, -1 on error
 */
int stime(time_t *t)
{
    if (!t) {
        return -1;
    }

    // Convert time_t to struct tm
    struct tm *new_tm = gmtime(t);

    // Update date in the RTC chip
    RTCSetDate((unsigned int)new_tm->tm_year + 1900,
                 (unsigned char)new_tm->tm_mon + 1,
                 (unsigned char)new_tm->tm_mday,
                 (unsigned char)new_tm->tm_wday);

    // Update time in the RTC chip
    RTCSetTime((unsigned char)new_tm->tm_hour,
                 (unsigned char)new_tm->tm_min,
                 (unsigned char)new_tm->tm_sec);

    return 0;
}

/*** Converter functions **************************************************************************/

unsigned char RTCToDEC(unsigned char bcdValue)
{
    return ((bcdValue >> 4) * 10) + (bcdValue & 0x0f);
}

unsigned char RTCToBCD(unsigned char decimalValue)
{
    return ((decimalValue / 10) << 4) + (decimalValue % 10);
}