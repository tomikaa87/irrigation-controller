/**
 * Irrigation System - Control module
 * Keypad peripheral module
 *
 * @author Tamas Karpati
 * @date 2013-05-03
 */

#include "keypad.h"
#include "../config.h"
#include "../global.h"

/*** Configuration ************************************************************/

// Number of cycles to wait between reading two buttons
static const uint8_t ScanDelay = 4;
// Number of key scans at every read instruction
static const uint8_t ReadSamples = 20;
// Number of ticks before repeating
static const uint8_t DelayBeforeRepeat = 45;
// Maximum number of ticks to wait between repeats
static const uint8_t RepeatDelayBegin = 10;
// Minimum number of tick to wait between repeats
static const uint8_t RepeatDelayEnd = 1;
// Ticks to wait before decreasing repeat wait
static const uint8_t RepeatSpeedUpDelay = 40;
// Delay after resetting, it gives some more bounce filtering
static const uint8_t ResetDelay = 20;

/*** Globals ******************************************************************/

static tick_t g_pressTicks = 0, g_repeatTicks = 0;
static uint8_t g_repeatDelay;

static enum {
    S_IDLE,
    S_PRESSED,
    S_REPEAT
} g_state = S_IDLE;

#ifndef _DEBUG_SIM_MODE
#define READ_DELAY()    { _delay(ScanDelay); }
#else
#define READ_DELAY() {}
#endif

/*** Forward declarations *****************************************************/

static void scan(uint16_t *pressedKeys);
static void delayTicks(tick_t ticks);

/*** API functions ************************************************************/

void KeypadInit()
{
    KEY_ROW_1_TRIS = 0;
    KEY_ROW_2_TRIS = 0;
    KEY_ROW_3_TRIS = 0;
    KEY_COL_1_TRIS = 1;
    KEY_COL_2_TRIS = 1;
    KEY_COL_3_TRIS = 1;
    KEY_ROW_1 = 0;
    KEY_ROW_2 = 0;
    KEY_ROW_3 = 0;
}

uint16_t KeypadTask()
{
    uint16_t pressedKeys = 0;
    static uint16_t lastPressedKeys = 0;

    scan(&pressedKeys);

    switch (g_state) {
        // Idle state, no keys are being pressed
        case S_IDLE:
            // Key(s) pressed
            if (pressedKeys > 0) {
                g_state = S_PRESSED;
                g_pressTicks = g_ticks;
                lastPressedKeys = pressedKeys;
                return pressedKeys;
            }
            break;

        case S_PRESSED:
            // Pressed key(s) changed, reset
            if (lastPressedKeys != pressedKeys) {
                g_state = S_IDLE;
                delayTicks(ResetDelay);
            } else {
                // Press time reached the repeat limit, enter repeat state
                if (TicksElapsed(g_pressTicks) > DelayBeforeRepeat) {
                    g_repeatTicks = g_ticks;
                    g_repeatDelay = RepeatDelayBegin;
                    g_pressTicks = g_ticks;
                    g_state = S_REPEAT;
                    return (pressedKeys | KEY_LONG_PRESS);
                }
            }
            break;

        case S_REPEAT:
            // Pressed key(s) changed, reset
            if (lastPressedKeys != pressedKeys) {
                g_state = S_IDLE;
                break;
            }
            // Repeat key and reset repeat timer
            if (TicksElapsed(g_repeatTicks) > g_repeatDelay) {
                g_repeatTicks = g_ticks;
                return (pressedKeys | KEY_LONG_PRESS);
            }
            // Decrease repeat delay => speed up repeat
            if (g_repeatDelay > RepeatDelayEnd &&
                    TicksElapsed(g_pressTicks) > RepeatSpeedUpDelay) {
                g_repeatDelay--;
                g_pressTicks = g_ticks;
            }
            break;
    }

    return 0;
}

/*** Internal functions *******************************************************/

/**
 * Reads the keypad with software debouncing
 */
static void scan(uint16_t *pressedKeys)
{
    for (uint8_t samples = 0; samples < ReadSamples; ++samples) {
        uint16_t code = 0;

        KEY_ROW_1_TRIS = 0;
        KEY_ROW_1 = 0;
        READ_DELAY();
        if (KEY_COL_1 == 0) {
            code |= KEY_R1_C1;
        }
        READ_DELAY();
        if (KEY_COL_2 == 0) {
            code |= KEY_R1_C2;
        }
        READ_DELAY();
        if (KEY_COL_3 == 0) {
            code |= KEY_R1_C3;
        }
        KEY_ROW_1_TRIS = 1;
        READ_DELAY();

        KEY_ROW_2_TRIS = 0;
        KEY_ROW_2 = 0;
        READ_DELAY();
        if (KEY_COL_1 == 0) {
            code |= KEY_R2_C1;
        }
        READ_DELAY();
        if (KEY_COL_2 == 0) {
            code |= KEY_R2_C2;
        }
        READ_DELAY();
        if (KEY_COL_3 == 0) {
            code |= KEY_R2_C3;
        }
        KEY_ROW_2_TRIS = 1;
        READ_DELAY();

        KEY_ROW_3_TRIS = 0;
        KEY_ROW_3 = 0;
        READ_DELAY();
        if (KEY_COL_1 == 0) {
            code |= KEY_R3_C1;
        }
        READ_DELAY();
        if (KEY_COL_2 == 0) {
            code |= KEY_R3_C2;
        }
        READ_DELAY();
        if (KEY_COL_3 == 0) {
            code |= KEY_R3_C3;
        }
        KEY_ROW_3_TRIS = 1;
        READ_DELAY();

        *pressedKeys |= code;
    }
}

static void delayTicks(tick_t ticks)
{
    tick_t start = g_ticks;
    while (TicksElapsed(start) < ticks)
        CLRWDT();
}