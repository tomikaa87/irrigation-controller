/**
 * Irrigation System - Control module
 * Buzzer peripheral module
 *
 * @author Tamas Karpati
 * @date 2012-09-24
 */

#include "buzzer.h"
#include "../config.h"

#include <stdint.h>

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

void BuzzerInit()
{
    BUZZER_LATCH = 0;
    BUZZER_TRIS = 0;
#ifndef BUZZER_SW_PWM
    BUZZER_TCON = 0;
    BUZZER_CCPCON = 0;
#endif

    // Buzzer test
//    unsigned char i;
//    BuzzerBeep(BV_LOW);
//    for (i = 0; i < 5; i++, __delay_ms(10));
//    BuzzerBeep(BV_LOW);
//    for (i = 0; i < 5; i++, __delay_ms(10));
//    buz_beep(BUZZER_VOL_HIGH);
//    for (i = 0; i < 5; i++, __delay_ms(10));
}

//--------------------------------------------------------------------------------------------------
void BuzzerBeep(EBuzzerVolume volume)
{
#ifndef BUZZER_SW_PWM
    unsigned char i;

    BUZZER_PR = BUZZER_PR_VAL;
    BUZZER_TCON = BUZZER_TCON_VAL;

    switch (volume) {
        case BV_LOW:
            BUZZER_CCPRL = 0b00000001 ;
            BUZZER_CCPCON = 0b00001100 ;
            break;

        case BV_MID:
            BUZZER_CCPRL = 0b00000110 ;
            BUZZER_CCPCON = 0b00011100 ;
            break;

        case BV_HIGH:
            BUZZER_CCPRL = 0b00111110 ;
            BUZZER_CCPCON = 0b00001100 ;
            break;
    }

    for (i = 0; i < 5; i++)
        __delay_ms(10);

    BUZZER_CCPCON = 0;
    BUZZER_TCON = 0;
#else
    /*
     * Software PWM by bit-banging
     */

    BUZZER_LATCH = 0;

    switch (volume) {
        case BV_SILENT:
            break;
            
        case BV_HIGH:
            // 4 KHz -> T=250us -> 125us/125us @50% dc
            for (uint16_t i = 0; i < 200; i++) {
                BUZZER_LATCH = 1;
                __delay_us(125);
                BUZZER_LATCH = 0;
                __delay_us(125);
            }
            break;

        case BV_MID:
            for (uint16_t i = 0; i < 200; i++) {
                BUZZER_LATCH = 1;
                __delay_us(55);
                BUZZER_LATCH = 0;
                __delay_us(195);
            }
            break;

        case BV_LOW:
            for (uint16_t i = 0; i < 200; i++) {
                BUZZER_LATCH = 1;
                __delay_us(10);
                BUZZER_LATCH = 0;
                __delay_us(240);
            }
            break;

        case BV_VERY_LOW:
            for (uint16_t i = 0; i < 200; i++) {
                BUZZER_LATCH = 1;
                __delay_us(2);
                BUZZER_LATCH = 0;
                __delay_us(248);
            }
            break;
    }
#endif
}