/**
 * Irrigation System - Control module
 * Pump control module
 *
 * @author Tamas Karpati
 * @date 2013-04-21
 */

#include "pump.h"
#include "../config.h"
#include "led.h"
#include "../watergauge.h"
#include "../alarm.h"

#include <stdio.h>

/*** Globals **************************************************************************************/

static tick_t _startTicks[PUMP_COUNT];
static uint32_t _startPulseCounts[PUMP_COUNT];
static uint8_t _calibPump;

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

// For safety reasons, MOSFETS needs cooling when operating on PWM mode
#define PUMP_NO_PWM

void PumpInit()
{
    // Init pump 1
    PUMP_1_PWM_TRIS = 0;
    CCP2CON = 0b00001100;
    CCPR2L = 0;

    // Init pump 2
    PUMP_2_PWM_TRIS = 0;
    CCP1CON = 0b00001100;
    CCPR1L = 0;
}

/**
 * Calculates the number of seconds the pump must be operated to pump the
 * required amount of water.
 * @param pump Index of the pump to be used.
 * @param decilitres Required amount of water to be pumped.
 * @return Number of seconds
 */
uint16_t PumpCalcRunTime(uint8_t pump, uint16_t amountDl)
{
    if (pump > PUMP_COUNT - 1) {
        return 0;
    }
    
    uint16_t timeSecs = GlobalData.Settings.Pump[pump].SpeedSecondsPerLitre * amountDl;
    timeSecs = timeSecs / 10 + (timeSecs % 10 > 5 ? 1 : 0);

    return timeSecs;
}

/**
 * Sets the PWM duty cycle of a specific pump.
 * @param pump Index of the pump.
 * @param duty PWM duty cycle.
 */
void PumpSetPWM(uint8_t pump, uint8_t dutyCycle)
{
    switch (pump) {
        case 0:
            // 10 bits resolution, 2 LSBs not used, only CCPRxL have to be touched
            #ifndef PUMP_NO_PWM
                CCPR2L = dutyCycle;
            #else
                CCPR2L = dutyCycle > 0 ? 0xff : 0;
            #endif
            //    // Write 8 MSbits of duty to CCPR1L
            //    CCPR2L = (duty >> 2) & 0xFF;
            //    // Write 2 LSBits of duty to DC1B0 and DC1B1
            //    DC2B0 = (duty & 1) ? 1 : 0;
            //    DC2B1 = (duty & 2) ? 1 : 0;
            break;

        case 1:
            #ifndef PUMP_NO_PWM
                CCPR1L = dutyCycle;
            #else
                CCPR1L = dutyCycle > 0 ? 0xff : 0;
            #endif
            //    // Write 8 MSbits of duty to CCPR1L
            //    CCPR1L = (duty >> 2) & 0xFF;
            //    // Write 2 LSBits of duty to DC1B0 and DC1B1
            //    DC1B0 = (duty & 1) ? 1 : 0;
            //    DC1B1 = (duty & 2) ? 1 : 0;
            break;

        default:
            break;
    }
    
    // Turn on LED if any of the pumps is on
    if (CCPR1L || CCPR2L) {
        LEDEnable(LED3);
    }

    // Turn off LED if all pumps are off
    if (!CCPR1L && !CCPR2L) {
        LEDDisable(LED3);
    }
    
}

/**
 * Returns the PWM duty cycle of a specific pump.
 * @param pump Index of the pump.
 * @return PWM duty cycle.
 */
uint8_t PumpGetPWM(uint8_t pump)
{
    switch (pump) {
        case 0:
            return CCPR2L;

        case 1:
            return CCPR1L;

        default:
            return 0;
    }
}

/**
 * Start calibration of a pump. This can be used to measure the time the
 * pump needs to move a specific amount of water.
 * @param pump Index of the pump.
 * @return 0 on success, 1 if the process is aborted.
 */
uint8_t PumpStartCalibration(uint8_t pump)
{
    // Abort calibration if any of the pumps are active
    if (CCPR1L || CCPR2L) {
        return 1;
    }

    // Indicate calibration process
    LEDFlash(LED3);

    // Turn on the specific pump
    _calibPump = pump;
    PumpStart(pump);

    return 0;
}

/**
 * Stops the calibration process.
 * @return Elapsed seconds since the calibration was started.
 */
uint8_t PumpStopCalibration()
{
    // Stop the pump
    PumpStop(_calibPump);

    // Calculate elapsed seconds
    return (uint8_t)(PumpGetRunTime(_calibPump) / 100);
}

/**
 * Starts the selected pump and updates the last run timestamp.
 * @param pump Pump index
 */
uint8_t PumpStart(uint8_t pump)
{
    if (!WaterGaugeIsPumpAllowed()) {
        AlarmEmptyTank();
        return 0;
    }

    // Start the pump and store current tick count
    _startTicks[pump] = g_ticks;
    _startPulseCounts[pump] = GlobalData.WaterFlowSensors[pump].TotalPulses;
    
    printf("PumpStart;\r\n");
    printf("_sPC[%u] = %ul\r\n", pump, _startPulseCounts[pump]);
    
    PumpSetPWM(pump, 0xFF);

    return 1;
}

/**
 * Stops the selected pump, measures the run time and calculates the amount of water used.
 * @param pump Pump index
 */
void PumpStop(uint8_t pump)
{
    // Stop the pump
    PumpSetPWM(pump, 0);
    
    uint32_t pulseCount = GlobalData.WaterFlowSensors[pump].TotalPulses;
            
    uint32_t amount = UINT32_DIFF(_startPulseCounts[pump], pulseCount);
    amount = amount / (GlobalData.Settings.Sensor.Sensors[pump].PulsesPerLitre / 10);
    
    printf("PumpStop; amount = %lu dl\r\n", amount);
       
    // Calculate used water and save the value
    // Amount (dl) = elapsed_ticks (100th sec) / pump_speed (secs/litre)
//    uint16_t amount = TicksElapsed(_startTicks[pump]) /
//            (uint16_t)GlobalData.Settings.Pump[pump].SpeedSecondsPerLitre;   

    //GlobalData.Settings.Tank.UsedAmountDl += amount / 10;
    WaterGaugeAddPumpAmount(amount);

    SettingsSave();
}

/**
 * Returns the elapsed ticks sincs the pump was started.
 * @param pump Pump index
 * @return Tick count
 */
inline tick_t PumpGetRunTime(uint8_t pump)
{
    return TicksElapsed(_startTicks[pump]);
}

inline uint8_t PumpIsRunning(uint8_t pump)
{
    return PumpGetPWM(pump) > 0;
}