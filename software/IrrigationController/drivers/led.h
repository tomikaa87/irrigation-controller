/**
 * Irrigation System - Control module
 * LED control module
 *
 * @author Tamas Karpati
 * @date 2013-05-05
 */

#ifndef LED_H
#define	LED_H

#include "stdint.h"

enum {
    LED1 = 1,
    LED2 = 2,
    LED3 = 4
};

void LEDInit();

void LEDEnableAll();
inline void LEDDisableAll();

void LEDEnable(uint8_t leds);
void LEDDisable(uint8_t leds);

void LEDFlash(uint8_t leds);

void LEDTask();

#endif	/* LED_H */

