/**
 * Irrigation System - Control module
 * I2C peripheral module
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */
 
#include "i2c.h"
#include "../config.h"

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

/**
 * Initializes the I2C module.
 */
void I2CInit()
{
    // Setup IO ports
    I2C_SCL_TRIS = 1;
    I2C_SDA_TRIS = 1;

    // Power up the I2C module (if supported)
#ifdef I2C_MD_SUPPORTED
    I2C_MD = 1;
#endif

    // Disable slew-rate control
    I2C_SSPSTAT_BITS.SMP = 1;

    // Enable I2C module
    I2C_SSPCON1_BITS.SSPEN = 1;

    // I2C master mode
    I2C_SSPCON1_BITS.SSPM3 = 1;

    // 100 KHz @ Fosc 64 MHz
    I2C_SSPADD = 200;
}

//--------------------------------------------------------------------------------------------------
/**
 * Send START condition.
 */
void I2CStart()
{
    I2C_SSPCON2_BITS.SEN = 1;
    while (I2C_SSPCON2_BITS.SEN)
        continue;
}

//--------------------------------------------------------------------------------------------------
/**
 * Send STOP condition.
 */
void I2CStop()
{
    I2C_SSPCON2_BITS.PEN = 1;
    while (I2C_SSPCON2_BITS.PEN)
        continue;
}

//--------------------------------------------------------------------------------------------------
/**
 * Send RESTART condition.
 */
void I2CRestart()
{
    I2C_SSPCON2_BITS.RSEN = 1;
    while (I2C_SSPCON2_BITS.RSEN)
        continue;
}

//--------------------------------------------------------------------------------------------------
/**
 * Send positive acknowledgement.
 */
void I2CAck()
{
    I2C_SSPCON2_BITS.ACKDT = 0;
    I2C_SSPCON2_BITS.ACKEN = 1;
    while (I2C_SSPCON2_BITS.ACKEN)
        continue;
}

//--------------------------------------------------------------------------------------------------
/**
 * Send negative acknowledgement.
 */
void I2CNak()
{
    I2C_SSPCON2_BITS.ACKDT = 1;
    I2C_SSPCON2_BITS.ACKEN = 1;
    while (I2C_SSPCON2_BITS.ACKEN)
        continue;
}

//--------------------------------------------------------------------------------------------------
/**
 * Wait for slave device.
 */
void I2CWait()
{
    while (I2C_SSPCON2_BITS.ACKEN || I2C_SSPCON2_BITS.RCEN ||
            I2C_SSPCON2_BITS.PEN || I2C_SSPCON2_BITS.RSEN ||
            I2C_SSPCON2_BITS.SEN || I2C_SSPSTAT_BITS.R_nW)
        continue;
}

//--------------------------------------------------------------------------------------------------
/**
 * Write a byte to the device.
 * @param b the byte
 */
void I2CWrite(uint8_t data)
{
    I2C_SSPBUF = data;
    while (I2C_SSPSTAT_BITS.BF)
        continue;
    I2CWait();
}

//--------------------------------------------------------------------------------------------------
/**
 * Read a byte from the device.
 */
uint8_t I2CRead()
{
    unsigned char b;

    I2C_SSPCON2_BITS.RCEN = 1;
    while (!I2C_SSPSTAT_BITS.BF)
        continue;
    b = I2C_SSPBUF;
    I2CWait();

    return b;
}