/**
 * Irrigation System - Control module
 * HD44780 based LCD driver
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */
 
#ifndef LCD_HD44780_H
#define LCD_HD44780_H

#include <stdint.h>

//--------------------------------------------------------------------------------------------------
// Storage types
//--------------------------------------------------------------------------------------------------

typedef struct {
    uint8_t x;
    uint8_t y;
} EPoint;

//--------------------------------------------------------------------------------------------------
// Flags
//--------------------------------------------------------------------------------------------------

// Display/cursor shift
enum {
    LCD_SH_CURSOR           = 0,
    LCD_SH_LEFT             = 0,
    LCD_SH_DISPLAY          = 8,
    LCD_SH_RIGHT            = 4
};

// Display mode
enum {
    LCD_DM_DISABLED         = 0,
    LCD_DM_ENABLED          = 4,
    LCD_DM_CUR_UNDERLINE    = 2,
    LCD_DM_CUR_BLINK        = 1
};

// Display functions
enum {
    LCD_SF_8BIT             = 16,
    LCD_SF_2LINES           = 8,
    LCD_SF_10DOTS           = 4
};

//--------------------------------------------------------------------------------------------------
// Custom character constants
//--------------------------------------------------------------------------------------------------

extern const uint8_t LCDCC_LevelIndicator0[8];
extern const uint8_t LCDCC_LevelIndicator1[8];
extern const uint8_t LCDCC_LevelIndicator2[8];
extern const uint8_t LCDCC_LevelIndicator3[8];

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

void LCDInit();
void LCDClear();
void LCDPutch(char c);
void LCDPuts(const char *s);
char LCDGetch();
void LCDMoveCursor(uint8_t x, uint8_t y);
inline void LCDMoveCursorToPos(EPoint pos);
void LCDDefineChar(uint8_t place, const uint8_t *data);
void LCDShift(uint8_t shiftFlags);
void LCDSetDisplayMode(uint8_t modeFlags);
uint8_t LCDGetDisplayMode();
void LCDSetFunc(uint8_t funcFlags);
void LCDDrawProgressBar(uint8_t x, uint8_t y, uint8_t width, uint8_t pos, uint8_t firstCustomCharPlace);
void LCDPrintf(const char *format, ...);
inline void LCDSetBacklight(uint8_t level);
inline uint8_t LCDGetBacklight();

#endif // LCD_HD44780_H