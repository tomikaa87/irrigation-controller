/**
 * Irrigation System - Control module
 * Buzzer peripheral module
 *
 * @author Tamas Karpati
 * @date 2012-09-24
 */

#ifndef BUZZER_H
#define BUZZER_H

typedef enum {
    BV_SILENT = 0,
    BV_VERY_LOW,
    BV_LOW,
    BV_MID,
    BV_HIGH
} EBuzzerVolume;

void BuzzerInit();
void BuzzerBeep(EBuzzerVolume volume);

#endif // BUZZER_H