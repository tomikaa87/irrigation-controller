/**
 * Irrigation System - Control module
 * LED control module
 *
 * @author Tamas Karpati
 * @date 2013-05-05
 */

#include "led.h"
#include "../config.h"
#include "../global.h"

/*** Globals ******************************************************************/

static struct {
    unsigned timer_on: 1;
    unsigned led1_flash: 1;
    unsigned led2_flash: 1;
    unsigned led3_flash: 1;
    unsigned unused: 4;
} flags = {0,};
static tick_t g_flash_ticks = 0;

/*** API functions ************************************************************/

/**
 * Initializes LED driver pins.
 */
void LEDInit()
{
    LED1_PIN = 0;
    LED1_TRIS = 0;
    LED2_PIN = 0;
    LED2_TRIS = 0;
    LED3_PIN = 0;
    LED3_TRIS = 0;
}

/**
 * Turns on all LEDs.
 */
void LEDEnableAll()
{
    LED1_PIN = 1;
    LED2_PIN = 1;
    LED3_PIN = 1;
}

/**
 * Turns off all LEDs (including flashing ones).
 */
inline void LEDDisableAll()
{
    LEDDisable(LED1 | LED2 | LED3);
}

/**
 * Turns on specific LEDs.
 * @param leds LEDs to turn on. Multiple LEDs can be or-ed together.
 */
void LEDEnable(uint8_t leds)
{
    if (leds & LED1) {
        LED1_PIN = 1;
    }
    if (leds & LED2) {
        LED2_PIN = 1;
    }
    if (leds & LED3) {
        LED3_PIN = 1;
    }
}

/**
 * Turns on specific LEDs (including flashing ones).
 * @param leds LEDs to turn off. Multiple LEDs can be or-ed together.
 */
void LEDDisable(uint8_t leds)
{
    if (leds & LED1) {
        LED1_PIN = 0;
        flags.led1_flash = 0;
    }
    if (leds & LED2) {
        LED2_PIN = 0;
        flags.led2_flash = 0;
    }
    if (leds & LED3) {
        LED3_PIN = 0;
        flags.led3_flash = 0;
    }

    // Turn off the timer if necessary
    if (!flags.led1_flash && !flags.led2_flash && !flags.led3_flash) {
        flags.timer_on = 0;
    }
}

/**
 * Enables flashing on specific LEDs.
 * @param leds LEDs to flash. Multiple LEDs can be or-ed together.
 */
void LEDFlash(uint8_t leds)
{
    // Set flash flags
    if (leds & LED1) {
        flags.led1_flash = 1;
    }
    if (leds & LED2) {
        flags.led2_flash = 1;
    }
    if (leds & LED3) {
        flags.led3_flash = 1;
    }

    // Turn on timer if necessary
    if (flags.led1_flash || flags.led2_flash || flags.led3_flash) {
        if (!flags.timer_on) {
            flags.timer_on = 1;
            g_flash_ticks = g_ticks;
        }
    }
}

/**
 * Task function that controls flashing. Must be called in the main loop.
 */
void LEDTask()
{
    if (flags.timer_on && TicksElapsed(g_flash_ticks) >= 50) {
        g_flash_ticks = g_ticks;

        LED1_PIN ^= flags.led1_flash;
        LED2_PIN ^= flags.led2_flash;
        LED3_PIN ^= flags.led3_flash;
    }
}