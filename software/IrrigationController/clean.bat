@echo off

del /f /s /q build\ > nul 2> nul
rd /s /q build\ > nul 2> nul
del /f /s /q dist\ > nul 2> nul
rd /s /q dist\ > nul 2> nul

mkdir build\18F26K22\production\drivers
mkdir build\18F26K22\production\lib
mkdir build\18F26K22\production\ui
mkdir dist\18F26K22\production