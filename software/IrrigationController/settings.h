/* 
 * File:   settings.h
 * Author: ToMikaa
 *
 * Created on 2012. okt�ber 3., 20:49
 */

#ifndef SETTINGS_H
#define	SETTINGS_H

#include "drivers/rtc_pcf8583p.h"
#include "config.h"

#include <stdint.h>

//--------------------------------------------------------------------------------------------------
// Global constants
//--------------------------------------------------------------------------------------------------

#define CFG_BASE_ADDR                   RTC_ADDR_USER_RAM_BASE

//--------------------------------------------------------------------------------------------------
// Storage types for scheduler settings
//--------------------------------------------------------------------------------------------------

typedef struct {
    //struct {
        uint8_t Hours;
        uint8_t Minutes;
    //};
    //uint16_t value;
} STimeval;

typedef struct {
    uint16_t AmountDecilitres;
    uint16_t Delay;
} SManualSchedule;

typedef struct {
    uint16_t AmountDecilitres;
    uint16_t Interval;
} SIntervalSchedule;

typedef struct {
    uint16_t AmountDecilitres;
    STimeval Time;
} SCalendarScheduleDay;

typedef struct {
    uint8_t Weekdays;
    SCalendarScheduleDay Days[7];
} SCalendarSchedule;

typedef struct {
    uint8_t Mode;
    int8_t Sensor;
} SGeneralScheduleSettings;

typedef struct {
    SManualSchedule Manual;
    SIntervalSchedule Interval;
    SCalendarSchedule Calendar;
    SGeneralScheduleSettings Settings;
} SSchedulerSettings;

typedef struct {
    struct {
        uint16_t PulsesPerLitre;
    } Sensors[1];
} SSensorSettings;

enum {
    ManualScheduler = 0,
    IntervalScheduler,
    CalendarScheduler
};

const int8_t SchedulerSensorDisabled = -1;

//--------------------------------------------------------------------------------------------------
// Storage types for pump settings
//--------------------------------------------------------------------------------------------------

typedef struct {
    uint8_t SpeedSecondsPerLitre;
} SPumpSettings;

typedef struct {
    uint16_t Capacity;
    uint16_t UsedAmountDl;
    uint16_t WarningAmountDl;
    uint16_t ErrorAmountDl;
} STankSettings;

//--------------------------------------------------------------------------------------------------
// Storage types for keypad settings
//--------------------------------------------------------------------------------------------------

typedef struct {
    uint8_t BeepLevel;
} SKeypadSettings;

//--------------------------------------------------------------------------------------------------
// Storage types for display settings
//--------------------------------------------------------------------------------------------------

typedef struct {
    uint8_t DimmingEnabled;
    uint8_t Level;
    uint8_t DimLevel;
    STimeval DimmingStart;
    STimeval DimmingEnd;
} SDisplaySettings;

//--------------------------------------------------------------------------------------------------
// Storage types for alarm settings
//--------------------------------------------------------------------------------------------------

typedef struct {
    struct
    {
        unsigned BlinkingEnabled : 1;
        unsigned BeepLevel : 3;
        unsigned _unused : 4;
    };
    uint8_t Interval;
    uint8_t Timeout;
} SAlarmSettings;

//--------------------------------------------------------------------------------------------------
// Unified storage type
//--------------------------------------------------------------------------------------------------

typedef uint16_t TChecksum;

#define SETTINGS_DATA_SIZE      \
        sizeof (TChecksum) + \
        sizeof (SSchedulerSettings) * PUMP_COUNT + \
        sizeof (SPumpSettings) * PUMP_COUNT + \
        sizeof (STankSettings) + \
        sizeof (SKeypadSettings) + \
        sizeof (SDisplaySettings) + \
        sizeof (SAlarmSettings) + \
        sizeof (SSensorSettings)

typedef union {
    struct {
        SSchedulerSettings Scheduler[PUMP_COUNT];
        SPumpSettings Pump[PUMP_COUNT];
        STankSettings Tank;
        SKeypadSettings Keypad;
        SDisplaySettings Display;
        SAlarmSettings Alarm;
        SSensorSettings Sensor;

        // Must be the last field
        TChecksum Checksum;
    };
    uint8_t Data[SETTINGS_DATA_SIZE];
} SSettings;

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

void SettingsLoad();
void SettingsSave();
void SettingsResetDefaults();
TChecksum SettingsCalculateChecksum();

void SettingsLoadWFSState();
void SettingsSaveWFSState();

#endif	/* SETTINGS_H */

