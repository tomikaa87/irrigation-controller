/* 
 * File:   global.h
 * Author: ToMikaa
 *
 * Created on 2013. m�rcius 17., 21:37
 */

#ifndef GLOBAL_H
#define	GLOBAL_H

#include "settings.h"
#include "drivers/rtc_pcf8583p.h"
#include "config.h"

#include <time.h>

//--------------------------------------------------------------------------------------------------
// Global constants
//--------------------------------------------------------------------------------------------------

extern const char *ScheduleModeStr[3];
extern const char *WeekdayShortStr[7];
extern const uint8_t MonthDays[12];

#define LCDCC_MenuPositionIndicator     0x7E

//--------------------------------------------------------------------------------------------------
// Types
//--------------------------------------------------------------------------------------------------

typedef union {
    struct {
        uint16_t PPS;
        uint32_t TotalPulses;
    };
    
    uint8_t Data[sizeof (uint32_t) + sizeof (uint16_t)];
} WaterFlowSensorData;

//--------------------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------------------

typedef uint32_t tick_t;
extern tick_t g_ticks;

typedef struct {
    SSettings Settings;
    SSettings ModifiedSettings;
    time_t RTCTime;
    WaterFlowSensorData WaterFlowSensors[PUMP_COUNT];
} SGlobalData;
extern SGlobalData GlobalData;

//--------------------------------------------------------------------------------------------------
// Tick system API
//-------------------------------------------------------------------------------------------------

#define TicksGet()              (g_ticks)
inline tick_t TicksElapsed(tick_t since);
inline tick_t TicksTo(tick_t ticks);

//--------------------------------------------------------------------------------------------------
// Utility functions
//--------------------------------------------------------------------------------------------------

inline uint8_t _bitWeight(int16_t w);

// Universal bit weight calculator for various types of integers
#define BIT_WEIGHT(_X) { \
    uint8_t _Count = 0, _I; \
    for (_I = 0; _I < sizeof (_X) * 8; ++_I) { \
        if (_X & 1) \
            ++_Count; \
        _X >>= 1; \
    } \
}

// Sets the 'b'th bit of 'i'
#define BIT_SET(i, b)   (i |= (1 << b))
// Clears the 'b'th bit of 'i'
#define BIT_CLR(i, b)   (i &= ~(1 << b))
// Reads the 'b'th bit of 'i'
#define BIT_CHK(i, b)   (i & (1 << b))
// Toggles the 'b'th bit of 'i'
#define BIT_TGL(i, b)   (BIT_CHK(i, b) ? BIT_CLR(i, b) : BIT_SET(i, b))

#define UNUSED(x)       (void)x

#define UINT32_DIFF(prev, curr) ((prev) <= (curr) ? ((curr) - (prev)) : (UINT32_MAX - (prev) + (curr)))

inline void _loadSettings();
inline void _saveSettings();

/*** Interface 2.0 ********************************************************************************/

#endif	/* GLOBAL_H */

