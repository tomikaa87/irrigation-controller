#include "settings.h"
#include "lib/crc16.h"
#include "config.h"
#include "drivers/buzzer.h"
#include "global.h"

#include <string.h>
#include <stdio.h>

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

void SettingsLoad()
{
    // Load data from RTC SRAM
    RTCReadRegArray(CFG_BASE_ADDR, GlobalData.Settings.Data, SETTINGS_DATA_SIZE);

    // Verify data
    if (SettingsCalculateChecksum() != GlobalData.Settings.Checksum) {
        SettingsResetDefaults();
        SettingsSave();
    }
}

//--------------------------------------------------------------------------------------------------
void SettingsSave()
{
    // Calculate checksum
    GlobalData.Settings.Checksum = SettingsCalculateChecksum();

    // Save data to RTC SRAM
    RTCWriteRegArray(CFG_BASE_ADDR, GlobalData.Settings.Data, SETTINGS_DATA_SIZE);
}

//--------------------------------------------------------------------------------------------------
void SettingsResetDefaults()
{
    // Reset all values to 0
    memset(GlobalData.Settings.Data, 0, SETTINGS_DATA_SIZE);

    // Disable sensors, set default pump speeds
    uint8_t i;
    for (i = 0; i < PUMP_COUNT; ++i) {
        GlobalData.Settings.Scheduler[i].Settings.Sensor = -1;
        GlobalData.Settings.Pump[i].SpeedSecondsPerLitre = PUMP_DEFAULT_SPEED;
    }

    // Default keypad settings
    GlobalData.Settings.Keypad.BeepLevel = BV_VERY_LOW;

    // Default display settings
    GlobalData.Settings.Display.Level = 100;

    // Default tank settings
    GlobalData.Settings.Tank.WarningAmountDl = 100;
    GlobalData.Settings.Tank.ErrorAmountDl = 30;

    // Default alarm settings
    GlobalData.Settings.Alarm.BeepLevel = BV_MID;
    GlobalData.Settings.Alarm.BlinkingEnabled = 1;
    GlobalData.Settings.Alarm.Interval = 120;
    GlobalData.Settings.Alarm.Timeout = 30;
}

//--------------------------------------------------------------------------------------------------
TChecksum SettingsCalculateChecksum()
{
    return crc16_array((char *)GlobalData.Settings.Data, SETTINGS_DATA_SIZE - sizeof (TChecksum));
}