/**
 * Irrigation System - Control module
 * Alarm settings Screen
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#ifndef SCR_ALARM_H
#define	SCR_ALARM_H

#include "screen.h"

EScreenEventResult AlarmScreenEventHandler(EScreenEvent event, uint16_t data);

#endif	/* SCR_ALARM_H */

