/**
 * Irrigation System - Control module
 * Pump settings Screen
 *
 * @author Tamas Karpati
 * @date 2013-05-03
 */

#include "scr_pump.h"

#include "../global.h"
#include "../settings.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"
#include "../drivers/pump.h"

#include <string.h>

/*** Globals ******************************************************************/

static uint8_t gs_curPos = 0;
static uint8_t gs_calibPump;
static bit gs_isInCalibMode;
/*** Forward declarations *****************************************************/

static void init();
static void _draw();
static void _update();
static EScreenEventResult _handleKey(uint16_t key);
static void _stepValue(uint8_t iUp);
static void _click();
static void _enterCalibMode();
static uint8_t _leaveCalibMode();

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult ScreenPumpEvent(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            init();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            return _handleKey(data);

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

/*** Internal functions *******************************************************/

static void init()
{
    _loadSettings();
    gs_isInCalibMode = 0;
    _draw();
}

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    if (gs_isInCalibMode) {
        LCDClear();
        LCDPuts("= Calibration mode =");
        LCDMoveCursor(0, 1);
        LCDPuts("Press Enter when 1 l");
        LCDMoveCursor(0, 2);
        LCDPuts("of water was pumped.");
        LCDMoveCursor(0, 3);
        LCDPuts("Cancel to abort.");

        return;
    }

    LCDClear();
    LCDPuts("Speed 1: --- sec/l");
    LCDMoveCursor(0, 1);
    LCDPuts("Speed 2: --- sec/l");
    LCDMoveCursor(0, 2);
    LCDPuts("Calibration: [1] [2]");
    LCDMoveCursor(0, 3);
    LCDPuts("Tank cap.: --- l");

    LCDSetDisplayMode(LCD_DM_ENABLED | LCD_DM_CUR_UNDERLINE);
    _update();
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    if (gs_isInCalibMode) {
        return;
    }

    static const EPoint coords[5] = {
        {10, 0}, {10, 1}, {14, 2}, {18, 2}, {12, 3}
    };

    SPumpSettings *pSettings = &(GlobalData.ModifiedSettings.Pump);

    uint8_t i;
    for (i = 0; i < 5; i++) {
        LCDMoveCursorToPos(coords[i]);

        switch (i) {
            // Pump 1 speed
            case 0:
                LCDPrintf("\b%03d", pSettings[0].SpeedSecondsPerLitre);
                break;
            // Pump 2 speed
            case 1:
                LCDPrintf("\b%03d", pSettings[1].SpeedSecondsPerLitre);
                break;
            // Water tank capacity
            case 4:
                LCDPrintf("\b%03d", GlobalData.ModifiedSettings.Tank.Capacity);
                break;
            default:
                break;
        }
    }

    LCDMoveCursorToPos(coords[gs_curPos]);
}

/**
 * Handles key presses and returns the required UI action.
 */
static EScreenEventResult _handleKey(uint16_t key)
{
    if (gs_isInCalibMode) {
        // Calibration mode key press handler
        if (key == KEY_ENTER) {
            GlobalData.ModifiedSettings.Pump[gs_calibPump].SpeedSecondsPerLitre = _leaveCalibMode();
        } else if (key == KEY_CANCEL) {
            _leaveCalibMode();
        }
    } else {
        // Normal mode key press handler
        switch (key) {
            case KEY_DOWN:
                if (++gs_curPos == 5) {
                    gs_curPos = 0;
                }
                break;

            case KEY_UP:
                if (--gs_curPos == 255) {
                    gs_curPos = 5;
                }
                break;

            case KEY_PLUS:
                _stepValue(1);
                break;

            case KEY_MINUS:
                _stepValue(0);
                break;

            case KEY_ENTER:
                _click();
                break;

            case KEY_MENU:
                _saveSettings();
                // No break;
            case KEY_CANCEL:
                LCDSetDisplayMode(LCD_DM_ENABLED);
                return SER_PREV_SCREEN;

            default:
                break;
        }
    }

    return SER_UPDATE;
}

/**
 * Handles key presses and returns the required UI action.
 */
static void _stepValue(uint8_t iUp)
{
    SPumpSettings *pSettings = &(GlobalData.ModifiedSettings.Pump);

    switch (gs_curPos) {
        case 0:
            if (iUp) {
                ++pSettings[0].SpeedSecondsPerLitre;
            } else {
                --pSettings[0].SpeedSecondsPerLitre;
            }
            break;

        case 1:
            if (iUp) {
                ++pSettings[1].SpeedSecondsPerLitre;
            } else {
                --pSettings[1].SpeedSecondsPerLitre;
            }
            break;

        case 4:
            if (iUp) {
                if (++GlobalData.ModifiedSettings.Tank.Capacity > 999) {
                    GlobalData.ModifiedSettings.Tank.Capacity = 0;
                }
            } else {
                if (--GlobalData.ModifiedSettings.Tank.Capacity > 999) {
                    GlobalData.ModifiedSettings.Tank.Capacity = 999;
                }
            }
            break;

        default:
            break;
    }
}

/**
 * Performs a click on the selected item.
 */
static void _click()
{
    switch (gs_curPos) {
        case 2:
            gs_calibPump = 0;
            _enterCalibMode();
            break;

        case 3:
            gs_calibPump = 1;
            _enterCalibMode();
            break;

        default:
            break;
    }
}

static void _enterCalibMode()
{
    // Check if we are already in calibration mode
    if (gs_isInCalibMode) {
        return;
    }

    // Start calibration
    if (PumpStartCalibration(gs_calibPump) > 0) {
        // Calibration cannot be started
        return;
    }

    gs_isInCalibMode = 1;
    LCDSetDisplayMode(LCD_DM_ENABLED);
    _draw();
}

static uint8_t _leaveCalibMode()
{
    // Check if we are already in normal mode
    if (!gs_isInCalibMode) {
        return 0;
    }

    // Stop calibration
    uint8_t result = PumpStopCalibration();

    gs_isInCalibMode = 0;
    _draw();

    return result;
}