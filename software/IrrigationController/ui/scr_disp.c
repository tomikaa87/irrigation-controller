//
// scr_disp.h
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-29.
//

#include "scr_disp.h"
#include "../global.h"
#include "../drivers/keypad.h"
#include "../drivers/lcd_hd44780.h"

//--------------------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------------------

static uint8_t _menuPos;

//--------------------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------------------

static void _init();
static void _draw();
static void _update();
static EScreenEventResult _handleKey(uint16_t key);
static void _stepValue(uint8_t increment);

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

EScreenEventResult ScreenDisplayEvent(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _init();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            return _handleKey(data);

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

//--------------------------------------------------------------------------------------------------
// Internal functions
//--------------------------------------------------------------------------------------------------

static void _init()
{
    _loadSettings();
    _draw();
}

//--------------------------------------------------------------------------------------------------
static void _draw()
{
    LCDClear();
    LCDPuts("Normal backlight:");
    LCDMoveCursor(6, 1);
    LCDPuts("xxx from xx:xx");
    LCDMoveCursor(0, 2);
    LCDPuts("Dimmed backlight");
    LCDMoveCursor(0, 3);
    LCDPuts("[---] xxx from xx:xx");

    LCDSetDisplayMode(LCD_DM_ENABLED | LCD_DM_CUR_UNDERLINE);
    _update();
}

//--------------------------------------------------------------------------------------------------
static void _update()
{
    static const EPoint coords[] = {
        {7, 1}, {15, 1}, {18, 1}, {2, 3}, {7, 3}, {15, 3}, {18, 3}
    };

    //SDisplaySettings* s = &(GlobalData.ModifiedSettings.Display);

    uint8_t i;
    for (i = 0; i < 7; i++) {
        LCDMoveCursorToPos(coords[i]);

        switch (i) {
            // Normal level
            case 0:
                LCDPrintf("\b%03d", GlobalData.ModifiedSettings.Display.Level);
                break;
            // Normal level start hours
            case 1:
                LCDPrintf("%02d", GlobalData.ModifiedSettings.Display.DimmingEnd.Hours);
                break;
            // Normal level start minutes
            case 2:
                LCDPrintf("%02d", GlobalData.ModifiedSettings.Display.DimmingEnd.Minutes);
                break;
            // Dim mode enable/disable
            case 3:
                LCDPrintf("\b%s", GlobalData.ModifiedSettings.Display.DimmingEnabled ? "On " : "Off");
                break;
             // Dimmed level
            case 4:
                LCDPrintf("\b%03d", GlobalData.ModifiedSettings.Display.DimLevel);
                break;
            // Dimmed level start hours
            case 5:
                LCDPrintf("%02d", GlobalData.ModifiedSettings.Display.DimmingStart.Hours);
                break;
            // Dimmed level start minutes
            case 6:
                LCDPrintf("%02d", GlobalData.ModifiedSettings.Display.DimmingStart.Minutes);
                break;
            default:
                break;
        }
    }

    LCDMoveCursorToPos(coords[_menuPos]);
}

//--------------------------------------------------------------------------------------------------
static EScreenEventResult _handleKey(uint16_t key)
{
    switch (key) {
        case KEY_RIGHT:
            if (++_menuPos == 7) {
                _menuPos = 0;
            }
            break;

        case KEY_LEFT:
            if (--_menuPos == 255) {
                _menuPos = 6;
            }
            break;

        case KEY_PLUS:
            _stepValue(1);
            break;

        case KEY_MINUS:
            _stepValue(0);
            break;

        case KEY_ENTER:
            //_click();
            break;

        case KEY_MENU:
            _saveSettings();
            // No break;
        case KEY_CANCEL:
            LCDSetDisplayMode(LCD_DM_ENABLED);
            return SER_PREV_SCREEN;

        default:
            break;
    }

    return SER_UPDATE;
}

//--------------------------------------------------------------------------------------------------
static void _stepValue(uint8_t increment)
{
    //SDisplaySettings* s = &(GlobalData.ModifiedSettings.Display);

    switch (_menuPos) {
        // Normal level
        case 0:
            if (increment)
                ++GlobalData.ModifiedSettings.Display.Level;
            else
                --GlobalData.ModifiedSettings.Display.Level;
            break;

        // Normal level start hours
        case 1:
            if (increment) {
                if (++(GlobalData.ModifiedSettings.Display.DimmingEnd.Hours) >= 24) {
                    GlobalData.ModifiedSettings.Display.DimmingEnd.Hours = 0;
                }
            } else {
                if (--(GlobalData.ModifiedSettings.Display.DimmingEnd.Hours) >= 24) {
                    GlobalData.ModifiedSettings.Display.DimmingEnd.Hours = 23;
                }
            }
            break;

        // Normal level start minutes
        case 2:
            if (increment) {
                if (++(GlobalData.ModifiedSettings.Display.DimmingEnd.Minutes) >= 60) {
                    GlobalData.ModifiedSettings.Display.DimmingEnd.Minutes = 0;
                }
            } else {
                if (--(GlobalData.ModifiedSettings.Display.DimmingEnd.Minutes) >= 60) {
                    GlobalData.ModifiedSettings.Display.DimmingEnd.Minutes = 59;
                }
            }
            break;

        // Dimmed mode enable/disable
        case 3:
            GlobalData.ModifiedSettings.Display.DimmingEnabled = !GlobalData.ModifiedSettings.Display.DimmingEnabled;
            break;

        // Dimmed mode level
        case 4:
            if (increment) {
                ++GlobalData.ModifiedSettings.Display.DimLevel;
            } else {
                --GlobalData.ModifiedSettings.Display.DimLevel;
            }
            break;

        // Dimmed level start minutes
        case 5:
            if (increment) {
                if (++(GlobalData.ModifiedSettings.Display.DimmingStart.Hours) >= 24) {
                    GlobalData.ModifiedSettings.Display.DimmingStart.Hours = 0;
                }
            } else {
                if (--(GlobalData.ModifiedSettings.Display.DimmingStart.Hours) >= 24) {
                    GlobalData.ModifiedSettings.Display.DimmingStart.Hours = 23;
                }
            }
            break;

        // Dimmed level start minutes
        case 6:
            if (increment) {
                if (++(GlobalData.ModifiedSettings.Display.DimmingStart.Minutes) >= 60) {
                    GlobalData.ModifiedSettings.Display.DimmingStart.Minutes = 0;
                }
            } else {
                if (--(GlobalData.ModifiedSettings.Display.DimmingStart.Minutes) >= 60) {
                    GlobalData.ModifiedSettings.Display.DimmingStart.Minutes = 59;
                }
            }
            break;

        default:
            break;
    }
}
