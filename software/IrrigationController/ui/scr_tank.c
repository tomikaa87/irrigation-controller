//
// scr_tank.c
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-30.
//

#include "scr_tank.h"
#include "../global.h"
#include "../drivers/keypad.h"
#include "../drivers/lcd_hd44780.h"
#include "../watergauge.h"

//--------------------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------------------

static uint8_t _menuPos;
static uint16_t _refillAmount = 0;
static uint16_t _setAmount = 0;

//--------------------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------------------

static void _init();
static void _draw();
static void _update();
static EScreenEventResult _handleKey(uint16_t key);
static void _stepValue(uint8_t increment);
static void _click();

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

EScreenEventResult ScreenTankEvent(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _init();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            return _handleKey(data);

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

//--------------------------------------------------------------------------------------------------
// Internal functions
//--------------------------------------------------------------------------------------------------

static void _init()
{
    _loadSettings();
    _draw();
}

//--------------------------------------------------------------------------------------------------
static void _draw()
{
    /*
    Capacity: xxx l
    W: xxx l | E: xxx l
    Refill  : xxx l [OK]
    Set lvl.: xxx l [OK]
    */


    LCDClear();
    LCDPuts("Capacity: xxx l");
    LCDMoveCursor(0, 1);
    LCDPuts("W: xxx l | E: xxx l");
    LCDMoveCursor(0, 2);
    LCDPuts("Refill  : xxx l [OK]");
    LCDMoveCursor(0, 3);
    LCDPuts("Set lvl.: xxx l [OK]");

    LCDSetDisplayMode(LCD_DM_ENABLED | LCD_DM_CUR_UNDERLINE);
    _update();
}

//--------------------------------------------------------------------------------------------------
static void _update()
{
    static const EPoint coords[] = {
        {11, 0}, {4, 1}, {15, 1}, {11, 2}, {17, 2}, {11, 3}, {17, 3}
    };

    //SDisplaySettings* s = &(GlobalData.ModifiedSettings.Display);

    uint8_t i;
    for (i = 0; i < 7; i++) {
        LCDMoveCursorToPos(coords[i]);

        switch (i) {
            // Tank capacity
            case 0:
                LCDPrintf("\b%03d", GlobalData.ModifiedSettings.Tank.Capacity);
                break;
            // Warning level
            case 1:
                LCDPrintf("\b%03d", GlobalData.ModifiedSettings.Tank.WarningAmountDl / 10);
                break;
            // Error level
            case 2:
                LCDPrintf("\b%03d", GlobalData.ModifiedSettings.Tank.ErrorAmountDl / 10);
                break;
            // Refill 
            case 3:
                LCDPrintf("\b%03d", _refillAmount);
                break;
            // Refill OK
            case 4:
                break;
            // Set level
            case 5:
                LCDPrintf("\b%03d", _setAmount);
                break;
            // Set level OK
            case 6:
                break;
            default:
                break;
        }
    }

    LCDMoveCursorToPos(coords[_menuPos]);
}

//--------------------------------------------------------------------------------------------------
static EScreenEventResult _handleKey(uint16_t key)
{
    switch (key) {
    case KEY_RIGHT:
        if (++_menuPos == 7) {
            _menuPos = 0;
        }
        break;

    case KEY_LEFT:
        if (--_menuPos == 255) {
            _menuPos = 6;
        }
        break;

    case KEY_PLUS:
        _stepValue(1);
        break;

    case KEY_MINUS:
        _stepValue(0);
        break;

    case KEY_ENTER:
        _click();
        break;

    case KEY_MENU:
        _saveSettings();
        // No break;
    case KEY_CANCEL:
        LCDSetDisplayMode(LCD_DM_ENABLED);
        return SER_PREV_SCREEN;

    default:
        break;
    }

    return SER_UPDATE;
}

//--------------------------------------------------------------------------------------------------
static void _stepValue(uint8_t increment)
{
    switch (_menuPos) {
        // Capacity
        case 0:
            if (increment) {
                if (++GlobalData.ModifiedSettings.Tank.Capacity >= 1000)
                    GlobalData.ModifiedSettings.Tank.Capacity = 0;
            } else {
                if (--GlobalData.ModifiedSettings.Tank.Capacity >= 1000)
                    GlobalData.ModifiedSettings.Tank.Capacity = 999;
            }
            break;

        // Warning level
        case 1:
            if (increment) {
                GlobalData.ModifiedSettings.Tank.WarningAmountDl += 10;
                if (GlobalData.ModifiedSettings.Tank.WarningAmountDl > GlobalData.ModifiedSettings.Tank.Capacity * 10)
                    GlobalData.ModifiedSettings.Tank.WarningAmountDl = 0;
            } else {
                GlobalData.ModifiedSettings.Tank.WarningAmountDl -= 10;
                if (GlobalData.ModifiedSettings.Tank.WarningAmountDl > GlobalData.ModifiedSettings.Tank.Capacity * 10)
                    GlobalData.ModifiedSettings.Tank.WarningAmountDl = GlobalData.ModifiedSettings.Tank.Capacity * 10;
            }
            break;

        // Error level
        case 2:
            if (increment) {
                GlobalData.ModifiedSettings.Tank.ErrorAmountDl += 10;
                if (GlobalData.ModifiedSettings.Tank.ErrorAmountDl > GlobalData.ModifiedSettings.Tank.WarningAmountDl)
                    GlobalData.ModifiedSettings.Tank.ErrorAmountDl = 0;
            } else {
                GlobalData.ModifiedSettings.Tank.ErrorAmountDl -= 10;
                if (GlobalData.ModifiedSettings.Tank.ErrorAmountDl > GlobalData.ModifiedSettings.Tank.WarningAmountDl)
                    GlobalData.ModifiedSettings.Tank.ErrorAmountDl = GlobalData.ModifiedSettings.Tank.WarningAmountDl;
            }
            break;

        // Refill amount
        case 3:
            if (increment) {
                if (++_refillAmount > GlobalData.ModifiedSettings.Tank.Capacity)
                    _refillAmount = 0;
            } else {
                if (--_refillAmount > GlobalData.ModifiedSettings.Tank.Capacity)
                    _refillAmount = GlobalData.ModifiedSettings.Tank.Capacity;
            }
            break;

        // Set level
        case 5:
            if (increment) {
                if (++_setAmount > GlobalData.ModifiedSettings.Tank.Capacity)
                    _setAmount = 0;
            } else {
                if (--_setAmount > GlobalData.ModifiedSettings.Tank.Capacity)
                    _setAmount = GlobalData.ModifiedSettings.Tank.Capacity;
            }
            break;

        default:
            break;
    }
}

//--------------------------------------------------------------------------------------------------
static void _click()
{
    switch (_menuPos) {
        // Refill OK
        case 4:
            WaterGaugeAddRefillAmount(_refillAmount * 10);
            GlobalData.ModifiedSettings.Tank.UsedAmountDl = GlobalData.Settings.Tank.UsedAmountDl;
            break;

        // Set level OK
        case 6:
            GlobalData.Settings.Tank.UsedAmountDl = GlobalData.ModifiedSettings.Tank.Capacity * 10 - _setAmount * 10;
            GlobalData.ModifiedSettings.Tank.UsedAmountDl = GlobalData.Settings.Tank.UsedAmountDl;
            break;

        default:
            break;
    }
}
