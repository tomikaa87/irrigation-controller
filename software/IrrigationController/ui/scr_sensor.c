/**
 * Irrigation System - Control module
 * Sensor settings Screen
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#include "scr_sensor.h"

#include "utils.h"
#include "../global.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"

/*** Globals ******************************************************************/

static uint8_t g_menuIndex = 0;
static uint8_t g_menuOffset = 0;
static const uint8_t MenuItemCount = 8;

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();
static void _adjust(int8_t value);

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult SensorScreenEventHandler(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            g_menuIndex = 0;
            g_menuOffset = 0;
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            switch (data) {
                case KEY_UP:
                    if (g_menuIndex > 0)
                    {
                        if (g_menuIndex - g_menuOffset == 0)
                        {
                            --g_menuOffset;
                            _draw();
                        }
                        --g_menuIndex;
                    }
                    break;

                case KEY_DOWN:
                    if (g_menuIndex < MenuItemCount - 1)
                    {
                        if (g_menuIndex - g_menuOffset == 3)
                        {
                            ++g_menuOffset;
                            _draw();
                        }
                        ++g_menuIndex;
                    }
                    break;

                case KEY_PLUS:
                    _adjust(1);
                    break;

                case KEY_MINUS:
                    _adjust(-1);
                    break;
                    
                case KEY_ENTER:
                    switch (g_menuIndex)
                    {
                        case 3:
                            // Sensor1 reset total
                            GlobalData.WaterFlowSensors[0].TotalPulses = 0;
                            break;
                            
                        default:
                            break;
                    }
                    break;

                case KEY_MENU:
                    _saveSettings();
                    // No break
                case KEY_CANCEL:
                    //g_flags.rtc_enabled = 1;
                    return SER_PREV_SCREEN;

                /*
                 * Default key press handler
                 */
                default:
                    break;
            }

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_UPDATE;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    LCDClear();

    _loadSettings();
    
    for (uint8_t i = 0; i < 4; ++i)
    {
        LCDMoveCursor(0, i);

        switch (i + g_menuOffset)
        {
            case 0: LCDPuts(" S1 Cal.:   ---- p/l"); break;
            case 1: LCDPuts(" S1 Tot.: ----------"); break;
            case 2: LCDPuts(" S1 Curr.:  ---- p/s"); break;
            case 3: LCDPuts(" [S1 Reset Total]   "); break;
            case 4: LCDPuts(" S2 Cal.:   ---- p/l"); break;
            case 5: LCDPuts(" S2 Tot.: ----------"); break;
            case 6: LCDPuts(" S2 Curr.:  ---- p/s"); break;
            case 7: LCDPuts(" [S2 Reset Total]   "); break;
            default: break;
        }
    }

    _update();
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    for (uint8_t i = 0; i < 4; ++i)
    {
        switch (i + g_menuOffset)
        {
            case 0:
                // Sensor1 PPL
                LCDMoveCursor(12, i);
                LCDPrintf("%4u", GlobalData.ModifiedSettings.Sensor.Sensors[0].PulsesPerLitre);

                break;
        
            case 1: 
                // Sensor1 Total
                LCDMoveCursor(10, i);
                LCDPrintf("%10u", GlobalData.WaterFlowSensors[0].TotalPulses);
                break;

            case 2: 
                // Sensor1 Current
                LCDMoveCursor(12, i);
                LCDPrintf("%4u", GlobalData.WaterFlowSensors[0].PPS);
                break;

            default: break;
        }
    }

    // Show cursor
    for (uint8_t i = 0; i < 4; ++i)
    {
        LCDMoveCursor(0, i);
        if (i == g_menuIndex - g_menuOffset)
            LCDPutch(LCDCC_MenuPositionIndicator);
        else
            LCDPutch(' ');
    }
}

/**
 * Adjusts the currently selected parameter by the given value.
 * @param value
 */
static void _adjust(int8_t value)
{
    switch (g_menuIndex) 
    {
        // Sensor1 PPL
        case 0:
        {
            uint16_t *ppl = &(GlobalData.ModifiedSettings.Sensor.Sensors[0].PulsesPerLitre);
            if (value < 0 && *ppl < -value)
                *ppl = 10000 - value;
            else
                *ppl = (*ppl + value) % 10000;
            break;
        }

        default:
            break;
    }

    _update();
}
