/**
 * Irrigation System - Control module
 * Scheduler settings Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#include "scr_sched.h"
#include "../settings.h"
#include "../global.h"
#include "../scheduler.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"

#include <time.h>
#include <stdio.h>

/*** Globals ******************************************************************/

static uint8_t gs_curPos = 0;
static uint8_t gs_pump = 0;
static uint8_t gs_day = 0;

/*** Forward declarations *****************************************************/

static void init();
static void _draw();
static void _update();
static void _drawManual();
static void _drawInterval();
static void _drawScheduler();
static void _updateManual();
static void _updateInterval();
static void _updateScheduler();
static EScreenEventResult _handleKey(uint16_t key);

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult ScreenScheduleEvent(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            init();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            return _handleKey(data);

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

/*** Internal functions *******************************************************/

static void init()
{
    _loadSettings();

//    printf("sch: init (mode=%d)\r\n", g_new_settings.scheduler[g_pump_sel].mode);
//    printf("sch: sch_days[0] = 0x%02X; sch_days[1] = 0x%02X\r\n",
//            g_new_settings.scheduler[g_pump_sel].sch_days[0],
//            g_new_settings.scheduler[g_pump_sel].sch_days[1]);

    _draw();
}

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    LCDClear();
    
    switch (GlobalData.ModifiedSettings.Scheduler[gs_pump].Settings.Mode) {
        case ManualScheduler:
            printf("sch: manual\r\n");
            _drawManual();
            break;

        case IntervalScheduler:
            printf("sch: interval\r\n");
            _drawInterval();
            break;

        case CalendarScheduler:
            printf("sch: calendar\r\n");
            _drawScheduler();
            break;
    }

    _update();
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    // Print pump index (from 1)
    LCDMoveCursor(0, 0);
    LCDPrintf("%u", gs_pump + 1);

    // Update the corresponding screen
    switch (GlobalData.ModifiedSettings.Scheduler[gs_pump].Settings.Mode) {
        case ManualScheduler:
            _updateManual();
            break;

        case IntervalScheduler:
            _updateInterval();
            break;

        case CalendarScheduler:
            _updateScheduler();
            break;
    }

    LCDSetDisplayMode(LCD_DM_ENABLED | LCD_DM_CUR_UNDERLINE);
}

static void _drawManual()
{
    LCDMoveCursor(1, 0);
    LCDPrintf(":%s", ScheduleModeStr[ManualScheduler]);

    LCDMoveCursor(0, 1);
    LCDPuts("Amount: ---- dl");

    LCDMoveCursor(0, 2);
    LCDPuts("Delay: --- m");

    LCDMoveCursor(0, 3);
    LCDPuts("[     ]");
}

static void _drawInterval()
{
    LCDMoveCursor(1, 0);
    LCDPrintf(":%s   R: ---:--.--", ScheduleModeStr[IntervalScheduler]);

    LCDMoveCursor(0, 1);
    LCDPuts("I: --- m  A: ---- dl");

    LCDMoveCursor(0, 2);
    LCDPuts("Sensor: ---");

    LCDMoveCursor(0, 3);
    LCDPuts("[Start] [Reset]");
}

static void _drawScheduler()
{
    // Print weekdays
    LCDMoveCursor(1, 0);
    LCDPrintf(":%s ", ScheduleModeStr[CalendarScheduler]);
    // " S M T W T F S"
    uint8_t day;
    for (day = 0; day < 7; day++) {
        LCDPutch(' ');
        // First letter of the weekday string
        LCDPutch(WeekdayShortStr[day][0]);
    }

    LCDMoveCursor(0, 1);
    LCDPuts("T: --:--  A: ---- dl");

    LCDMoveCursor(0, 2);
    LCDPuts("Sensor: ---");
    
    LCDMoveCursor(0, 3);
}

static void _updateManual()
{
    static const EPoint coords[6] =  {
        {0, 0}, {2, 0}, {8, 1}, {7, 2}, {11, 2}, {1, 3}
    };
    uint8_t i;
    SSchedulerSettings *pSettings = &(GlobalData.ModifiedSettings.Scheduler[gs_pump]);

    for (i = 0; i < 7; ++i) {
        LCDMoveCursorToPos(coords[i]);

        switch (i) {
            case 2:
                LCDPrintf("%04u", pSettings->Manual.AmountDecilitres);
                break;
            case 3:
                LCDPrintf("%03u", (unsigned)(pSettings->Manual.Delay & 0x7fff));
                break;
            case 4:
#ifndef __XC8
                LCDPrintf("%c", BIT_CHK(pSettings->Manual.Delay, 15) > 0 ? 'h' : 'm');
#else
                LCDPutch(BIT_CHK(pSettings->Manual.Delay, 15) > 0 ? 'h' : 'm');
#endif
                break;
            case 5:
#ifndef __XC8
                LCDPrintf("%s", SchedulerManualIsRunning(gs_pump) ? "Stop " : "Start");
#else
                LCDPuts(SchedulerManualIsRunning(gs_pump) ? "Stop " : "Start");
#endif
                break;

            default:
                break;
        }
    }

    LCDMoveCursorToPos(coords[gs_curPos]);
}

static void _updateInterval()
{
    static const EPoint coords[9] =  {
        {0, 0}, {2, 0}, {11, 0}, {3, 1}, {7, 1}, {13, 1}, {8, 2}, {1, 3}, {9, 3}
    };
    uint8_t i;
    SSchedulerSettings *pSettings = &(GlobalData.ModifiedSettings.Scheduler[gs_pump]);

    for (i = 0; i < 9; ++i) {
        LCDMoveCursorToPos(coords[i]);

        switch (i) {
            case 2: {
                time_t diff = SchedulerIntervalGetRemaining(gs_pump);
                struct tm pt = *gmtime(&diff);
                LCDPrintf("%03u:%02u.%02u", pt.tm_hour + (pt.tm_mday - 1) * 24, pt.tm_min, pt.tm_sec);
                break;
            }
            case 3:
                LCDPrintf("%03u", (unsigned)(pSettings->Interval.Interval & 0x7fff));
                break;
            case 4:
#ifndef __XC8
                LCDPrintf("%c", BIT_CHK(pSettings->Interval.Interval, 15) > 0 ? 'h' : 'm');
#else
                LCDPutch(BIT_CHK(pSettings->Interval.Interval, 15) > 0 ? 'h' : 'm');
#endif
                break;
            case 5:
                LCDPrintf("%04u", pSettings->Interval.AmountDecilitres);
                break;
            case 6:
                if (pSettings->Settings.Sensor == -1) {
                    LCDPuts("Off");
                } else {
                    LCDPrintf("%u  ", pSettings->Settings.Sensor);
                }
                break;
            case 7:
                //lcd_printf("%s", sch_int_is_running(g_pump_sel) ? "Stop " : "Start");
                break;

            default:
                break;
        }
    }

    LCDMoveCursorToPos(coords[gs_curPos]);
}

static void _updateScheduler()
{
    static const EPoint coords[13] = {
        {0, 0}, {2, 0}, {6, 0}, {8, 0}, {10, 0}, {12, 0}, {14, 0}, {16, 0},
        {18, 0}, {3, 1}, {6, 1}, {13, 1}, {8, 2}
    };
    SSchedulerSettings *pSettings = &(GlobalData.ModifiedSettings.Scheduler[gs_pump]);
    SCalendarSchedule *pSchedule = &(pSettings->Calendar);
    SCalendarScheduleDay *pDay = &(pSchedule->Days[gs_day]);

    uint8_t i;

    for (i = 0; i < 13; ++i) {
        LCDMoveCursorToPos(coords[i]);

        switch (i) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                LCDPutch((pSchedule->Weekdays & (1 << (i - 2))) ? '>' : ' ');
                break;
            case 9:
                LCDPrintf("%02u",pDay->Time.Hours);
                break;
            case 10:
                LCDPrintf("%02u",pDay->Time.Minutes);
                break;
            case 11:
                LCDPrintf("%04u",pDay->AmountDecilitres);
                break;
            case 12:
                if (pSettings->Settings.Sensor == -1) {
                    LCDPuts("Off");
                } else {
                    LCDPrintf("%u  ", pSettings->Settings.Sensor);
                }
                break;

            default:
                break;
        }
    }

    LCDMoveCursorToPos(coords[gs_curPos]);
}

static EScreenEventResult _handleKey(uint16_t key)
{
    SSchedulerSettings *pSettings = &(GlobalData.ModifiedSettings.Scheduler[gs_pump]);
    
    switch (gs_curPos) {
       // Pump selector
        case 0:
            if (key == KEY_PLUS || key == KEY_MINUS) {
                gs_pump = gs_pump == 1 ? 0 : 1;
                _draw();
            }
            break;

        // Mode selector
        case 1:
            switch (key) {
                case KEY_PLUS:
                    ++pSettings->Settings.Mode;
                    break;

                case KEY_MINUS:
                    --pSettings->Settings.Mode;
                    break;

                default:
                    break;
            }

            if (pSettings->Settings.Mode == 3) {
                pSettings->Settings.Mode = 0;
            } else if (pSettings->Settings.Mode == 255) {
                pSettings->Settings.Mode = 2;
            }

            _draw();
            break;

        default:
            break;
    }

    // Handle Cancel key
    switch (key) {
        case KEY_MENU:
            _saveSettings();
            printf("sch: settings saved\r\n");
            // No break
        case KEY_CANCEL:
            printf("sch: leaving\r\n");
            LCDSetDisplayMode(LCD_DM_ENABLED);
            return SER_PREV_SCREEN;
        default:
            break;
    }

    switch (pSettings->Settings.Mode) {
        case ManualScheduler:
            switch (key) {
                case KEY_LEFT:
                    if (--gs_curPos == 255) {
                        gs_curPos = 5;
                    }
                    break;

                case KEY_RIGHT:
                    if (++gs_curPos > 5) {
                        gs_curPos = 0;
                    }
                    break;

                case KEY_ENTER:
                    // Start/stop timer
                    if (gs_curPos == 5) {
                        if (SchedulerManualIsRunning(gs_pump)) {
                            SchedulerManualStop(gs_pump);
                        } else {
                            _saveSettings();
                            SettingsSave();
                            SchedulerManualStart(gs_pump);
                        }
                    }
                    break;

                case KEY_PLUS:
                    // Increment water amount
                    if (gs_curPos == 2) {
                        uint16_t amount = pSettings->Manual.AmountDecilitres;
                        if (++amount >= 10000) {
                            amount = 0;
                        }
                        pSettings->Manual.AmountDecilitres = amount;
                    }
                    // Increment delay
                    else if (gs_curPos == 3) {
                        uint16_t delay = pSettings->Manual.Delay & 0x7fff;
                        if (++delay >= 1000) {
                            delay = 0;
                        }
                        pSettings->Manual.Delay &= 0x8000;
                        pSettings->Manual.Delay |= delay;
                    }
                    // Change delay unit
                    else if (gs_curPos == 4) {
                        BIT_TGL(pSettings->Manual.Delay, 15);
                    }
                    break;

                case KEY_MINUS:
                    // Decrement water amount
                    if (gs_curPos == 2) {
                        uint16_t amount = pSettings->Manual.AmountDecilitres;
                        if (--amount >= 10000) {
                            amount = 9999;
                        }
                        pSettings->Manual.AmountDecilitres = amount;
                    }
                    // Decrement delay
                    else if (gs_curPos == 3) {
                        uint16_t delay = pSettings->Manual.Delay & 0x7fff;
                        if (--delay >= 1000) {
                            delay = 999;
                        }
                        pSettings->Manual.Delay &= 0x8000;
                        pSettings->Manual.Delay |= delay;
                    }
                    // Change delay unit
                    else if (gs_curPos == 4) {
                        BIT_TGL(pSettings->Manual.Delay, 15);
                    }
                    break;

                default:
                    break;
            }
            break;

        case IntervalScheduler:
            switch (key) {
                case KEY_LEFT:
                    if (--gs_curPos == 255) {
                        gs_curPos = 8;
                    }
                    break;
                case KEY_RIGHT:
                    if (++gs_curPos > 8) {
                        gs_curPos = 0;
                    }
                    break;

                case KEY_ENTER:
                    // Start irrigation immediately
                    if (gs_curPos == 7) {
                        SchedulerIntervalStartNow(gs_pump);
                    }
                    // Reset timer
                    else if (gs_curPos == 8) {
                        SchedulerIntervalReset(gs_pump);
                    }
                    break;

                case KEY_PLUS:
                    // Increment remaining time
                    // @todo FIXME: new interval must be saved before setting remaining time
                    if (gs_curPos == 2) {
                        SchedulerIntervalAdjustRemaining(gs_pump, 60);
                    }
                    // Increment interval
                    if (gs_curPos == 3) {
                        uint16_t timeval = pSettings->Interval.Interval & 0x7fff;
                        if (++timeval >= 1000) {
                            timeval = 0;
                        }
                        pSettings->Interval.Interval &= 0x8000;
                        pSettings->Interval.Interval |= timeval;
                    }
                    // Switch interval unit
                    else if (gs_curPos == 4) {
                        // Toggle minute/hour bit
                        BIT_TGL(pSettings->Interval.Interval, 15);
                    }
                    // Increment amount
                    else if (gs_curPos == 5) {
                        uint16_t amount = pSettings->Interval.AmountDecilitres;
                        if (++amount >= 10000) {
                            amount = 0;
                        }
                        pSettings->Interval.AmountDecilitres = amount;
                    }
                    // Increment sensor index
                    else if (gs_curPos == 6) {
                        if (++pSettings->Settings.Sensor >= SENSOR_COUNT) {
                            pSettings->Settings.Sensor = -1;
                        }
                    }
                    break;

                case KEY_MINUS:
                    // Decrement remaining time
                    if (gs_curPos == 2) {
                        SchedulerIntervalAdjustRemaining(gs_pump, -60);
                    }
                    // Decrement interval
                    if (gs_curPos == 3) {
                        uint16_t timeval = pSettings->Interval.Interval & 0x7fff;
                        if (--timeval >= 1000) {
                            timeval = 999;
                        }
                        pSettings->Interval.Interval &= 0x8000;
                        pSettings->Interval.Interval |= timeval;
                    }
                    // Switch interval unit
                    else if (gs_curPos == 4) {
                        // Toggle minute/hour bit
                        BIT_TGL(pSettings->Interval.Interval, 15);
                    }
                    // Decrement amount
                    else if (gs_curPos == 5) {
                        uint16_t amount = pSettings->Interval.AmountDecilitres;
                        if (--amount >= 10000) {
                            amount = 9999;
                        }
                        pSettings->Interval.AmountDecilitres = amount;
                    }
                    // Decrement sensor index
                    else if (gs_curPos == 6) {
                        if (--pSettings->Settings.Sensor < 0) {
                            pSettings->Settings.Sensor = SENSOR_COUNT - 1;
                        }
                    }
                    break;

                default:
                    break;
            }
            break;

        case CalendarScheduler:
            switch (key) {
                case KEY_LEFT:
                    if (--gs_curPos == 255) {
                        gs_curPos = 12;
                    }
                    break;
                case KEY_RIGHT:
                    if (++gs_curPos > 12) {
                        gs_curPos = 0;
                    }
                    break;

                case KEY_ENTER:
                    // Select day of week
                    if (gs_curPos >= 2 && gs_curPos <= 8) {
                        gs_day = gs_curPos - 2;
                    }
                    break;

                case KEY_PLUS:
                    // Enable scheduler for the selected day
                    if (gs_curPos >= 2 && gs_curPos <= 8) {
                        gs_day = gs_curPos - 2;
                        pSettings->Calendar.Weekdays |= (1 << gs_day);
                        printf("sch: sch_days[%d] = 0x%02X\r\n", gs_pump,
                                pSettings->Calendar.Weekdays);
                    }
                    // Increment hour value for the current day
                    else if (gs_curPos == 9) {
                        uint8_t h = pSettings->Calendar.Days[gs_day].Time.Hours;
                        if (++h >= 24) {
                            h = 0;
                        }
                        pSettings->Calendar.Days[gs_day].Time.Hours = h;
                    }
                    // Increment minute value for the current day
                    else if (gs_curPos == 10) {
                        uint8_t m = pSettings->Calendar.Days[gs_day].Time.Minutes;
                        if (++m >= 60) {
                            m = 0;
                        }
                        pSettings->Calendar.Days[gs_day].Time.Minutes = m;
                    }
                    // Increment water amount
                    else if (gs_curPos == 11) {
                        uint16_t amount = pSettings->Calendar.Days[gs_day].AmountDecilitres;
                        if (++amount >= 10000) {
                            amount = 0;
                        }
                        pSettings->Calendar.Days[gs_day].AmountDecilitres = amount;
                    }
                    // Increment sensor index
                    else if (gs_curPos == 12) {
                        if (++pSettings->Settings.Sensor >= SENSOR_COUNT) {
                            pSettings->Settings.Sensor = -1;
                        }
                    }
                    break;

                case KEY_MINUS:
                    // Disable scheduler for the selected day
                    if (gs_curPos >= 2 && gs_curPos <= 8) {
                        gs_day = gs_curPos - 2;
                        pSettings->Calendar.Weekdays &= ~(1 << gs_day);
                        printf("sch: sch_days[%d] = 0x%02X\r\n", gs_pump,
                                pSettings->Calendar.Weekdays);
                    }
                    // Decrement hour value for the current day
                    else if (gs_curPos == 9) {
                        uint8_t h = pSettings->Calendar.Days[gs_day].Time.Hours;
                        if (--h >= 24) {
                            h = 23;
                        }
                        pSettings->Calendar.Days[gs_day].Time.Hours = h;
                    }
                    // Decrement minute value for the current day
                    else if (gs_curPos == 10) {
                        uint8_t m = pSettings->Calendar.Days[gs_day].Time.Minutes;
                        if (--m >= 60) {
                            m = 59;
                        }
                        pSettings->Calendar.Days[gs_day].Time.Minutes = m;
                    }
                    // Decrement water amount
                    else if (gs_curPos == 11) {
                        uint16_t amount = pSettings->Calendar.Days[gs_day].AmountDecilitres;
                        if (--amount >= 10000) {
                            amount = 9999;
                        }
                        pSettings->Calendar.Days[gs_day].AmountDecilitres = amount;
                    }
                    // Decrement sensor index
                    else if (gs_curPos == 12) {
                        if (--pSettings->Settings.Sensor < 0) {
                            pSettings->Settings.Sensor = SENSOR_COUNT - 1;
                        }
                    }
                    break;

                default:
                    break;
            }
            break;
    }

    return SER_UPDATE;
}