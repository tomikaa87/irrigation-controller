/**
 * Irrigation System - Control module
 * Keypad settings Screen
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#include "scr_keyboard.h"

#include "utils.h"
#include "../global.h"
#include "../drivers/buzzer.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"

/*** Globals ******************************************************************/

static uint8_t g_menuIndex = 0;

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();
static void _adjust(int8_t value);

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult KeyboardScreenEventHandler(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            switch (data) {
                case KEY_UP:
                    g_menuIndex--;
                    if (g_menuIndex > 0) {
                        g_menuIndex = 0;
                    }
                    break;

                case KEY_DOWN:
                    g_menuIndex++;
                    if (g_menuIndex > 0) {
                        g_menuIndex = 0;
                    }
                    break;

                case KEY_PLUS:
                    _adjust(1);
                    break;

                case KEY_MINUS:
                    _adjust(-1);
                    break;

                case KEY_MENU:
                    _saveSettings();
                    // No break
                case KEY_CANCEL:
                    //g_flags.rtc_enabled = 1;
                    return SER_PREV_SCREEN;

                /*
                 * Default key press handler
                 */
                default:
                    break;
            }

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_UPDATE;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    LCDDefineChar(0, LCDCC_LevelIndicator0);
    LCDDefineChar(1, LCDCC_LevelIndicator1);
    LCDDefineChar(2, LCDCC_LevelIndicator2);
    LCDDefineChar(3, LCDCC_LevelIndicator3);

    LCDClear();

    _loadSettings();

    // Line 1
    LCDPuts(" Beep Level:        ");

    _update();
    g_menuIndex = 0;
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    static const EPoint coords[] = { {0, 0} };
    SKeypadSettings *settings = &(GlobalData.ModifiedSettings.Keypad);

    LCDMoveCursor(14, 0);
    for (uint8_t i = 0; i < 5; ++i)
        LCDPutch(0);

    switch (settings->BeepLevel)
    {
        case BV_HIGH:
            LCDMoveCursor(18, 0);
            LCDPutch(0xFF);

        case BV_MID:
            LCDMoveCursor(17, 0);
            LCDPutch(3);

        case BV_LOW:
            LCDMoveCursor(16, 0);
            LCDPutch(2);

        case BV_VERY_LOW:
            LCDMoveCursor(15, 0);
            LCDPutch(1);

        case BV_SILENT:
        default:
            break;
    }

    // Show cursor
    for (uint8_t i = 0; i < sizeof (coords) / sizeof (coords[0]); ++i)
    {
        LCDMoveCursorToPos(coords[i]);
        if (i == g_menuIndex)
            LCDPutch(LCDCC_MenuPositionIndicator);
        else
            LCDPutch(' ');
    }
}

/**
 * Adjusts the currently selected parameter by the given value.
 * @param value
 */
static void _adjust(int8_t value)
{
    SKeypadSettings *settings = &(GlobalData.ModifiedSettings.Keypad);

    switch (g_menuIndex) {
        case 0:
        {
            uint8_t v = (uint8_t)settings->BeepLevel + value;
            if (v > BV_HIGH)
                v = BV_SILENT;
            settings->BeepLevel = (EBuzzerVolume)v;
            BuzzerBeep((EBuzzerVolume)v);
            break;
        }

        default:
            break;
    }

    _update();
}
