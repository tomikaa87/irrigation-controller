/**
 * Irrigation System - Control module
 * Pump settings Screen
 *
 * @author Tamas Karpati
 * @date 2013-05-03
 */

#ifndef SCR_PUMP_H
#define	SCR_PUMP_H

#include "screen.h"

EScreenEventResult ScreenPumpEvent(EScreenEvent event, uint16_t data);

#endif	/* SCR_PUMP_H */

