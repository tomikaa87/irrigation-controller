/**
 * Irrigation System - Control module
 * Date/Time settings Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#include "scr_time.h"

#include "utils.h"
#include "../global.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"
#include <time.h>

/*** Globals ******************************************************************/

static uint8_t menu_pos = 0;
struct tm g_tm;

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();
static void _adjust(int8_t value);
static uint8_t _monthDays();

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult ScreenTimeEvent(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            switch (data) {
                case KEY_LEFT:
                    menu_pos--;
                    if (menu_pos > 4) {
                        menu_pos = 4;
                    }
                    break;

                case KEY_RIGHT:
                    menu_pos++;
                    if (menu_pos > 4) {
                        menu_pos = 0;
                    }
                    break;

                case KEY_PLUS:
                    _adjust(1);
                    break;

                case KEY_MINUS:
                    _adjust(-1);
                    break;

                case KEY_ENTER:
                    // Update time
                    g_tm.tm_sec = 0;
                    time_t t = mktime(&g_tm);
                    stime(&t);
                    // No break
                case KEY_CANCEL:
                    //g_flags.rtc_enabled = 1;
                    return SER_PREV_SCREEN;

                /*
                 * Default key press handler
                 */
                default:
                    break;
            }

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_UPDATE;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    LCDClear();

    // Line 1
    LCDPrintf("Date                ");
    // Line 3
    LCDPrintf("Time                ");

    // Load date and time from the RTC chip
    time_t t;
    time(&t);
    g_tm = *gmtime(&t);

    _update();
    menu_pos = 0;
    LCDSetDisplayMode(LCD_DM_ENABLED | LCD_DM_CUR_UNDERLINE);
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    static const EPoint coords[6] =
            { {8, 1}, {11, 1}, {14, 1}, {8, 3}, {11, 3} };

    // Print date
    LCDMoveCursor(0, 1);
    //         "     1970.01.01     "
    LCDPrintf("     %04u.%02d.%02d",
            g_tm.tm_year + 1900,
            g_tm.tm_mon + 1,
            g_tm.tm_mday);

    // Print time
    LCDMoveCursor(0, 3);
    LCDPrintf("       %02d:%02d", g_tm.tm_hour, g_tm.tm_min);

    // Show cursor
    LCDMoveCursorToPos(coords[menu_pos]);
}

/**
 * Adjusts the currently selected parameter by the given value.
 * @param value
 */
static void _adjust(int8_t value)
{
    switch (menu_pos) {
        case 0:
            g_tm.tm_year += value;
            if (g_tm.tm_year < 0) {
                g_tm.tm_year = 2099 - 1970;
            } else if (g_tm.tm_year > 2099 - 1970) {
                g_tm.tm_year = 2000 - 1970;
            }
            break;

        case 1:
            g_tm.tm_mon += value;
            if (g_tm.tm_mon < 0) {
                g_tm.tm_mon = 11;
            } else if (g_tm.tm_mon > 11) {
                g_tm.tm_mon = 0;
            }
            // Adjust day of month if necessary
            uint8_t max = _monthDays();
            if (g_tm.tm_mday > max) {
                g_tm.tm_mday = max;
            }
            break;

        case 2:
            g_tm.tm_mday += value;
            uint8_t max = _monthDays();
            if (g_tm.tm_mday < 1) {
                g_tm.tm_mday = max;
            } else if (g_tm.tm_mday > max) {
                g_tm.tm_mday = 1;
            }
            break;

        case 3:
            g_tm.tm_hour += value;
            if (g_tm.tm_hour < 0) {
                g_tm.tm_hour = 23;
            } else if (g_tm.tm_hour > 23) {
                g_tm.tm_hour = 0;
            }
            break;

        case 4:
            g_tm.tm_min += value;
            if (g_tm.tm_min < 0) {
                g_tm.tm_min = 59;
            } else if (g_tm.tm_min > 59) {
                g_tm.tm_min = 0;
            }
            break;
    }

    _update();
}

static uint8_t _monthDays()
{
    return MonthDays[g_tm.tm_mon] + ((g_tm.tm_mon == 1 && g_tm.tm_year % 4 == 0) ? 1 : 0);
}