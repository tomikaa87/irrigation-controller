/**
 * Irrigation System - Control module
 * Global definitions for screens
 *
 * @author Tamas Karpati
 * @date 2013-04-20
 */

#ifndef SCREEN_H
#define	SCREEN_H

#include <stdint.h>

/**
 * Screen event type.
 */
typedef enum {
    SE_DRAW,            /**< Instructs the screen to re-draw itself. */
    SE_UPDATE,          /**< Instructs the screen to update itself. */
    SE_KEYPRESS         /**< Sends a key press to the screen. Key code in 'data'.*/
} EScreenEvent;

/**
 * Event handler return values and screen indexes.
 */
typedef enum {
    /*
     * Screen indexes (from 0)
     */
    SER_GO_MAIN_SCREEN = 0,
    SER_GO_MENU_SCREEN,
    SER_GO_DATETIME_SCREEN,
    SER_GO_SCHEDULER_SCREEN,
    SER_GO_KEYBOARD_SCREEN,
    SER_GO_DISPLAY_SCREEN,
    SER_GO_PUMP_SCREEN,
    SER_GO_SENSOR_SCREEN,
    SER_GO_ALARM_SCREEN,
    SER_GO_TANK_SCREEN,

    SER_LAST_SCREEN_INDEX = 99,
       
    /*
     * Return values
     */
    SER_NOTHING,        /**< Indicates that no further action should be taken. */
    SER_PREV_SCREEN,    /**< Screen manager must go back to the prev. screen. */
    SER_UPDATE          /**< Indicates that the current screen must be updated. */
} EScreenEventResult;

/**
 * Event handler function pointer. First parameter is the event type, second
 * one if the optional user data.
 */
typedef EScreenEventResult (*TScreenEventFunc)(EScreenEvent, uint16_t);

#endif	/* SCREEN_H */

