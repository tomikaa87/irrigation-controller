/**
 * Irrigation Controller
 * UI utilities
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#ifndef UTILS_H
#define	UTILS_H

typedef enum {
    ROLL_UP,
    ROLL_DOWN
} roll_dir_t;

void Delay10ms(unsigned int count);
unsigned char is_in_time_intval(unsigned char h1, unsigned char m1,
        unsigned char h2, unsigned char m2);
void roll_uchar(unsigned char *val, unsigned char min, unsigned char max,
        roll_dir_t direction);
void roll_uint(unsigned int *val, unsigned int min, unsigned int max,
        roll_dir_t direction);

#endif	/* UTILS_H */

