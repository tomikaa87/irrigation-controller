/**
 * Irrigation System - Control module
 * Keypad settings Screen
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#ifndef SCR_KEYBOARD_H
#define	SCR_KEYBOARD_H

#include "screen.h"

EScreenEventResult KeyboardScreenEventHandler(EScreenEvent event, uint16_t data);

#endif	/* SCR_KEYBOARD_H */

