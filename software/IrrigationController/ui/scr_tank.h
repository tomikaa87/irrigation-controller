//
// scr_tank.h
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-30.
//

#ifndef SCR_TANK_H
#define	SCR_TANK_H

#include "screen.h"

EScreenEventResult ScreenTankEvent(EScreenEvent event, uint16_t data);

#endif	/* SCR_TANK_H */

