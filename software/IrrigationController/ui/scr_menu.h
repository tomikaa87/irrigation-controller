/**
 * Irrigation System - Control module
 * Menu Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#ifndef SCR_MENU_H
#define	SCR_MENU_H

#include "screen.h"

EScreenEventResult ScreenMenuEvent(EScreenEvent event, uint16_t data);

#endif	/* SCR_MENU_H */

