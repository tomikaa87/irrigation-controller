/**
 * Irrigation System - Control module
 * Menu Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#include "scr_menu.h"

#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"
#include "../global.h"
#include "utils.h"

#include <stdint.h>

/*** Globals ******************************************************************/

static uint8_t menu_pos = 0;
static const EPoint item_coords[8] = { {0, 0}, {0, 1}, {0, 2}, {0, 3},
            {10, 0}, {10, 1}, {10, 2}, {10, 3} };

static const char *item_texts[8] = {
        " Date/time", " Scheduler", " Keyboard", " Display",
        " Pump", " Sensor", " Alarm", " Tank"
    };

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult ScreenMenuEvent(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            switch (data) {

                case KEY_CANCEL:
                    return SER_PREV_SCREEN;

                case KEY_UP:
                    menu_pos--;
                    if (menu_pos > 7) {
                        menu_pos = 7;
                    }
                    return SER_UPDATE;

                case KEY_DOWN:
                    menu_pos++;
                    if (menu_pos > 7) {
                        menu_pos = 0;
                    }
                    return SER_UPDATE;

                case KEY_ENTER:
                    // MENU_SCREEN + 1 is the first setting screen
                    return SER_GO_MENU_SCREEN + menu_pos + 1;

                /*
                 * Default key press handler
                 */
                default:
                    break;
            }

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    LCDClear();

    // Disable cursor
    LCDSetDisplayMode(LCD_DM_ENABLED);

    // Print menu items
    unsigned char i;
    for (i = 0; i < 8; i++) {
        LCDMoveCursorToPos(item_coords[i]);
        LCDPuts(item_texts[i]);
    }

    _update();
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    unsigned char i;

    for (i = 0; i < 8; i++) {
        LCDMoveCursorToPos(item_coords[i]);
        if (i == menu_pos)
            LCDPutch(LCDCC_MenuPositionIndicator);
        else
            LCDPutch(' ');
    }
}