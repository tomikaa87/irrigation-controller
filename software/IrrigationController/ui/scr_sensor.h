/**
 * Irrigation System - Control module
 * Sensor settings Screen
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#ifndef SCR_SENSOR_H
#define	SCR_SENSOR_H

#include "screen.h"

EScreenEventResult SensorScreenEventHandler(EScreenEvent event, uint16_t data);

#endif	/* SCR_SENSOR_H */

