/**
 * Irrigation System - Control module
 * Main Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#ifndef SCR_MAIN_H
#define	SCR_MAIN_H

#include "screen.h"

EScreenEventResult ScreenMainEvent(EScreenEvent event, uint16_t data);

#endif	/* SCR_MAIN_H */

