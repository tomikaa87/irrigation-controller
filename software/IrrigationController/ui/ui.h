/* 
 * File:   ui.h
 * Author: ToMikaa
 *
 * Created on 2013. m�rcius 17., 20:15
 */

#ifndef UI_H
#define	UI_H

#include <stdint.h>

void UIInit();
void UITask();
void UIKeyPressEvent(uint16_t keyCode);
void UIRedrawCurrentScreen();

#endif	/* UI_H */

