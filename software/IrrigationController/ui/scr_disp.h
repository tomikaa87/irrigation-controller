//
// scr_disp.h
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-29.
//

#ifndef SCR_DISP_H
#define	SCR_DISP_H

#include "screen.h"

EScreenEventResult ScreenDisplayEvent(EScreenEvent event, uint16_t data);

#endif	/* SCR_DISP_H */

