/**
 * Irrigation Controller
 * UI utilities
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#include "utils.h"
#include "../config.h"

#include <htc.h>

void Delay10ms(unsigned int count)
{
    while (count-- > 0)
        __delay_ms(10);
}


unsigned char is_in_time_intval(unsigned char h1, unsigned char m1,
        unsigned char h2, unsigned char m2)
{
    unsigned int t1, t2;

    t1 = h1 * 100 + m1;
    t2 = h2 * 100 + m2;

    if (t1 >= t2)
        return 1;
    else
        return 0;
}

void roll_uchar(unsigned char *val, unsigned char min, unsigned char max,
        roll_dir_t direction)
{
    if (direction == ROLL_UP) {
        if (*val < max)
            (*val)++;
        else
            *val = min;
    } else {
        if (*val > min)
            (*val)--;
        else
            *val = max;
    }
}

void roll_uint(unsigned int *val, unsigned int min, unsigned int max,
        roll_dir_t direction)
{
    if (direction == ROLL_UP) {
        if (*val < max)
            (*val)++;
        else
            *val = min;
    } else {
        if (*val > min)
            (*val)--;
        else
            *val = max;
    }
}