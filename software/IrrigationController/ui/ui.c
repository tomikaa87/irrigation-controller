#include "ui.h"
#include "utils.h"
#include "../alarm.h"
#include "../drivers/keypad.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/rtc_pcf8583p.h"
#include "../drivers/buzzer.h"
#include "../config.h"
#include "../settings.h"
#include "../global.h"

#include "screen.h"
#include "scr_disp.h"
#include "scr_main.h"
#include "scr_menu.h"
#include "scr_time.h"
#include "scr_sched.h"
#include "scr_pump.h"
#include "scr_tank.h"
#include "scr_keyboard.h"
#include "scr_sensor.h"
#include "scr_alarm.h"

#include <stdint.h>

/*** Global variables *********************************************************/

/*
 * Stuff for screen storage stack
 */
static int8_t _screenStackIndex = -1;
static uint8_t _screenStack[4] = {0,};

/*
 * Array of pointers to screen event functions
 */
static TScreenEventFunc _screenEvents[] = {
    &ScreenMainEvent,
    &ScreenMenuEvent,
    &ScreenTimeEvent,
    &ScreenScheduleEvent,
    &KeyboardScreenEventHandler,
    &ScreenDisplayEvent,
    &ScreenPumpEvent,
    &SensorScreenEventHandler,
    &AlarmScreenEventHandler,
    &ScreenTankEvent
};

/*** Internal methods *********************************************************/

/**
 * Shows the screen given by its index and stores it in the screen stack.
 * @param index
 */
void _openScreen(uint8_t index)
{
    // Check if screen stack is full
    if (_screenStackIndex == sizeof (_screenStack) - 1) {
        return;
    }

    // Check if the given screen index is valid
    if (index >= sizeof (_screenEvents)) {
        return;
    }

    // Put the required screen's index into the stack
    ++_screenStackIndex;
    _screenStack[_screenStackIndex] = index;

    // Draw the screen
    _screenEvents[index](SE_DRAW, 0);
}

/**
 * Leaves the current screen and goes back to the previous one stored in
 * the screen stack.
 */
void _closeScreen()
{
    // Check if we are already on the main screen
    if (_screenStackIndex == -1) {
        return;
    }

    --_screenStackIndex;

    uint8_t index = 0;
    if (_screenStackIndex >= 0) {
        index = _screenStack[_screenStackIndex];
    }

    // Draw the screen
    _screenEvents[index](SE_DRAW, 0);
}

/**
 * Gets the index of the current screen.
 * @return
 */
inline uint8_t _currentScreenIndex()
{
    if (_screenStackIndex == -1) {
        return 0;
    }

    return _screenStack[_screenStackIndex];
}

/***API methods ***************************************************************/

/**
 * Initializes the UI.
 */
void UIInit()
{
    LCDSetBacklight(50);

    LCDClear();

    LCDPuts(" Irrigation  System ");
    LCDPuts("                    ");
    LCDPuts("     Controller     ");
    LCDPrintf("Version: %d.%d.%d", FW_VER_MAJOR, FW_VER_MINOR, FW_VER_PATCH);

    Delay10ms(200);
    LCDClear();

    _screenEvents[0](SE_DRAW, 0);
}

/**
 * Task function, must be called periodically.
 */
void UITask()
{
    if (!AlarmIsActive())
        _screenEvents[_currentScreenIndex()](SE_UPDATE, 0);
}

/**
 * Key press handler.
 * @param key_code Key code received from the keypad driver.
 */
void UIKeyPressEvent(uint16_t keyCode)
{
    if (AlarmIsActive())
    {
        keyCode &= KEY_CODE_MASK;
        if (keyCode == KEY_ENTER || keyCode == KEY_CANCEL)
            AlarmDismiss();
        
        return;
    }

    if (!(keyCode & KEY_LONG_PRESS)) {
        BuzzerBeep(GlobalData.Settings.Keypad.BeepLevel);
    }

    uint8_t index = _currentScreenIndex();
    EScreenEventResult result = _screenEvents[index](SE_KEYPRESS, keyCode);
    
    switch (result) {
        case SER_NOTHING:
            break;

        case SER_UPDATE:
            _screenEvents[index](SE_UPDATE, 0);
            break;

        case SER_PREV_SCREEN:
            if (index == SER_GO_MENU_SCREEN) {
                SettingsSave();
            }
            _closeScreen();
            break;

        default:
            // If result code is greater than SER_LAST_RESULT, switch to a
            // specific screen
            if (result <= SER_LAST_SCREEN_INDEX) {
                _openScreen((uint8_t)result);
            }
    }
}

void UIRedrawCurrentScreen()
{
    LCDClear();
    
    uint8_t index = _currentScreenIndex();
    _screenEvents[index](SE_DRAW, 0);
    _screenEvents[index](SE_UPDATE, 0);
}