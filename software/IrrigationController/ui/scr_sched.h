/**
 * Irrigation System - Control module
 * Scheduler settings Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#ifndef SCR_SCHED_H_
#define SCR_SCHED_H_

#include "screen.h"

EScreenEventResult ScreenScheduleEvent(EScreenEvent event, uint16_t data);

#endif /* SCR_SCHED_H_ */
