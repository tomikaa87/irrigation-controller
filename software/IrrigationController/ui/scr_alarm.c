/**
 * Irrigation System - Control module
 * Alarm settings Screen
 *
 * @author Tamas Karpati
 * @date 2014-05-11
 */

#include "scr_alarm.h"

#include "utils.h"
#include "../global.h"
#include "../drivers/lcd_hd44780.h"
#include "../drivers/keypad.h"
#include "../drivers/buzzer.h"

/*** Globals ******************************************************************/

static uint8_t g_menuIndex = 0;
static int8_t g_menuOffset = 0;

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();
static void _adjust(int8_t value);

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult AlarmScreenEventHandler(EScreenEvent event, uint16_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            g_menuIndex = 0;
            g_menuOffset = 0;
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            data &= KEY_CODE_MASK;
            switch (data) {
                case KEY_UP:
                    if (g_menuIndex > 0)
                    {
                        if (g_menuIndex - g_menuOffset == 0)
                        {
                            --g_menuOffset;
                            _draw();
                        }
                        --g_menuIndex;
                    }
                    
//                    if (g_menuIndex > 4) {
//                        g_menuOffset = 1;
//                        g_menuIndex = 4;
//                    }
                    break;

                case KEY_DOWN:
                    if (g_menuIndex < 4)
                    {
                        if (g_menuIndex - g_menuOffset == 3)
                        {
                            ++g_menuOffset;
                            _draw();
                        }
                        ++g_menuIndex;
                    }
                    
//                    if (g_menuIndex > 4) {
//                        g_menuOffset = 0;
//                        g_menuIndex = 0;
//                    }
                    break;

                case KEY_PLUS:
                    _adjust(1);
                    break;

                case KEY_MINUS:
                    _adjust(-1);
                    break;

                case KEY_ENTER:
                    switch (g_menuIndex)
                    {
                        case 4:
                            // showLastAlarm
                            break;
                            
                        default:
                            break;
                    }
                    break;

                case KEY_MENU:
                    _saveSettings();
                    // No break
                case KEY_CANCEL:
                    //g_flags.rtc_enabled = 1;
                    return SER_PREV_SCREEN;

                /*
                 * Default key press handler
                 */
                default:
                    break;
            }

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_UPDATE;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    LCDDefineChar(0, LCDCC_LevelIndicator0);
    LCDDefineChar(1, LCDCC_LevelIndicator1);
    LCDDefineChar(2, LCDCC_LevelIndicator2);
    LCDDefineChar(3, LCDCC_LevelIndicator3);

    LCDClear();

    _loadSettings();


    for (uint8_t i = 0; i < 4; ++i)
    {
        LCDMoveCursor(0, i);

        switch (i + g_menuOffset)
        {
            case 0: LCDPuts(" Beep Level:"); break;
            case 1: LCDPuts(" Msg. T.out:"); break;
            case 2: LCDPuts(" Msg. Intval:"); break;
            case 3: LCDPuts(" Blink:"); break;
            case 4: LCDPuts(" [Show Last Alarm]"); break;
            default: break;
        }
    }

    _update();
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    SAlarmSettings *settings = &(GlobalData.ModifiedSettings.Alarm);

    for (uint8_t i = 0; i < 4; ++i)
    {
        switch (i + g_menuOffset)
        {
            case 0:
                // Beep level
                LCDMoveCursor(14, i);

                for (uint8_t i = 0; i < 5; ++i)
                    LCDPutch(0);
                switch (settings->BeepLevel)
                {
                    case BV_HIGH:
                        LCDMoveCursor(18, i);
                        LCDPutch(0xFF);

                    case BV_MID:
                        LCDMoveCursor(17, i);
                        LCDPutch(3);

                    case BV_LOW:
                        LCDMoveCursor(16, i);
                        LCDPutch(2);

                    case BV_VERY_LOW:
                        LCDMoveCursor(15, i);
                        LCDPutch(1);

                    case BV_SILENT:
                    default:
                        break;
                }

                break;
        
            case 1: 
                // Message interval
                LCDMoveCursor(15, i);
                LCDPrintf("%3u s", settings->Interval);
                break;

            case 2: 
                // Message timeout
                LCDMoveCursor(15, i);
                LCDPrintf("%3u s", settings->Timeout);
                break;

            case 3:
                // Blinking
                LCDMoveCursor(17, i);
                if (settings->BlinkingEnabled)
                    LCDPuts("On ");
                else
                    LCDPuts("Off");
                break;

            default: break;
        }
    }

    // Show cursor
    for (uint8_t i = 0; i < 4; ++i)
    {
        LCDMoveCursor(0, i);
        if (i == g_menuIndex - g_menuOffset)
            LCDPutch(LCDCC_MenuPositionIndicator);
        else
            LCDPutch(' ');
    }
}

/**
 * Adjusts the currently selected parameter by the given value.
 * @param value
 */
static void _adjust(int8_t value)
{
    SAlarmSettings *settings = &(GlobalData.ModifiedSettings.Alarm);

    switch (g_menuIndex) {
        case 0:
        {
            uint8_t v = (uint8_t)settings->BeepLevel + value;
            if (v > BV_HIGH)
                v = BV_SILENT;
            settings->BeepLevel = (EBuzzerVolume)v;
            BuzzerBeep((EBuzzerVolume)v);
            break;
        }

        case 1:
            settings->Interval += value;
            break;

        case 2:
            settings->Timeout += value;
            break;

        case 3:
            settings->BlinkingEnabled ^= 1;
            break;

        default:
            break;
    }

    _update();
}

