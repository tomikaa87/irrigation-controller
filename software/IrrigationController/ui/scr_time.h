/**
 * Irrigation System - Control module
 * Date/Time settings Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */
 
#ifndef SCR_TIME_H_
#define SCR_TIME_H_

#include "screen.h"

EScreenEventResult ScreenTimeEvent(EScreenEvent event, uint16_t data);

#endif /* SCR_TIME_H_ */
