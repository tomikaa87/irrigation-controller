/**
 * Irrigation System - Control module
 * Main Screen
 *
 * @author Tamas Karpati
 * @date 2013-03-14
 */

#include "scr_main.h"

#include "../drivers/lcd_hd44780.h"
#include "../global.h"
#include "../drivers/keypad.h"
#include "../drivers/pump.h"
#include "../scheduler.h"
#include "../drivers/led.h"
#include "../watergauge.h"
#include "../alarm.h"

/*** Globals ******************************************************************/

//static uint8_t menu_pos = 0;

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult ScreenMainEvent(EScreenEvent event, uint16_t data)
{
    static uint16_t last_key = 0;
    static uint8_t press_cnt = 0;

    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            if (last_key != data) {
                last_key = data;
                press_cnt = 0;
            } else if (data & KEY_LONG_PRESS && press_cnt < 100) {
                press_cnt++;
            }

            data &= KEY_CODE_MASK;

            switch (data) {

                case KEY_MENU:
                    return SER_GO_MENU_SCREEN;

                case KEY_MINUS:
                    if (press_cnt == 100) {
                        LEDDisable(LED1);
                    }
                    break;

                case KEY_CANCEL:
                    // Reset water level on very long press of CANCEL
                    if (press_cnt == 100) {
                        WaterGaugeReset();
                    }
                    break;

                /*
                 * Default key press handler
                 */
                default:
                    break;
            }

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{
    if (AlarmIsActive())
        return;
    
    LCDClear();

    // First line
    LCDMoveCursor(0, 0);
#ifndef __XC8
    LCDPrintf("--:-- ---      [%c/%c]",
            ScheduleModeStr[GlobalData.Settings.Scheduler[0].Settings.Mode][0],
            ScheduleModeStr[GlobalData.Settings.Scheduler[1].Settings.Mode][0]);
#else
    LCDPrintf("--:-- ---      [-/-]");
    LCDMoveCursor(16, 0);
    LCDPutch(ScheduleModeStr[GlobalData.Settings.Scheduler[0].Settings.Mode][0]);
    LCDMoveCursor(18, 0);
    LCDPutch(ScheduleModeStr[GlobalData.Settings.Scheduler[1].Settings.Mode][0]);
#endif

    // Second line
    LCDMoveCursor(0, 1);
    LCDPrintf("WL: --- l  WT: --- C");

    LCDMoveCursor(0, 2);
    LCDPrintf("                    ");
    LCDMoveCursor(0, 3);
    LCDPrintf("                    ");

    // Update screen
    _update();
}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    if (AlarmIsActive())
        return;

    time_t t;
    time(&t);
    struct tm tt = *gmtime(&t);

    // Update clock
    LCDMoveCursor(0, 0);
    LCDPrintf("%2d %02d.%02d %s", tt.tm_hour, tt.tm_min, tt.tm_sec,
            WeekdayShortStr[tt.tm_wday]);
    LCDMoveCursor(2, 0);
    static unsigned char cnt = 0;
    if (++cnt & 1) {
        LCDPutch(':');
    }

    // Update water level
    LCDMoveCursor(4, 1);
    int16_t level = WaterGaugeGetRemaining();
    LCDPrintf("%03u", (level > 0 ? level / 10 : 0));

    // Scheduler stuff
    uint8_t pump;
    for (pump = 0; pump < PUMP_COUNT; ++pump) {
        // Go to the corresponding line
        LCDMoveCursor(0, 2 + pump);
        LCDPrintf("%d", pump + 1);

        if (PumpIsRunning(pump))
            LCDPutch('*');
        else
            LCDPutch(' ');

        LCDPutch(' ');

        // Print scheduler data
        switch (GlobalData.Settings.Scheduler[pump].Settings.Mode) {
            case ManualScheduler: {

                break;
            }

            case IntervalScheduler: {
                // Print remaining time
                time_t diff = SchedulerIntervalGetRemaining(pump);
                struct tm pt = *gmtime(&diff);
                LCDPrintf("%03u:%02u.%02u", pt.tm_hour + (pt.tm_mday - 1) * 24, pt.tm_min, pt.tm_sec);

                LCDMoveCursor(13, 2 + pump);
                if (PumpIsRunning(pump))
                    LCDPrintf("%4u dl", GlobalData.Settings.Scheduler[pump].Interval.AmountDecilitres);
                else
                    LCDPrintf("%4u %c ", GlobalData.Settings.Scheduler[pump].Interval.Interval & 0x7fff,
                              BIT_CHK(GlobalData.Settings.Scheduler[pump].Interval.Interval, 15) > 0 ? 'h' : 'm');

                break;
            }

            case CalendarScheduler: {

                break;
            }

            default:
                break;
        }
    }
}
