/**
 * Irrigation System - Control module
 * xxxx Screen
 *
 * @author Tamas Karpati
 * @date 2013-xx-xx
 */

#include "scr_xxxx.h"

/*** Globals ******************************************************************/

static uint8_t g_cur_pos = 0;

/*** Forward declarations *****************************************************/

static void _draw();
static void _update();
static EScreenEventResult _handleKey(uint8_t key);
static void _stepValue(uint8_t up);
static void _click();

/*** API functions ************************************************************/

/**
 * Handles events sent to this screen.
 * @param event Type of the screen event
 * @param data Optional user data (e.g. code of pressed key)
 */
EScreenEventResult scr_xxxx_event(EScreenEvent event, uint8_t data)
{
    switch (event) {
        /*
         * Handle DRAW event
         */
        case SE_DRAW:
            _draw();
            break;

        /*
         * Handle UPDATE event
         */
        case SE_UPDATE:
            _update();
            break;

        /*
         * Handle KEYPRESS event
         */
        case SE_KEYPRESS:
            return _handleKey();

        /*
         * Default event handler
         */
        default:
            break;
    }

    return SER_NOTHING;
}

/*** Internal functions *******************************************************/

/**
 * Draws the whole screen on the display.
 */
static void _draw()
{

}

/**
 * Updates data on the screen without re-drawing all the elements.
 */
static void _update()
{
    
}

/**
 * Handles key presses and returns the required UI action.
 */
static EScreenEventResult _handleKey(uint8_t key)
{
    switch (key) {

        case KEY_ENTER:
            _click();
            break;

        case KEY_MENU:
            // Save configuration
            // No break
        case KEY_CANCEL:
            return SER_PREV_SCREEN;

        default:
            break;
    }

    return SER_NOTHING;
}

/**
 * Steps the selected value up/down.
 * @param up If true, the selected value will be stepped up, otherwise down.
 */
static void _stepValue(uint8_t up)
{
    switch (g_cur_pos) {

        default:
            break;
    }
}

/**
 * Performs a click on the selected item.
 */
static void _click()
{
    
}