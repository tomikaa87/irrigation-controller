/**
 * Irrigation System - Control module
 * System routines
 *
 * @author Tamas Karpati
 * @date 2013-04-20
 */

#include <htc.h>

#include "config.h"
#include "global.h"
#include "drivers/serial.h"

void interrupt isr()
{
    // Timer0 interrupt handler
    if (TMR0IE && TMR0IF) {
        TMR0H = 0x63;
        TMR0L = 0xC0;
        g_ticks++;
        TMR0IF = 0;
    }

    // Serial interrupt
    if (SERIAL_RCIE && SERIAL_RCIF) {
        putbufch(SERIAL_RCREG);
    }
}
