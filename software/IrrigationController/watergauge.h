//
// watergauge.h
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-27.
//

#ifndef WATERGAUGE_H
#define	WATERGAUGE_H

#include <stdint.h>

typedef enum {
    WGTR_NONE,
    WGTR_LEVEL_OK,
    WGTR_WARNING_LEVEL,
    WGTR_ERROR_LEVEL
} EWaterGaugeTaskResult;

void WaterGaugeInit();
EWaterGaugeTaskResult WaterGaugeTask();

void WaterGaugeAddPumpAmount(uint16_t amountDl);
void WaterGaugeAddRefillAmount(uint16_t amountDl);
void WaterGaugeReset();
void WaterGaugeSetAmount(uint16_t amountDl);
uint16_t WaterGaugeGetRemaining();

void WaterGaugeSetWarningLevel(uint8_t percent);
void WaterGaugeSetErrorLevel(uint8_t percent);

uint8_t WaterGaugeIsPumpAllowed();

void WaterGaugeCheckLevel();
uint16_t WaterGaugeCalculateAmountFromPercent(uint8_t percent);

#endif	/* WATERGAUGE_H */

