/**
 * Irrigation System - Control module
 * Scheduler module
 *
 * @author Tamas Karpati
 * @date 2013-05-02
 */

#ifndef SCHEDULER_H
#define	SCHEDULER_H

#include <stdint.h>
#include <time.h>

/*
 * General functions
 */
void SchedulerInit();
void SchedulerTask();

/*
 * Manual mode functions
 */
void SchedulerManualStart(uint8_t pump);
void SchedulerManualStop(uint8_t pump);
uint8_t SchedulerManualIsRunning(uint8_t pump);
void SchedulerManualAdjustDelay(uint8_t pump, int16_t value);

/*
 * Interval mode functions
 */
void SchedulerIntervalStartNow(uint8_t pump);
void SchedulerIntervalReset(uint8_t pump);
void SchedulerIntervalAdjustRemaining(uint8_t pump, int16_t value);
time_t SchedulerIntervalGetRemaining(uint8_t pump);

/*
 * Calibration functions
 */
void SchedulerPumpCalibStart(uint8_t pump);
void SchedulerPumpCalibStop();

#endif	/* SCHEDULER_H */

