/**
 * Irrigation System - Control module
 * Basic configuration header
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <htc.h>


#define FW_VER_MAJOR            1
#define FW_VER_MINOR            0
#define FW_VER_PATCH            2

#define PUMP_COUNT              2
#define PUMP_DEFAULT_SPEED      18

#define SENSOR_COUNT            2

#define _XTAL_FREQ                  64000000ul
#pragma config CONFIG1H = 0x38
//__CONFIG(1, FOSC_INTIO67 & PLLCFG_ON & PRICLKEN_ON & FCMEN_OFF & IESO_OFF);
#pragma config CONFIG2L = 0x6
//__CONFIG(2, PWRTEN_ON & BOREN_SBORDIS & BORV_285);
#pragma config CONFIG2H = 0x33
//__CONFIG(3, WDTEN_ON & WDTPS_4096);
#pragma config CONFIG3H = 0xB5
//__CONFIG(4, CCP2MX_PORTC1 & PBADEN_OFF & CCP3MX_PORTB5 & HFOFST_OFF & T3CMX_PORTC0 & P2BMX_PORTB5 & MCLRE_EXTMCLR);
#pragma config CONFIG4L = 0x81
//__CONFIG(5, STVREN_ON & LVP_OFF & XINST_OFF);
#pragma config CONFIG5L = 0x0
//__CONFIG(6, CP0_ON & CP1_ON & CP2_ON & CP3_ON);
#pragma config CONFIG5H = 0x80
//__CONFIG(7, CPB_ON & CPD_OFF);
#pragma config CONFIG6L = 0x0
//__CONFIG(8, WRT0_ON & WRT1_ON & WRT2_ON & WRT3_ON);
#pragma config CONFIG6H = 0xA0
//__CONFIG(9, WRTC_OFF & WRTB_ON & WRTD_OFF);
#pragma config CONFIG7L = 0xF
//__CONFIG(10, EBTR0_OFF & EBTR1_OFF & EBTR2_OFF & EBTR3_OFF);
#pragma config CONFIG7H = 0x40
//__CONFIG(11, EBTRB_OFF);

/*
 * I2C configuration
 */
#define I2C_SCL_TRIS                TRISC3
#define I2C_SDA_TRIS                TRISC4
#define I2C_SSPCON1_BITS            SSP1CON1bits
#define I2C_SSPCON2_BITS            SSP1CON2bits
#define I2C_SSPSTAT_BITS            SSP1STATbits
#define I2C_SSPADD                  SSP1ADD
#define I2C_SSPBUF                  SSP1BUF

/*
 * LCD configuration
 */
#define LCD_HD44780_EXT_CFG
#define LCD_HD44780_4BIT
#define LCD_HD44780_4x20

#define LCD_PIN_RS                  LATBbits.LB4
#define LCD_PIN_RW                  LATAbits.LA3
#define LCD_PIN_EN                  LATBbits.LB3
#define LCD_TRIS_RS                 TRISBbits.TRISB4
#define LCD_TRIS_RW                 TRISAbits.TRISA3
#define LCD_TRIS_EN                 TRISBbits.TRISB3
#define LCD_LED_TRIS                TRISBbits.TRISB0

#ifdef LCD_HD44780_4BIT
#define LCD_PIN_D4_O                LATAbits.LA4
#define LCD_PIN_D5_O                LATBbits.LB2
#define LCD_PIN_D6_O                LATAbits.LA5
#define LCD_PIN_D7_O                LATBbits.LB1
#define LCD_PIN_D4_I                PORTAbits.RA4
#define LCD_PIN_D5_I                PORTBbits.RB2
#define LCD_PIN_D6_I                PORTAbits.RA5
#define LCD_PIN_D7_I                PORTBbits.RB1
#define LCD_TRIS_D4                 TRISAbits.TRISA4
#define LCD_TRIS_D5                 TRISBbits.TRISB2
#define LCD_TRIS_D6                 TRISAbits.TRISA5
#define LCD_TRIS_D7                 TRISBbits.TRISB1
#else
#define LCD_DATA_O                  LATD
#define LCD_DATA_I                  PORTD
#define LCD_DATA_TRIS               TRISD
#endif
#define LCD_TRIS_LED                TRISBbits.TRISB0

// Buzzer pin configuration
#define BUZZER_SW_PWM
#define BUZZER_LATCH                LATCbits.LATC5
#define BUZZER_TRIS                 TRISCbits.TRISC5

// Keypad IO configuration
#define KEY_ROW_1                   LATAbits.LA2
#define KEY_ROW_2                   LATAbits.LA1
#define KEY_ROW_3                   LATAbits.LA0
#define KEY_COL_1                   PORTBbits.RB5
#define KEY_COL_2                   PORTBbits.RB6
#define KEY_COL_3                   PORTBbits.RB7
#define KEY_ROW_1_TRIS              TRISAbits.TRISA2
#define KEY_ROW_2_TRIS              TRISAbits.TRISA1
#define KEY_ROW_3_TRIS              TRISAbits.TRISA0
#define KEY_COL_1_TRIS              TRISBbits.TRISB5
#define KEY_COL_2_TRIS              TRISBbits.TRISB6
#define KEY_COL_3_TRIS              TRISBbits.TRISB7

// LED pins
#define LED1_TRIS                   TRISAbits.TRISA7
#define LED1_PIN                    LATAbits.LATA7
#define LED2_TRIS                   TRISAbits.TRISA6
#define LED2_PIN                    LATAbits.LATA6
#define LED3_TRIS                   TRISCbits.TRISC0
#define LED3_PIN                    LATCbits.LATC0

// Pump PWM pins
#define PUMP_1_PWM_TRIS             TRISCbits.TRISC1
#define PUMP_1_PWM_PIN              LATCbits.LATC1
#define PUMP_2_PWM_TRIS             TRISCbits.TRISC2
#define PUMP_2_PWM_PIN              LATCbits.LATC2

// Serial port configuration

#define SERIAL_SPBRG                SPBRG1
#define SERIAL_SPBRGH               SPBRGH1
#define SERIAL_TXSTA                TXSTA1
#define SERIAL_RCSTA                RCSTA1
#define SERIAL_BAUDCON              BAUDCON1
#define SERIAL_TRMT                 TRMT1
#define SERIAL_RCIF                 RC1IF
#define SERIAL_RCIE                 RC1IE
#define SERIAL_RCREG                RCREG1
#define SERIAL_TXREG                TXREG1
#define SERIAL_RX_TRIS              TRISCbits.TRISC7
#define SERIAL_TX_TRIS              TRISCbits.TRISC6

#endif // CONFIG_H