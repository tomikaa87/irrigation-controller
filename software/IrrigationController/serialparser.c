#include "serialparser.h"
#include "global.h"

#include <string.h>

void ProcessSerialData(uint8_t data)
{
    static enum {
        S_INIT,
        S_PACKET_BEGIN,
        S_PACKET_HEADER,
        S_PACKET_DATA
    } state;

    // packet format:
    // indicator (2)      : 0xf00d
    // packet type (1))   : see above
    // data length (1)    : 0x00-0xff
    // channel (1)        : 0x00-PUMP_COUNT-1
    // checksum (xor) (1) : 0x00-0xff
    // data (n)

    static const uint8_t PacketIndicatorBegin = 0xf0;
    static const uint8_t PacketIndicatorEnd = 0x0d;
    static const uint8_t PacketMaxDataLength = 32;
    static const uint8_t PacketChecksumInitValue = 0x55;

    static SerialPacketHeader header;
    static uint8_t buf[32] = { 0 };
    static uint8_t bufptr = 0;
    static uint8_t localChecksum = 0x55;

    switch (state)
    {
        case S_INIT:
            if (data == PacketIndicatorBegin)
                state = S_PACKET_BEGIN;
            break;

        case S_PACKET_BEGIN:
            if (data == PacketIndicatorEnd)
            {
                state = S_PACKET_HEADER;
                bufptr = 0;
            }
            break;

        case S_PACKET_HEADER:
            if (bufptr < sizeof(SerialPacketHeader))
                header.Data[bufptr++] = data;

            if (bufptr >= sizeof(SerialPacketHeader))
            {
                bufptr = 0;
                localChecksum = PacketChecksumInitValue;

                if (header.Fields.Length <= PacketMaxDataLength && header.Fields.Type != PKT_INVALID)
                    state = S_PACKET_DATA;
                else
                    state = S_INIT;
            }
            break;

        case S_PACKET_DATA:
            if (bufptr < header.Fields.Length)
            {
                buf[bufptr++] = data;
                localChecksum ^= data;
            }

            if (bufptr >= header.Fields.Length)
            {
                state = S_INIT;

                if (localChecksum == header.Fields.Checksum)
                    ProcessPacket(&header, buf);
            }
            break;
    }    
}

void ProcessPacket(SerialPacketHeader *header, uint8_t *buf)
{
    if (header->Fields.Type == PKT_WFSDATA && header->Fields.Channel < PUMP_COUNT)
        memcpy(GlobalData.WaterFlowSensors[header->Fields.Channel].Data, buf, sizeof (WaterFlowSensorData));
}