//
// displaymanager.h
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-27.
//

#ifndef DISPLAYMANAGER_H
#define DISPLAYMANAGER_H

#include <stdint.h>

void DisplayManagerInit();
void DisplayManagerTask();
void DisplayBlink(uint8_t count);
void DisplayStopBlinking();

#endif // DISPLAYMANAGER_H