/**
 * Irrigation System - Control module
 * System routines
 *
 * @author Tamas Karpati
 * @date 2013-04-20
 */

#include "config.h"
#include "drivers/lcd_hd44780.h"
#include "drivers/rtc_pcf8583p.h"
#include "drivers/keypad.h"
#include "drivers/i2c.h"
#include "drivers/buzzer.h"
#include "drivers/pump.h"
#include "drivers/serial.h"
#include "drivers/led.h"

#include <htc.h>

void SystemInit()
{
    OSCCON = 0b01110000;        // Internal osc., 16 MHz
    PLLEN = 1;                  // PLL x4

    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;

    // Disable unnecessary peripherals
    PMD0 = 0b10110101;          // UART1, Timer2 and 4 enabled
    PMD1 = 0b10110100;          // MSSP1, CCP4, CCP2, CCP1 enabled
    PMD2 = 0b00001111;          // CTMU, ADC and comparators disabled

    // Setup LCD backlight PWM Timer, 16 KHz @ 64MHz Fosc
    PR2 = 0b11111001;
    T2CON = 0b00000101;

    // Setup pump PWM timer, 4 KHz
    PR4 = 0b11111001;
    T4CON = 0b00000111;
    CCPTMRS0 = 0b00001001;      // CCP1 and 2 are driven by Timer4

    // Setup weak pull-ups for keypad
    WPUB = 0b11100000;          // RB5-6-7 pull-ups
    RBPU = 0;                   // Enable weak pull-ups

    // Enable interrupts
    GIE = 1;
    PEIE = 1;
}

void SystemPeriphInit()
{
    LCDInit();
    LEDEnableAll();

    LEDInit();
    I2CInit();
    RTCInit();
    KeypadInit();
    PumpInit();
    SerialInit();
    BuzzerInit();

    LEDDisableAll();
}

void SystemMainTimerInit()
{
    // Use Timer0 as tick count timer
    // PSA 1:4, Fosc/4, 100 Hz @64 MHz Fosc
    T0CON = 0b10010001;
    TMR0H = 0x63;
    TMR0L = 0xC0;
    TMR0IF = 0;
    TMR0IE = 1;
}