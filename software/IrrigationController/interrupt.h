/**
 * Irrigation System - Control module
 * System routines
 *
 * @author Tamas Karpati
 * @date 2013-04-20
 */

#ifndef INTERRUPT_H
#define	INTERRUPT_H

void interrupt isr();

#endif	/* INTERRUPT_H */

