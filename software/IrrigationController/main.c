/**
 * Irrigation System - Control module
 * Main source
 *
 * @author Tamas Karpati
 * @date 2012-09-23
 */

#include "config.h"
#include "drivers/keypad.h"
#include "ui/ui.h"
#include "settings.h"
#include "global.h"
#include "ui/utils.h"
#include "system.h"
#include "interrupt.h"
#include "scheduler.h"
#include "drivers/lcd_hd44780.h"
#include "drivers/serial.h"
#include "drivers/buzzer.h"
#include "drivers/led.h"
#include "watergauge.h"
#include "displaymanager.h"
#include "alarm.h"

#include <stdio.h>
#include <string.h> // memset
#include <stdlib.h>

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------

void main()
{
    SystemInit();
    SystemPeriphInit();

    printf("Initializing... RCON: 0x%02X\r\n", RCON);
    printf("Loading settings: %u bytes\r\n", SETTINGS_DATA_SIZE);

    SettingsLoad();

    memset(GlobalData.WaterFlowSensors[0].Data, 0, sizeof (WaterFlowSensorData));
    memset(GlobalData.WaterFlowSensors[1].Data, 0, sizeof (WaterFlowSensorData));

    printf("RTC epoch: %lu\r\n", time(0));
    printf("Initializing main timer...\r\n");

    // Initialize main timer
    SystemMainTimerInit();

    printf("Initializing UI...\r\n");

    // Initialize UI
    UIInit();

    printf("Initializing Scheduler...\r\n");

    SchedulerInit();
    WaterGaugeInit();
    DisplayManagerInit();

    printf("Irrigation System Controller initialized\r\n");

    while (1) {
        // Handle key presses
        uint16_t key_code = KeypadTask();
        if (key_code > 0) {
            UIKeyPressEvent(key_code);
        }

        // Timer tick
        static tick_t update_ticks = 0;
        if (TicksElapsed(update_ticks) >= 50) {
            update_ticks = g_ticks;

            // Update time
            RTCTask();

            // Run the scheduler
            SchedulerTask();

            // Update the UI
            UITask();
            AlarmTask();

            // Handle water gauge level changes
            switch (WaterGaugeTask()) {
                case WGTR_ERROR_LEVEL:
                    LEDDisable(LED2);
                    LEDEnable(LED2);
                    break;

                case WGTR_WARNING_LEVEL:
                    LEDFlash(LED2);
                    break;

                case WGTR_LEVEL_OK:
                    LEDDisable(LED2);
                    break;

                default:
                    break;
            }

            DisplayManagerTask();
        }

        // Handle serial IO
        SerialTask();

        // LED task for LED flashing etc.
        LEDTask();

        CLRWDT();
    }
}