/**
 * CRC-16 checksum calculation algorithms
 *
 * @author Tamas Karpati <tomikaa87>
 * @date 2012. okt�ber 18.
 */

#ifndef CRC16_H
#define CRC16_H

#include <stdint.h>

uint16_t crc16_array(char *data, uint16_t len);
void crc16_reset();
void crc16_byte(uint8_t byte);
uint16_t crc16_value();

#endif /* CRC16_H */