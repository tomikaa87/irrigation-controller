/**
 * Irrigation System - Control module
 * Scheduler module
 *
 * @author Tamas Karpati
 * @date 2013-05-02
 */

#include "scheduler.h"
#include "config.h"
#include "global.h"
#include "settings.h"
#include "alarm.h"

#include "drivers/pump.h"
#include "drivers/buzzer.h"

#include <time.h>
#include <stdio.h>

/*** Globals **************************************************************************************/

static uint8_t _manualStates = 0;
static time_t _startTicks[PUMP_COUNT] = {0,};
static uint32_t _startPulseCounts[PUMP_COUNT] = {0};
static uint16_t _errorCounter = 0;

/*** Forward declarations *************************************************************************/

static void _taskManual(uint8_t pump);
static void _taskInterval(uint8_t pump);
static void _taskCalendar(uint8_t pump);

/*** API functions ********************************************************************************/

/**
 * Initializes the scheduler module.
 */
void SchedulerInit()
{
    // Reset start timestamps
    uint8_t i;
    for (i = 0; i < PUMP_COUNT; ++i) {
        _startTicks[i] = time(0);
    }
}

/**
 * Task function that must be called frequently.
 */
void SchedulerTask()
{
    uint8_t pump;
    for (pump = 0; pump < PUMP_COUNT; ++pump) {
        switch (GlobalData.Settings.Scheduler[pump].Settings.Mode) {
            case ManualScheduler:
                _taskManual(pump);
                break;

            case IntervalScheduler:
                _taskInterval(pump);
                break;

            case CalendarScheduler:
                _taskCalendar(pump);
                break;
        }
    }
}

/**
 * Start manual mode irrigation.
 * @param pump Pump index
 */
void SchedulerManualStart(uint8_t pump)
{
    // Check if manual mode is inactive
    if (!BIT_CHK(_manualStates, pump + 1)) {
        // Setup delay if necessary
        if (GlobalData.Settings.Scheduler[pump].Manual.Delay > 0) {
            time(&_startTicks[pump]);
        }
        // Activate manual irrigation
        BIT_SET(_manualStates, pump + 1);
    }
}

/**
 * Stops manual mode irrigation.
 * @param pump Pump index
 */
void SchedulerManualStop(uint8_t pump)
{
    // Check if manual mode is active
    if (BIT_CHK(_manualStates, pump + 1)) {
        // Check if pump is running and stop it
        if (PumpIsRunning(pump)) {
            PumpStop(pump);
        }
        // Deactivate manual mode
        BIT_CLR(_manualStates, pump + 1);
    }
}

/**
 * Returns true if manual mode is active.
 * @param pump Pump index
 */
uint8_t SchedulerManualIsRunning(uint8_t pump)
{
    return BIT_CHK(_manualStates, pump + 1) > 0;
}

/**
 * Adjusts manual mode delay if manual mode is already running.
 * @param pump Pump index
 * @param value Value to add to delay
 */
void SchedulerManualAdjustDelay(uint8_t pump, int16_t value)
{
    // Check if manual mode is active and pump is not running
    if (BIT_CHK(_manualStates, pump + 1) && !PumpIsRunning(pump)) {
        // Adjust delay
        if (_startTicks[pump] + value >= 0) {
            _startTicks[pump] += value;
        } else {
            _startTicks[pump] = 0;
        }
    }
}

/**
 * Starts pumping immediately in interval mode.
 * @param pump Pump index
 */
void SchedulerIntervalStartNow(uint8_t pump)
{
    if (GlobalData.Settings.Scheduler[pump].Settings.Mode == IntervalScheduler) {
        if (!PumpIsRunning(pump)) {
            _startTicks[pump] = 0;
        }
    }
}

/**
 * Resets current remaining time.
 * @param pump Pump index
 */
void SchedulerIntervalReset(uint8_t pump)
{
    if (GlobalData.Settings.Scheduler[pump].Settings.Mode == IntervalScheduler) {
        if (!PumpIsRunning(pump)) {
            // Reset start time to current time
            time(&_startTicks[pump]);
        }
    }
}

/**
 * Adjusts remaining time in interval mode.
 * @param pump Pump index
 * @param value Number of seconds to add/subtract
 */
void SchedulerIntervalAdjustRemaining(uint8_t pump, int16_t value)
{
    if (GlobalData.Settings.Scheduler[pump].Settings.Mode == IntervalScheduler) {
        if (!PumpIsRunning(pump)) {
            time_t t = _startTicks[pump];
            if (t < (time(0) - value) && t > value) {
                t += value;
            }
            _startTicks[pump] = t;
        }
    }
}

/**
 *
 * @param pump
 * @return
 */
time_t SchedulerIntervalGetRemaining(uint8_t pump)
{
    if (GlobalData.Settings.Scheduler[pump].Settings.Mode == IntervalScheduler) {
        if (!PumpIsRunning(pump)) {
            SIntervalSchedule *pSchedule = &(GlobalData.Settings.Scheduler[pump].Interval);
            time_t interval = pSchedule->Interval * 60;
            if (pSchedule->Interval & 0x8000) {
                interval *= 60;
            }
            interval += _startTicks[pump];

            interval -= time(0);
            if (interval > 0)
                return interval;
            else
                return 0;
        } else {
            return time(0) - _startTicks[pump];
        }
    } else {
        return 0;
    }
}

/*** Internal stuff *******************************************************************************/

/**
 * Task for 'MANUAL' mode
 * @param pump Pump index
 */
static void _taskManual(uint8_t pump)
{
    SManualSchedule *pSchedule = &(GlobalData.Settings.Scheduler[pump].Manual);
    time_t t;
    time(&t);

    time_t delay = pSchedule->Delay * 60;
    // Check 'hour mode' bit
    if (pSchedule->Delay & 0x8000) {
        delay *= 60;
    }

    // Check if manual mode is active
    if (BIT_CHK(_manualStates, pump + 1)) {
        // Check if pump is running
        if (!PumpIsRunning(pump)) {
            // Check if delay is 0
            if (delay != 0) {
                // Check if delay time is elapsed
                if (t - _startTicks[pump] < delay) {
                    return;
                }
            }

            // Start the pump
            _startTicks[pump] = t;
            _startPulseCounts[pump] = GlobalData.WaterFlowSensors[pump].TotalPulses;
            _errorCounter = 0;
            if (!PumpStart(pump)) {
                BIT_CLR(_manualStates, pump + 1);
            }
        } else {
            // Check if the given amount is pumped out
            uint32_t pulseCount = GlobalData.WaterFlowSensors[pump].TotalPulses;
            uint32_t amount = UINT32_DIFF(_startPulseCounts[pump], pulseCount);
            amount = amount / (GlobalData.Settings.Sensor.Sensors[pump].PulsesPerLitre / 10);
            
            // Pump protection
            if (GlobalData.WaterFlowSensors[pump].PPS <= 3)
                ++_errorCounter;
            else
                _errorCounter = 0;
            
            if (amount >= pSchedule->AmountDecilitres)
            {
                PumpStop(pump);
                BIT_CLR(_manualStates, pump + 1);
            }
            else if (_errorCounter > 10)
            {
                PumpStop(pump);
                BIT_CLR(_manualStates, pump + 1);
                AlarmPumpFailure(pump);
            }
                
            
            // Check if pump time is elapsed
//            if (t - _startTicks[pump] >= PumpCalcRunTime(pump, pSchedule->AmountDecilitres)) {
//                // Stop pump and deactivate manual mode
//                PumpStop(pump);
//                BIT_CLR(_manualStates, pump + 1);
//            }
            
        }
    }
}

/**
 * Task for 'INTERVAL' mode
 * @param pump Pump index
 * @todo Implement sensor checking
 */
static void _taskInterval(uint8_t pump)
{
    SIntervalSchedule *pSchedule = &(GlobalData.Settings.Scheduler[pump].Interval);

    time_t t;
    time(&t);

    time_t interval = pSchedule->Interval * 60;
    if (pSchedule->Interval & 0x8000) {
        interval *= 60;
    }

    // Check if pump is running
    if (!PumpIsRunning(pump)) {
        // Check if interval is elapsed
        if (t - _startTicks[pump] < interval) {
            return;
        }

        // Start the pump
        _startTicks[pump] = t;
        _startPulseCounts[pump] = GlobalData.WaterFlowSensors[pump].TotalPulses;
        _errorCounter = 0;
        if (!PumpStart(pump)) {
            SchedulerIntervalReset(pump);
        }
    } else {
        // Check if the given amount is pumped out
        uint32_t pulseCount = GlobalData.WaterFlowSensors[pump].TotalPulses;
        uint32_t amount = UINT32_DIFF(_startPulseCounts[pump], pulseCount);
        amount = amount / (GlobalData.Settings.Sensor.Sensors[pump].PulsesPerLitre / 10);
        
        // Pump protection
        if (GlobalData.WaterFlowSensors[pump].PPS <= 3)
                ++_errorCounter;
        else
            _errorCounter = 0;

        if (amount >= pSchedule->AmountDecilitres)
        {
            PumpStop(pump);
            BIT_CLR(_manualStates, pump + 1);
        }
        else if (_errorCounter > 10)
        {
            PumpStop(pump);
            BIT_CLR(_manualStates, pump + 1);
            AlarmPumpFailure(pump);
        }
        
//        // Check if pump time is elapsed
//        if (t - _startTicks[pump] >= PumpCalcRunTime(pump, pSchedule->AmountDecilitres)) {
//            _startTicks[pump] = t;
//            // Stop pump
//            PumpStop(pump);
//        }
    }
}

#include <stdio.h>

/**
 * Task for 'SCHEDULER' mode
 * @param pump Pump index
 * @todo Schedule should start if only a portion of water is remaining needed for the irrigation,
 * and stop if the tank is empty
 * @todo Implement sensor checking
 * @todo Schedule completion flags instead of "time+runtime" checking
 */
static void _taskCalendar(uint8_t pump)
{
    printf("*** CALENDAR SCHEDULE FOR %d ***\r\n", pump);

    // Get current time
    time_t t;
    time(&t);
    printf("  t = %lu\r\n", t);

    // Create tm structure
    struct tm tt = *gmtime(&t);
    printf("  tt = %2d:%02d (wd = %d)\r\n", tt.tm_hour, tt.tm_min, tt.tm_wday);

    // Get calendar scheduler settings
    SCalendarSchedule *pSchedule = &(GlobalData.Settings.Scheduler[pump].Calendar);
    SCalendarScheduleDay *pDay = &(pSchedule->Days[tt.tm_wday]);

    // Create a tm structure of scheduled time
    struct tm schTime = tt;
    schTime.tm_hour = pDay->Time.Hours;
    schTime.tm_min = pDay->Time.Minutes;
    schTime.tm_sec = 0;
    printf("  schTime = %2d:%02d (wd = %d)\r\n", schTime.tm_hour, schTime.tm_min, schTime.tm_wday);
    // Convert schedule time to time_t
    time_t schTime_t = mktime(&schTime);
    printf("  schTime_t = %lu\r\n", schTime_t);

    // Calculate pump running time
    uint16_t runTime = PumpCalcRunTime(pump, pDay->AmountDecilitres);
    printf("  runtime = %u\r\n", runTime);

    // Check if we have a scheduler for tuday
    if (pSchedule->Weekdays & (1 << tt.tm_wday)) {
        printf("  day OK\r\n");
        // Check if pump is running
        if (!PumpIsRunning(pump)) {
            // Check if irrigation time has come
            if (t >= schTime_t) {
                printf("  schedule time has come\r\n");
                printf("  time+runtime = %lu\r\n", schTime_t + runTime);
                // Check if the time is already passed (-1 is for safety)
                if (t < (schTime_t + runTime - 1)) {
                    printf("  starting irrigation\r\n");
                    
                    // Start the pump
                    _startTicks[pump] = t;
                    _startPulseCounts[pump] = GlobalData.WaterFlowSensors[pump].TotalPulses;
                    
                    if (!PumpStart(pump)) {
                        _startTicks[pump] = t - runTime;
                    }
                } else {
                    printf("  schedule time has passed\r\n");
                }
            }
        } else {
            // Check if the given amount is pumped out
            uint32_t pulseCount = GlobalData.WaterFlowSensors[pump].TotalPulses;
            uint32_t amount = UINT32_DIFF(_startPulseCounts[pump], pulseCount);
            amount = amount / (GlobalData.Settings.Sensor.Sensors[pump].PulsesPerLitre / 10);

            if (amount >= pDay->AmountDecilitres)
            {
                PumpStop(pump);
                BIT_CLR(_manualStates, pump + 1);
            }
//            // Stop the pump
//            if (t - _startTicks[pump] >= runTime) {
//                printf("  schedule irrigation finished\r\n");
//                PumpStop(pump);
//            }
        }
    }
}