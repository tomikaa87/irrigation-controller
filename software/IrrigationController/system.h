/**
 * Irrigation System - Control module
 * System routines
 *
 * @author Tamas Karpati
 * @date 2013-04-20
 */

#ifndef SYSTEM_H
#define	SYSTEM_H

void SystemInit();
void SystemPeriphInit();
void SystemMainTimerInit();

#define SystemStopMainTimer()   { TMR0IE = 0; }
#define SystemStartMainTimer()  { TMR0IE = 1; }

#endif	/* SYSTEM_H */

