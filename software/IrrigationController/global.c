#include "global.h"

#include <string.h>         // memcpy

/*** Global constants *****************************************************************************/

const char *ScheduleModeStr[3] = {"MAN", "INT", "CAL"};
const char *WeekdayShortStr[7] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
const uint8_t MonthDays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/*** Global variables *****************************************************************************/

tick_t g_ticks = 0;

/**************************************************************************************************/

/**
 * Calculates the number of ticks elapsed since the given number of ticks.
 * @param since Number of ticks to compare.
 * @return Elapsed ticks.
 */
inline tick_t TicksElapsed(tick_t since)
{
    return g_ticks - since;
}

/**
 * Calculates the number of ticks from the current ticks to the given number
 * of ticks.
 * @param ticks Number of ticks to compare.
 * @return Number of ticks
 */
inline tick_t TicksTo(tick_t ticks)
{
    return ticks - g_ticks;
}

/**
 * Calculates the bit-weight of a 16-bit integer.
 * @param w
 * @return
 */
inline uint8_t _bitWeight(int16_t w)
{
    uint8_t count = 0, i;

    for (i = 0; i < 16; i++) {
        if (w & 1) {
            count++;
        }
        w >>= 1;
    }

    return count;
}

inline void _loadSettings()
{
    memcpy(GlobalData.ModifiedSettings.Data, GlobalData.Settings.Data, SETTINGS_DATA_SIZE);
}

inline void _saveSettings()
{
    GlobalData.ModifiedSettings.Tank.UsedAmountDl = GlobalData.Settings.Tank.UsedAmountDl;
    memcpy(GlobalData.Settings.Data, GlobalData.ModifiedSettings.Data, SETTINGS_DATA_SIZE);
    GlobalData.Settings.Checksum = SettingsCalculateChecksum();
}

/*** Interface 2.0 ********************************************************************************/

/**
 * Storage for global data (for example Settings).
 */
SGlobalData GlobalData;