//
// watergauge.cpp
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-27.
//

#include "watergauge.h"
#include "global.h"

struct {
    unsigned warning : 1;
    unsigned error : 1;
    unsigned stateChanged : 1;
    unsigned : 5;
} _flags;

//--------------------------------------------------------------------------------------------------
void WaterGaugeInit()
{
    _flags.warning = 0;
    _flags.error = 0;
    _flags.stateChanged = 1;    // Force resetting warning LED
}

//--------------------------------------------------------------------------------------------------
EWaterGaugeTaskResult WaterGaugeTask()
{
    WaterGaugeCheckLevel();

    if (_flags.stateChanged) {
        _flags.stateChanged = 0;

        if (_flags.warning)
            return WGTR_WARNING_LEVEL;
        else if (_flags.error)
            return WGTR_ERROR_LEVEL;
        else
            return WGTR_LEVEL_OK;
    } else
        return WGTR_NONE;
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeAddPumpAmount(uint16_t amountDl)
{
    if (amountDl == 0)
        return;

    if (amountDl + GlobalData.Settings.Tank.UsedAmountDl <= GlobalData.Settings.Tank.Capacity * 10)
        GlobalData.Settings.Tank.UsedAmountDl += amountDl;
    else
        GlobalData.Settings.Tank.UsedAmountDl = GlobalData.Settings.Tank.Capacity * 10;

    SettingsSave();
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeAddRefillAmount(uint16_t amountDl)
{
    if (amountDl == 0)
        return;

    if (amountDl <= GlobalData.Settings.Tank.UsedAmountDl)
        GlobalData.Settings.Tank.UsedAmountDl -= amountDl;
    else
        GlobalData.Settings.Tank.UsedAmountDl = 0;

    SettingsSave();
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeReset()
{
    GlobalData.Settings.Tank.UsedAmountDl = 0;
    SettingsSave();
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeSetAmount(uint16_t amountDl)
{
    GlobalData.Settings.Tank.UsedAmountDl = amountDl;
    if (GlobalData.Settings.Tank.UsedAmountDl > GlobalData.Settings.Tank.Capacity * 10)
        GlobalData.Settings.Tank.UsedAmountDl = GlobalData.Settings.Tank.Capacity * 10;

    SettingsSave();
}

//--------------------------------------------------------------------------------------------------
uint16_t WaterGaugeGetRemaining()
{
    return GlobalData.Settings.Tank.Capacity * 10 - GlobalData.Settings.Tank.UsedAmountDl;
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeSetWarningLevel(uint8_t percent)
{
    GlobalData.Settings.Tank.WarningAmountDl = WaterGaugeCalculateAmountFromPercent(percent);
    SettingsSave();
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeSetErrorLevel(uint8_t percent)
{
    GlobalData.Settings.Tank.ErrorAmountDl = WaterGaugeCalculateAmountFromPercent(percent);
    SettingsSave();
}

//--------------------------------------------------------------------------------------------------
void WaterGaugeCheckLevel()
{
    // Sanity check
    if (GlobalData.Settings.Tank.Capacity > 999)
        GlobalData.Settings.Tank.Capacity = 999;

    if (GlobalData.Settings.Tank.UsedAmountDl > GlobalData.Settings.Tank.Capacity * 10)
        GlobalData.Settings.Tank.UsedAmountDl = GlobalData.Settings.Tank.Capacity * 10;

    if (GlobalData.Settings.Tank.WarningAmountDl > GlobalData.Settings.Tank.Capacity * 10)
        WaterGaugeSetWarningLevel(30);
    if (GlobalData.Settings.Tank.ErrorAmountDl > GlobalData.Settings.Tank.WarningAmountDl)
        WaterGaugeSetErrorLevel(15);

    // Warning/Error level check
    uint16_t remaining = WaterGaugeGetRemaining();
    if (remaining <= GlobalData.Settings.Tank.WarningAmountDl && 
            remaining > GlobalData.Settings.Tank.ErrorAmountDl && 
            !_flags.warning) {
        _flags.warning = 1;
        _flags.error = 0;
        _flags.stateChanged = 1;
    } else if (remaining <= GlobalData.Settings.Tank.ErrorAmountDl && !_flags.error) {
        _flags.warning = 0;
        _flags.error = 1;
        _flags.stateChanged = 1;
    } else if (remaining > GlobalData.Settings.Tank.WarningAmountDl && 
            (_flags.warning || _flags.error)) {
        _flags.warning = 0;
        _flags.error = 0;
        _flags.stateChanged = 1;
    }
}

//--------------------------------------------------------------------------------------------------
uint16_t WaterGaugeCalculateAmountFromPercent(uint8_t percent)
{
    if (GlobalData.Settings.Tank.Capacity == 0 || percent == 0)
        return 0;
    if (percent >= 100)
        return GlobalData.Settings.Tank.Capacity * 10;

    return (GlobalData.Settings.Tank.Capacity * 10) * percent / 100;
}

//--------------------------------------------------------------------------------------------------
uint8_t WaterGaugeIsPumpAllowed()
{
    return !_flags.error;
}
