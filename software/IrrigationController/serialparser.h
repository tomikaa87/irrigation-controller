/* 
 * File:   serialparser.h
 * Author: Tam�s
 *
 * Created on 2015. j�nius 27., 11:44
 */

#ifndef SERIALPARSER_H
#define	SERIALPARSER_H

#include <stdint.h>

enum {
    PKT_INVALID = 0,
    PKT_WFSDATA         // Water flow sensor data
} SerialPacketTypes;

typedef union {
    struct _fields {
        uint8_t Type;
        uint8_t Length;
        uint8_t Channel;
        uint8_t Checksum;
    } Fields;

    uint8_t Data[sizeof (struct _fields)];
} SerialPacketHeader;

void ProcessSerialData(char c);
void ProcessPacket(SerialPacketHeader *header, uint8_t *buf);

#endif	/* SERIALPARSER_H */

