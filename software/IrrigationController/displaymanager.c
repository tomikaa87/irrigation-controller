//
// displaymanager.c
// Irrigation System Controller
//
// This file was created by Tam�s K�rp�ti on 2013-07-27.
//

#include "displaymanager.h"
#include "global.h"
#include "drivers/lcd_hd44780.h"

#include <time.h>
#include <stdio.h>

//--------------------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------------------

bit g_isBlinkingActive = 0;
static uint8_t g_blinkCount = 0;

//--------------------------------------------------------------------------------------------------
// API functions
//--------------------------------------------------------------------------------------------------

void DisplayManagerInit()
{
    LCDSetBacklight(GlobalData.Settings.Display.Level);
}

//--------------------------------------------------------------------------------------------------
void DisplayManagerTask()
{
    if (g_isBlinkingActive)
    {
        if (g_blinkCount > 0)
        {
            if (LCDGetBacklight() != GlobalData.Settings.Display.Level)
                LCDSetBacklight(GlobalData.Settings.Display.Level);
            else
            {
                LCDSetBacklight(1);
                --g_blinkCount;
            }

            return;
        }
        else
        {
            g_isBlinkingActive = 0;
        }
    }

    if (GlobalData.Settings.Display.DimmingEnabled) {
        time_t t = time(0);
        struct tm pt = *gmtime(&t);

        printf("DisplayManagerTask tm_hour=%02d tm_min=%02d\r\n", pt.tm_hour, pt.tm_min);
        printf("DisplayManagerTask DimmingStart=%02d:%02d DimmingEnd=%02d:%02d\r\n",
            GlobalData.Settings.Display.DimmingStart.Hours,
            GlobalData.Settings.Display.DimmingStart.Minutes,
            GlobalData.Settings.Display.DimmingEnd.Hours,
            GlobalData.Settings.Display.DimmingEnd.Minutes);
    
        uint16_t from = ((uint16_t)GlobalData.Settings.Display.DimmingStart.Hours << 8) 
            + GlobalData.Settings.Display.DimmingStart.Minutes;
        uint16_t to = ((uint16_t)GlobalData.Settings.Display.DimmingEnd.Hours << 8) 
            + GlobalData.Settings.Display.DimmingEnd.Minutes;
        uint16_t current = (pt.tm_hour << 8) + pt.tm_min;

        printf("DisplayManagerTask from=%04x to=%04x current=%04x\r\n", from, to, current);
        
        uint8_t enableDimming = 0;

        if (to < from) 
        {
            if (current >= to && current < from)
                enableDimming = 0;
            else
                enableDimming = 1;
        } 
        else 
        {
            if (current >= from && current < to)
                enableDimming = 1;
            else
                enableDimming = 0;
        }
                
        if (enableDimming && LCDGetBacklight() != GlobalData.Settings.Display.DimLevel) {
            printf("DisplayManagerTask setting dimmed level\r\n");
            LCDSetBacklight(GlobalData.Settings.Display.DimLevel);
        } else if (!enableDimming && LCDGetBacklight() != GlobalData.Settings.Display.Level) {
            printf("DisplayManagerTask setting normal level\r\n");
            LCDSetBacklight(GlobalData.Settings.Display.Level);
        }
        
    } else {
        // Dimming disabled, set brightness to normal level
        if (LCDGetBacklight() != GlobalData.Settings.Display.Level)
            LCDSetBacklight(GlobalData.Settings.Display.Level);
    }
}

//--------------------------------------------------------------------------------------------------
void DisplayBlink(uint8_t count)
{
    g_blinkCount = count;
    g_isBlinkingActive = 1;
}

//--------------------------------------------------------------------------------------------------
void DisplayStopBlinking()
{
    g_isBlinkingActive = 0;
    DisplayManagerTask();
}